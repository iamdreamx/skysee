object fMain: TfMain
  Left = 192
  Top = 104
  Width = 696
  Height = 480
  Caption = 'SkySee - '#38271#39118#22270#29255#27983#35272#22120
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnMouseWheel = FormMouseWheel
  OnPaint = FormPaint
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object splLeft: TSplitter
    Left = 161
    Top = 20
    Height = 395
  end
  object splRight: TSplitter
    Left = 685
    Top = 20
    Height = 395
    Align = alRight
  end
  object SkyDirFileTree1: TSkyDirFileTree
    Left = 0
    Top = 20
    Width = 161
    Height = 395
    Hint = #30446#24405#26641
    Align = alLeft
    Constraints.MinWidth = 1
    Indent = 19
    PopupMenu = pmDirTree
    TabOrder = 0
    OnChange = SkyDirFileTree1Change
    RootName = #25105#30340#30005#33041
    FileTypes = '*.*'
    Options = [sdfFindHidden]
    UnZipDir = '.\'
    ZipperPath = '.'
  end
  object ScrollBox1: TScrollBox
    Left = 164
    Top = 20
    Width = 521
    Height = 395
    HorzScrollBar.Style = ssFlat
    HorzScrollBar.Tracking = True
    HorzScrollBar.Visible = False
    VertScrollBar.Style = ssFlat
    VertScrollBar.Tracking = True
    VertScrollBar.Visible = False
    Align = alClient
    Color = clBtnFace
    ParentColor = False
    TabOrder = 1
    OnMouseWheel = ScrollBox1MouseWheel
    OnResize = ScrollBox1Resize
    object lbErrMsg: TLabel
      Tag = -1
      Left = 0
      Top = 0
      Width = 517
      Height = 391
      Align = alClient
      Alignment = taCenter
      Caption = 'lbErrMsg'
      Layout = tlCenter
      Visible = False
    end
    object PaintBox1: TPaintBox
      Left = 16
      Top = 64
      Width = 257
      Height = 233
      ParentShowHint = False
      PopupMenu = pmGraphic
      ShowHint = True
      OnMouseDown = PaintBox1MouseDown
      OnMouseMove = PaintBox1MouseMove
      OnMouseUp = PaintBox1MouseUp
      OnPaint = PaintBox1Paint
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 415
    Width = 688
    Height = 19
    AutoHint = True
    Panels = <
      item
        Width = 360
      end
      item
        Alignment = taCenter
        Width = 214
      end
      item
        Alignment = taCenter
        Width = 66
      end
      item
        Width = 50
      end>
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 688
    Height = 20
    AutoSize = True
    ButtonHeight = 20
    ButtonWidth = 37
    Caption = 'ToolBar1'
    EdgeBorders = []
    Flat = True
    ShowCaptions = True
    TabOrder = 3
    Visible = False
    object tbtTest: TToolButton
      Left = 0
      Top = 0
      Caption = '&Test'
      ImageIndex = 0
    end
    object Image1: TImage
      Left = 37
      Top = 0
      Width = 32
      Height = 32
      AutoSize = True
      Picture.Data = {
        055449636F6E0000010001001010000000000000680500001600000028000000
        1000000020000000010008000000000040010000000000000000000000000000
        00000000FFFFFF00F3FFFF00EBFFFF00D9FFFF00D6FFFF00CCFCFF00C7F9FF00
        C2EEF700FFB1B100FFB1AF00FFACAC00FFA3A300FF9A9A00FC919100F8898900
        F48B8B00F2848400E2909000EE899300EC838300E5898300B9EAFF00B6E9FF00
        B3E2FF00B2DFFF00ACDBFF00A9D7FF00A5D3FF00A1CEFF00B1D7D0009CC9FF00
        8DB5D000D9837C00E17C7C00DD797900D47D7600D3757500CE6F6F00C0004900
        A1003A00980030007DB4E60076A6D4006FA1CE006698C900729188006E8C8800
        698781006C807700637568007349490053656800485D77004154680062383100
        61353500632E2E00592626001B00000016000000000D0C000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        FFFFFF00122827272727272727263D292B2B2B2A092320202020202014362D17
        1919182B0A25242424242424213A1F1A1B1B192B0A252424220D0B0B083D0015
        1C1B192B0A25240C323D3D3D3D3D3D3001181A2B0A25133D370E13130D3D0134
        3D051A2B0A252413212413130D3D0419151B192B0A25242424133D3D3D3D061B
        1B1B192B0A25242424133D0216151B1B1B1B192B0A25242424133D031B1B1B1B
        1B1B192B0A2524240D0F3B1D1B1B19051B1B192B0A25243D3D0A392E15193531
        191B192B0A25243D3D0D0F3D01193533191B192B0A252424132421382F151919
        1B1B192B0A252525252525103D011E1C1C1C1B2C0B0A0A0A0A0A0A08113C0203
        0303030700000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000}
      Visible = False
    end
  end
  object MainMenu1: TMainMenu
    Left = 48
    Top = 40
    object miFile: TMenuItem
      Caption = #25991#20214'(&F)'
      object B1: TMenuItem
        Action = actOpenDir
      end
      object R5: TMenuItem
        Action = actSaveCurrPic
      end
      object A2: TMenuItem
        Action = actSaveAs
      end
      object R7: TMenuItem
        Action = actAutoBrowse
        AutoCheck = True
      end
      object N31: TMenuItem
        Caption = '-'
      end
      object L1: TMenuItem
        Action = actLastPicture
      end
      object N30: TMenuItem
        Action = actNextPicture
      end
      object N28: TMenuItem
        Caption = '-'
      end
      object miRecent_0: TMenuItem
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_1: TMenuItem
        Tag = 1
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_2: TMenuItem
        Tag = 2
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_3: TMenuItem
        Tag = 3
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_4: TMenuItem
        Tag = 4
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_5: TMenuItem
        Tag = 5
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_6: TMenuItem
        Tag = 6
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_7: TMenuItem
        Tag = 7
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_8: TMenuItem
        Tag = 8
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_9: TMenuItem
        Tag = 9
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_A: TMenuItem
        Tag = 10
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_B: TMenuItem
        Tag = 11
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_C: TMenuItem
        Tag = 12
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_D: TMenuItem
        Tag = 13
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_E: TMenuItem
        Tag = 14
        Visible = False
        OnClick = miRecentClick
      end
      object miRecent_F: TMenuItem
        Tag = 15
        Visible = False
        OnClick = miRecentClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object miExit: TMenuItem
        Caption = #36864#20986'(&X)'
        ShortCut = 32883
        OnClick = miExitClick
      end
    end
    object miEffect: TMenuItem
      Caption = #29305#25928'(&E)'
      object R1: TMenuItem
        Action = actRevert
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object S3: TMenuItem
        Action = actBlurSoft
      end
      object P1: TMenuItem
        Action = actSharpen
      end
      object G1: TMenuItem
        Action = actGrayScale
      end
      object V1: TMenuItem
        Action = actReverseColor
      end
      object X1: TMenuItem
        Action = actExposure
      end
      object N24: TMenuItem
        Caption = '-'
      end
      object B2: TMenuItem
        Action = actEmboss
      end
      object N9: TMenuItem
        Action = actEngrave
      end
      object W3: TMenuItem
        Action = actWoodcut
      end
      object N16: TMenuItem
        Action = actWatercolor
      end
      object N17: TMenuItem
        Action = actCanvasOut
      end
      object N18: TMenuItem
        Action = actLight
      end
      object N25: TMenuItem
        Caption = '-'
      end
      object M1: TMenuItem
        Action = actMergeColor
      end
      object C1: TMenuItem
        Action = actCover
      end
      object N19: TMenuItem
        Action = actBrightDark
      end
    end
    object miAdjust: TMenuItem
      Caption = #35843#33410'(&J)'
      object R3: TMenuItem
        Action = actRevert
      end
      object A4: TMenuItem
        Action = actAdjSize
      end
      object N12: TMenuItem
        Caption = '-'
      end
      object C3: TMenuItem
        Action = actAdjRGB
      end
      object H1: TMenuItem
        Action = actAdjHue
      end
      object B5: TMenuItem
        Action = actAdjBrightness
      end
      object N210: TMenuItem
        Action = actAdjBrightness2
      end
      object S5: TMenuItem
        Action = actAdjSaturation
      end
      object E1: TMenuItem
        Action = actEnhanceContrast
      end
      object W1: TMenuItem
        Action = actWeakenContrast
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object N14: TMenuItem
        Action = actHorzMirror
      end
      object N15: TMenuItem
        Action = actVertMirror
      end
      object N90L1: TMenuItem
        Action = actLevorotation90
      end
      object N90D1: TMenuItem
        Action = actDextrorotation90
      end
      object N1801: TMenuItem
        Action = actRotate180
      end
    end
    object miView: TMenuItem
      Caption = #26597#30475'(&V)'
      object B7: TMenuItem
        Action = actSetAutoBrowse
      end
      object N35: TMenuItem
        Caption = '-'
      end
      object S1: TMenuItem
        Action = actSearchSubDir
        AutoCheck = True
      end
      object D1: TMenuItem
        Action = actSearchHiddenDir
        AutoCheck = True
      end
      object O1: TMenuItem
        Action = actShowRecentMenu
        AutoCheck = True
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object L2: TMenuItem
        Action = actStatusBar
        AutoCheck = True
      end
      object N38: TMenuItem
        Caption = '-'
      end
      object Q1: TMenuItem
        Action = actQuickShow
        AutoCheck = True
      end
      object G4: TMenuItem
        Action = actDebugMode
        AutoCheck = True
      end
      object T1: TMenuItem
        Action = actFitView
        AutoCheck = True
      end
      object R4: TMenuItem
        Action = actMarkRotateInfo
        AutoCheck = True
      end
      object N34: TMenuItem
        Caption = '-'
      end
      object I1: TMenuItem
        Action = actShowPicTip
        AutoCheck = True
      end
    end
    object miHelp: TMenuItem
      Caption = #24110#21161'(&H)'
      object A1: TMenuItem
        Action = actAbout
        Hint = #20851#20110#38271#39118#22270#29255#27983#35272#22120
      end
    end
  end
  object ActionList1: TActionList
    Left = 80
    Top = 40
    object actSetAutoBrowse: TAction
      Category = 'View'
      Caption = #33258#21160#27983#35272#35774#32622'(&B)'
      Hint = #35774#32622#33258#21160#27983#35272#36873#39033
      OnExecute = actSetAutoBrowseExecute
    end
    object actRevert: TAction
      Category = 'Effect'
      Caption = #36824#21407'(&Z)'
      Hint = #36824#21407#22270#29255
      ShortCut = 16474
      OnExecute = actFitViewExecute
    end
    object actAbout: TAction
      Category = 'Help'
      Caption = #20851#20110'...(&A)'
      ShortCut = 8304
      OnExecute = actAboutExecute
    end
    object actOpenDir: TAction
      Category = 'File'
      Caption = #27983#35272'(&G)...'
      Hint = #25171#24320#22270#29255#30446#24405
      ShortCut = 16463
      OnExecute = actOpenDirExecute
    end
    object actBlurSoft: TAction
      Category = 'Effect'
      Caption = #27169#31946'/'#26580#21270'(&R)'
      OnExecute = actBlurSoftExecute
    end
    object actSharpen: TAction
      Category = 'Effect'
      Caption = #38160#21270'(&P)'
      OnExecute = actSharpenExecute
    end
    object actSearchSubDir: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #25628#32034#23376#30446#24405'(&S)'
      Checked = True
      Hint = #25628#32034#23376#30446#24405#65311
      OnExecute = actSearchSubDirExecute
    end
    object actSearchHiddenDir: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #25628#32034#38544#34255#30446#24405'(&D)'
      Checked = True
      Hint = #25628#32034#38544#34255#30446#24405#65311
      OnExecute = actSearchHiddenDirExecute
    end
    object actUpdateDir: TAction
      Category = 'DirTree'
      Caption = #26356#26032#24403#21069#30446#24405'(&U)'
      ShortCut = 121
      OnExecute = actUpdateDirExecute
    end
    object actShowRecentMenu: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #35760#20303#25171#24320#30446#24405'(&O)'
      Checked = True
      Hint = #26174#31034#26368#36817#25171#24320#30446#24405#21015#34920
      OnExecute = actShowRecentMenuExecute
    end
    object actStatusBar: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #29366#24577#26639'(&L)'
      Checked = True
      Hint = #38544#34255'/'#26174#31034#29366#24577#26639
      OnExecute = actStatusBarExecute
    end
    object actQuickShow: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #24555#36895#27169#24335'(&Q)'
      Checked = True
      Hint = #29992'GDI+'#24555#36895#26174#31034#22270#29255#65311
      ShortCut = 16468
      OnExecute = actQuickShowExecute
    end
    object actDebugMode: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #35843#35797#27169#24335'(&G)'
      Hint = #36827#20837#35843#35797#27169#24335#65311
      ShortCut = 16452
      OnExecute = actDebugModeExecute
    end
    object actFitView: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #36866#24212#31383#21475'(&T)'
      Checked = True
      Hint = #26174#31034#22270#29255#26102#25353#26174#31034#31383#21475#33258#21160#35843#25972#22823#23567#65311
      OnExecute = actFitViewExecute
    end
    object actGrayScale: TAction
      Category = 'Effect'
      Caption = #28784#24230#21270'(&G)'
      Hint = #23558#22270#29255#28784#24230#21270
      OnExecute = actGrayScaleExecute
    end
    object actSaveAs: TAction
      Category = 'File'
      Caption = #21478#23384#20026'(&S)'
      Hint = #23558#24403#21069#22270#29255#25442#21517#20445#23384
      ShortCut = 123
      OnExecute = actSaveAsExecute
    end
    object actReverseColor: TAction
      Category = 'Effect'
      Caption = #21453#33394'/'#24213#29255'(&V)'
      OnExecute = actReverseColorExecute
    end
    object actExposure: TAction
      Category = 'Effect'
      Caption = #26333#20809'(&X)'
      OnExecute = actExposureExecute
    end
    object actEmboss: TAction
      Category = 'Effect'
      Caption = #28014#38613'(&O)'
      OnExecute = actEmbossExecute
    end
    object actEngrave: TAction
      Category = 'Effect'
      Caption = #38613#21051'(&N)'
      OnExecute = actEngraveExecute
    end
    object actMergeColor: TAction
      Category = 'Effect'
      Caption = #39068#33394#35206#30422'(&M)'
      OnExecute = actMergeColorExecute
    end
    object actCover: TAction
      Category = 'Effect'
      Caption = #22270#29255#36974#32617'(&C)'
      Hint = 'F6'#38190#21487#35774#32622#22270#29255#36974#32617#31243#24230
      OnExecute = actCoverExecute
    end
    object actCoverPercent: TAction
      Category = 'Effect'
      Caption = #36974#32617#31243#24230
      ShortCut = 117
      OnExecute = actCoverPercentExecute
    end
    object actAdjRGB: TAction
      Category = 'Adjust'
      Caption = #19977#21407#33394'(&I)'
      Hint = #35843#33410#19977#21407#33394
      OnExecute = actAdjRGBExecute
    end
    object actAdjHue: TAction
      Category = 'Adjust'
      Caption = #33394#35843'/'#33394#30456'(&H)'
      Hint = #35843#25972#33394#35843'/'#33394#30456
      OnExecute = actAdjHueExecute
    end
    object actAdjBrightness: TAction
      Category = 'Adjust'
      Caption = #20142#24230'(&B)'
      Hint = #35843#33410#20142#24230
      OnExecute = actAdjBrightnessExecute
    end
    object actAdjBrightness2: TAction
      Category = 'Adjust'
      Caption = #20142#24230'(&2)'
      Hint = #21478#19968#31181#20142#24230#35843#33410'('#26469#33258'PhotoShop?)'
      OnExecute = actAdjBrightness2Execute
    end
    object actAdjSaturation: TAction
      Category = 'Adjust'
      Caption = #39281#21644#24230'(&T)'
      Hint = #35843#33410#39281#21644#24230
      OnExecute = actAdjSaturationExecute
    end
    object actEnhanceContrast: TAction
      Category = 'Adjust'
      Caption = #22686#24378#23545#27604#24230'(&E)'
      OnExecute = actEnhanceContrastExecute
    end
    object actWeakenContrast: TAction
      Category = 'Adjust'
      Caption = #20943#24369#23545#27604#24230'(&W)'
      OnExecute = actWeakenContrastExecute
    end
    object actHorzMirror: TAction
      Category = 'Adjust'
      Caption = #27700#24179#38236#35937'(&H)'
      OnExecute = actHorzMirrorExecute
    end
    object actVertMirror: TAction
      Category = 'Adjust'
      Caption = #22402#30452#38236#35937'(&V)'
      OnExecute = actVertMirrorExecute
    end
    object actLevorotation90: TAction
      Category = 'Adjust'
      Caption = #24038#26059'90'#176'(&L)'
      Hint = #21521#24038#26059#36716'('#36870#26102#38024')90'#176
      ShortCut = 16421
      OnExecute = actLevorotation90Execute
    end
    object actDextrorotation90: TAction
      Category = 'Adjust'
      Caption = #21491#26059'90'#176'(&D)'
      Hint = #21521#21491#26059#36716'('#39034#26102#38024')90'#176
      ShortCut = 16423
      OnExecute = actDextrorotation90Execute
    end
    object actWoodcut: TAction
      Category = 'Effect'
      Caption = #26408#21051'(&U)'
      Hint = #40657#30333#26408#21051#30011#25928#26524
      OnExecute = actWoodcutExecute
    end
    object actWatercolor: TAction
      Category = 'Effect'
      Caption = #27700#24425#30011'(&J)'
      OnExecute = actWatercolorExecute
    end
    object actCanvasOut: TAction
      Category = 'Effect'
      Caption = #27833#30011'(&Y)'
      OnExecute = actCanvasOutExecute
    end
    object actLight: TAction
      Category = 'Effect'
      Caption = #28783#20809'(&L)'
      OnExecute = actLightExecute
    end
    object actBrightDark: TAction
      Category = 'Effect'
      Caption = #26126#26263'(&D)'
      OnExecute = actBrightDarkExecute
    end
    object actRotate180: TAction
      Category = 'Adjust'
      Caption = #26059#36716'180'#176'(&K)'
      ShortCut = 16424
      SecondaryShortCuts.Strings = (
        'Ctrl+Up')
      OnExecute = actRotate180Execute
    end
    object actMarkRotateInfo: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #35760#20303#26059#36716#20301#32622'(&R)'
      Checked = True
      Hint = #26059#36716#22270#29255#26102#35760#20303#26059#36716#20301#32622'('#35282#24230')?'
      OnExecute = actMarkRotateInfoExecute
    end
    object actSaveCurrPic: TAction
      Category = 'File'
      Caption = #24403#21069#23384#20026'(&K)'
      Hint = #23558#24403#21069#26174#31034#22823#23567#22270#20687#20445#23384
      ShortCut = 122
      Visible = False
      OnExecute = actSaveCurrPicExecute
    end
    object actLastPicture: TAction
      Category = 'File'
      Caption = #19978#19968#22270#20687'(&L)'
      Hint = #26174#31034#19978#19968#24352#22270#20687
      ShortCut = 115
      OnExecute = actLastPictureExecute
    end
    object actNextPicture: TAction
      Category = 'File'
      Caption = #19979#19968#22270#20687'(&N)'
      Hint = #26174#31034#19979#19968#24352#22270#20687
      ShortCut = 116
      OnExecute = actNextPictureExecute
    end
    object actAdjSize: TAction
      Category = 'Adjust'
      Caption = #35843#25972#22823#23567'(&A)'
      Hint = #35843#25972#24403#21069#22270#29255#22823#23567
      ShortCut = 16458
      OnExecute = actAdjSizeExecute
    end
    object actQuickGif: TAction
      Category = 'Extra'
      Caption = #21152#24555#36895#24230'(&K)'
      Hint = #21152#24555'GIF'#21160#30011#26174#31034#36895#24230
      ShortCut = 16459
      OnExecute = actQuickGifExecute
    end
    object actSlowGif: TAction
      Category = 'Extra'
      Caption = #20943#24930#36895#24230'(&M)'
      Hint = #20943#24930'GIF'#21160#30011#26174#31034#36895#24230
      ShortCut = 16461
      OnExecute = actSlowGifExecute
    end
    object actOriginGif: TAction
      Category = 'Extra'
      Caption = #21407#22987#36895#24230'(&U)'
      Hint = #21407#22987#36895#24230#26174#31034'GIF'#21160#30011
      ShortCut = 16473
      OnExecute = actOriginGifExecute
    end
    object actShowPicTip: TAction
      Category = 'View'
      AutoCheck = True
      Caption = #26174#31034#22270#29255#35814#24773'(&I)'
      Checked = True
      Hint = #32553#30053#22270#21015#34920#26174#31034#22270#29255#35814#32454#23646#24615
      OnExecute = actShowPicTipExecute
    end
    object actAutoBrowse: TAction
      Category = 'File'
      AutoCheck = True
      Caption = #33258#21160#27983#35272'(&R)'
      Hint = #33258#21160#27983#35272#22270#29255
      ShortCut = 16450
      OnExecute = actAutoBrowseExecute
    end
    object actFileProp: TAction
      Category = 'Other'
      Caption = #24403#21069#25991#20214#23646#24615'(&F)'
      Hint = #26174#31034#24403#21069#25991#20214#30340#31995#32479#23646#24615
      OnExecute = actFilePropExecute
    end
    object actEditExif: TAction
      Category = 'Other'
      Caption = #32534#36753#22270#29255#23646#24615'(&G)'
      Hint = #32534#36753#22270#29255'Exif'#23646#24615
      ShortCut = 113
      OnExecute = actEditExifExecute
    end
  end
  object ImageList1: TImageList
    Height = 80
    Width = 67
    Left = 512
    Top = 136
  end
  object pmDirTree: TPopupMenu
    Left = 48
    Top = 88
    object U1: TMenuItem
      Action = actUpdateDir
    end
    object pmiShellOpenDir: TMenuItem
      Caption = #25171#24320#24403#21069#30446#24405'(&O)'
      Hint = #29992#36164#28304#31649#29702#22120#25171#24320#24403#21069#30446#24405
      OnClick = pmiShellOpenDirClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object S2: TMenuItem
      Action = actSearchSubDir
      AutoCheck = True
    end
    object D2: TMenuItem
      Action = actSearchHiddenDir
      AutoCheck = True
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object pmiDirTreeColor: TMenuItem
      Caption = #30446#24405#26641#32972#26223#33394'(&B)'
      Hint = #35774#32622#30446#24405#26641#32972#26223#33394
      OnClick = pmiDirTreeColorClick
    end
    object N29: TMenuItem
      Caption = '-'
    end
    object pmiDirProp: TMenuItem
      Caption = #23646#24615'(&P)'
      Hint = #26174#31034#24403#21069#30446#24405#30340#31995#32479#23646#24615
      OnClick = pmiDirPropClick
    end
  end
  object pmPreview: TPopupMenu
    Left = 544
    Top = 136
    object pmiShellOpen: TMenuItem
      Caption = #25171#24320'(&O)'
      Default = True
      Hint = #29992#40664#35748#20851#32852#31243#24207#25171#24320
      OnClick = pmiShellOpenClick
    end
    object pmiShellEdit: TMenuItem
      Caption = #32534#36753'(&E)'
      Hint = #29992#40664#35748#20851#32852#31243#24207#32534#36753
      OnClick = pmiShellEditClick
    end
    object pmiOpenAs: TMenuItem
      Caption = #25171#24320#26041#24335'...(&A)'
      Hint = #29992#26576#20010#31243#24207#25171#24320#22270#29255
      OnClick = pmiOpenAsClick
    end
    object R8: TMenuItem
      Action = actAutoBrowse
      AutoCheck = True
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object pmiDelFile: TMenuItem
      Caption = #21024#38500'(&D)'
      Hint = #21024#38500#24403#21069#25991#20214'('#25353#20303'Shift'#38190#24443#24213#21024#38500')'
      OnClick = pmiDelFileClick
    end
    object N36: TMenuItem
      Caption = '-'
    end
    object I2: TMenuItem
      Action = actShowPicTip
      AutoCheck = True
    end
    object G3: TMenuItem
      Action = actEditExif
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object pmiMultiProps: TMenuItem
      Caption = #20840#37096#25991#20214#20449#24687'(&M)'
      Hint = #26174#31034#20840#37096#22270#29255#25991#20214#30340#32479#35745#20449#24687
      OnClick = pmiMultiPropsClick
    end
    object pmiFileProp: TMenuItem
      Action = actFileProp
    end
  end
  object pmGraphic: TPopupMenu
    Left = 304
    Top = 136
    object R2: TMenuItem
      Action = actRevert
    end
    object A5: TMenuItem
      Action = actAdjSize
    end
    object pmiExtraProcess: TMenuItem
      Caption = #38468#21152#22788#29702'(&F)'
      object K1: TMenuItem
        Action = actQuickGif
      end
      object M3: TMenuItem
        Action = actSlowGif
      end
      object N33: TMenuItem
        Caption = '-'
      end
      object U2: TMenuItem
        Action = actOriginGif
      end
    end
    object N32: TMenuItem
      Caption = '-'
    end
    object R6: TMenuItem
      Action = actSaveCurrPic
    end
    object A3: TMenuItem
      Action = actSaveAs
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object S4: TMenuItem
      Action = actBlurSoft
    end
    object P2: TMenuItem
      Action = actSharpen
    end
    object G2: TMenuItem
      Action = actGrayScale
    end
    object V2: TMenuItem
      Action = actReverseColor
    end
    object B3: TMenuItem
      Action = actExposure
    end
    object N26: TMenuItem
      Caption = '-'
    end
    object B4: TMenuItem
      Action = actEmboss
    end
    object N10: TMenuItem
      Action = actEngrave
    end
    object W4: TMenuItem
      Action = actWoodcut
    end
    object N21: TMenuItem
      Action = actWatercolor
    end
    object N22: TMenuItem
      Action = actCanvasOut
    end
    object N23: TMenuItem
      Action = actLight
    end
    object N27: TMenuItem
      Caption = '-'
    end
    object M2: TMenuItem
      Action = actMergeColor
    end
    object C2: TMenuItem
      Action = actCover
    end
    object N20: TMenuItem
      Action = actBrightDark
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object C4: TMenuItem
      Action = actAdjRGB
    end
    object H2: TMenuItem
      Action = actAdjHue
    end
    object B6: TMenuItem
      Action = actAdjBrightness
    end
    object N211: TMenuItem
      Action = actAdjBrightness2
    end
    object S6: TMenuItem
      Action = actAdjSaturation
    end
    object E2: TMenuItem
      Action = actEnhanceContrast
    end
    object W2: TMenuItem
      Action = actWeakenContrast
    end
    object N37: TMenuItem
      Caption = '-'
    end
    object pmiGraphicProp: TMenuItem
      Caption = #24403#21069#25991#20214#23646#24615'(&Q)'
      Hint = #26174#31034#24403#21069#25991#20214#30340#31995#32479#23646#24615
      OnClick = pmiGraphicPropClick
    end
  end
  object dlgSavePic: TSavePictureDialog
    DefaultExt = 'jpg'
    Filter = 'JPEG'#22270#29255'(*.jpg)|*.jpg|24'#20301#33394#20301#22270'(*.bmp)|*.bmp'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    OnTypeChange = dlgSavePicTypeChange
    Left = 48
    Top = 136
  end
  object ColorDialog1: TColorDialog
    OnShow = ColorDialog1Show
    Options = [cdFullOpen]
    Left = 80
    Top = 136
  end
  object tmGifShow: TTimer
    Enabled = False
    OnTimer = tmGifShowTimer
    Left = 236
    Top = 136
  end
  object tmAutoBrowse: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = tmAutoBrowseTimer
    Left = 268
    Top = 136
  end
end
