object frmDblView: TfrmDblView
  Left = 0
  Top = 0
  Width = 430
  Height = 239
  TabOrder = 0
  object pnView: TPanel
    Left = 0
    Top = 0
    Width = 430
    Height = 239
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lbBeforeAdj: TLabel
      Left = 84
      Top = 216
      Width = 42
      Height = 12
      Caption = #35843#25972#21069' '
    end
    object lbAfterAdj: TLabel
      Left = 298
      Top = 216
      Width = 42
      Height = 12
      Caption = #35843#25972#21518' '
    end
    object pnBeforeAdj: TPanel
      Left = 6
      Top = 6
      Width = 203
      Height = 203
      BevelOuter = bvLowered
      TabOrder = 0
      object pbBeforeAdj: TPaintBox
        Left = 48
        Top = 40
        Width = 105
        Height = 105
        OnPaint = pbBeforeAdjPaint
      end
    end
    object pnAfterAdj: TPanel
      Left = 221
      Top = 6
      Width = 203
      Height = 203
      BevelOuter = bvLowered
      TabOrder = 1
      object pbAfterAdj: TPaintBox
        Left = 40
        Top = 40
        Width = 105
        Height = 105
        OnPaint = pbAfterAdjPaint
      end
    end
  end
end
