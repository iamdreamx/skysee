{*****************************************************************************
*                    SkySee -- 长风图片浏览器                                *
*                                                                            *
* 功能: 以缩略图+预览方式查看大量图片                                        *
* 版本: 1.0                                                                  *
* 作者: 顾中军                                                               *
* 实现:                                                                      *
*     图片浏览器可以说已经“泛滥成灾”了，网上到处都是，那么我为什么还要自己 *
* 写一个呢？因为专业一些的图片浏览器如ACDSee之类对我来说功能太多，实际上绝大 *
* 多数时间我只是想顺序浏览一下数码照片而已；而很多开放源代码的浏览器似乎又不 *
* 太符合我的看图习惯，呵呵，正好想学一下GDI+的使用，于是就自己动手写了一个。 *
*     以下粗略的实现与改进过程。                                             *
*     2010.9  初步实现缩略图+预览方式查看功能                                *
*     2011.2~2011.5 改进缩略图显示方法以提高响应速度，修正一些错误           *
*     2012.2~2012.4 显示缩略图的ListView部分独立出来，并改用Virtual方式，提  *
* 高显示速度；搜索目录及文件也改用Thread，不但加快软件响应速度，也可以随时快 *
* 速切换目录（即使目录未搜索完）                                             *
*     2012.12 改进代码，以便可以在Unicode版本的Delphi下编译，测试了D2010，编 *
* 译运行正常，加上简单的多语言支持                                           *
*     2013.1~2019.5 一些小的改进，包括PNG/GIF图片显示的改进，以及用高版本    *
* Delphi编辑时缩略图大小不对的改进                                           *
*     2021.3.9~2021.3.14 改进代码以支持高清屏，并在高清屏下显示大一些的缩略图*
*                                                                            *
* 说明:                                                                      *
*     本软件代码简单，小软件嘛，简单才是美。                                 *
*     SkySeeV.dpr/SkySeeV.bdsproj/SkySeeV.dproj是采用Virtual ListView+Thread *
* 方式搜索目录实现的，速度最快；                                             *
*     SkySeeD.dpr/SkySeeD.bdsproj/SkySeeD.dproj是采用ListView默认绘制+Thread *
* 方式搜索目录实现的，速度次之，但切换目录的响应速度未有改善；               *
*     SkySeeO.dpr/SkySeeO.bdsproj/SkySeeO.dproj是采用ListView默认绘制+事件触 *
* 发方式搜索目录实现的，速度最慢（可将它与SkySee.dpr/bdsproj/dproj比较，查看 *
* 改进痕迹）。                                                               *
*     SkySee.zip是所有源代码，可直接打开dpr(D7)/bdsproj(D2007)/dproj(D2010)  *
* 编译；SkySeeExe.zip是编译好的程序，可直接运行；SkySeeBugMore.txt记录了一些 *
* 已发现但尚未解决的一些问题，以及一些未来可能会改进实现的想法。             *
*     注意：如果你在D2007/D2010的高版本Delphi环境中打开dpr工程文件，则可能无 *
* 法直接编译，这是因为高版本Delphi忽略了cfg/dof中的一些选项。你需要在工程的  *
* Options中找到Unit output directory/Search path/Conditional defines这些选项 *
* 并重新设置。                                                               *
*                                                                            *
* 版权声明:                                                                  *
*     源码公开，你可以将它用于任何场合；                                     *
*     如果你有更好的改进，别忘了给我发一份啊；                               *
*     此外，如果你是在它的基础上改进，请保留我写的文本及说明，毕竟我花了不少 *
* 精力和时间，请尊重我的劳动成果；另外我希望你能将改进的代码公布出来，多交流 *
* 才能更快地进步嘛；                                                         *
*     最后，愿与所有喜爱软件开发的朋友们共勉：让世界因软件而变得更美好！     *
*                                                                            *
* 博客：http://dreamisx.blog.163.com/                                        *
* 妹儿：iamdream@yeah.net                                                    *
*****************************************************************************}

unit ssMainX;

interface

{$WARN UNIT_PLATFORM OFF}  //避免出现 FileCtrl is platform... 的警告
{$WARN SYMBOL_PLATFORM OFF}

{$IFDEF VER150}            //取消D7警告
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$ENDIF}

  {At the XE4 release, the Delphi compilers were changed to accept either $IFEND
   or $ENDIF to close $IF statements. Before XE4, only $IFEND could be used to
   close $IF statements.
  The $LEGACYIFEND directive allows you to restore the old behavior, which is
   useful if your code is emitting E2029 related to nested $IF and $IFDEF
   statements.}
  {.$IF CompilerVersion >= 25.0} //Delphi XE4+
  {$IFDEF LEGACYIFEND}
  {$LEGACYIFEND ON}
  {$ENDIF}
  {.$IFEND}

  {$IFDEF conditionalexpressions}
  {$IF CompilerVersion >= 22.0} //Delphi XE+
    {$DEFINE DelphiXEUp}
  {$IFEND}
  {$IF CompilerVersion >= 23.0} //Delphi XE2+
    {$DEFINE  DelphiXE2Up}
  {$IFEND}
  {$IF CompilerVersion >= 24.0} //Delphi XE3+
    {$DEFINE  DelphiXE3Up}
  {$IFEND}
  {$IF CompilerVersion >= 31.0} //Delphi Berlin
    {$DEFINE  DelphiBerlin10Dot1Up}
  {$IFEND}
  {$IF CompilerVersion >= 33.0} //Delphi Rio
    {$DEFINE  DelphiRio10Dot3Up}
  {$IFEND}
  {$ENDIF}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, ExtCtrls, ComCtrls, SkyDirFileTree, ToolWin,
  ImgList, ExtDlgs, StdCtrls, ActiveX, ValEdit,
  IniFiles, Jpeg, Math, ShellApi, Contnrs, SyncObjs, FileCtrl,
  {$IFDEF Virtual}
  SkyThumbViewVirtual
  {$ELSE}
    {$IFDEF DefDraw}
  SkyThumbViewDefDraw
    {$ELSE}
  SkyThumbViewOrigin  
    {$ENDIF}
  {$ENDIF},
  SkyZipPlugIn, GdiPObj, GdipAPI, PicFun, SelDirEx, SkyPub, ShellFun,
  MultiFileProps, MyGraphic, MyGifPub, MyExifPub,
  ssAdjRGB, ssAdjHue, ssAdjBrightness, ssAdjSaturation, ssAdjSize,
  ssLightParam, ssAbout, ssSetAutoBrowse, ssEditProp, SkyTranslator;

{-----------------------------------------------------------------------------
  Main Form
-----------------------------------------------------------------------------}

const
  RECENT_COUNT = 16;  //最近打开目录缓存数    2011.05.26  (0~9,A~F)

type
  TChangeProc = procedure (const Bmp: TBitmap);
  TChangeObjProc = procedure (const Bmp: TBitmap) of object;

type
  TfMain = class(TForm)
    MainMenu1: TMainMenu;
    ActionList1: TActionList;
    miFile: TMenuItem;
    miHelp: TMenuItem;
    actAbout: TAction;
    miExit: TMenuItem;
    A1: TMenuItem;
    SkyDirFileTree1: TSkyDirFileTree;
    ScrollBox1: TScrollBox;
    splLeft: TSplitter;
    splRight: TSplitter;
    PaintBox1: TPaintBox;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    tbtTest: TToolButton;
    ImageList1: TImageList;
    actOpenDir: TAction;
    N1: TMenuItem;
    B1: TMenuItem;
    miView: TMenuItem;
    actSearchSubDir: TAction;
    S1: TMenuItem;
    pmDirTree: TPopupMenu;
    S2: TMenuItem;
    actSearchHiddenDir: TAction;
    D1: TMenuItem;
    D2: TMenuItem;
    actUpdateDir: TAction;
    U1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    pmiDirProp: TMenuItem;
    pmPreview: TPopupMenu;
    pmiFileProp: TMenuItem;
    Image1: TImage;
    pmiOpenAs: TMenuItem;
    N4: TMenuItem;
    pmiShellOpen: TMenuItem;
    N5: TMenuItem;
    pmiDelFile: TMenuItem;
    actFitView: TAction;
    N6: TMenuItem;
    T1: TMenuItem;
    miEffect: TMenuItem;
    actGrayScale: TAction;
    G1: TMenuItem;
    pmGraphic: TPopupMenu;
    G2: TMenuItem;
    actRevert: TAction;
    R1: TMenuItem;
    N7: TMenuItem;
    actSaveAs: TAction;
    A2: TMenuItem;
    R2: TMenuItem;
    N8: TMenuItem;
    A3: TMenuItem;
    actBlurSoft: TAction;
    actSharpen: TAction;
    actReverseColor: TAction;
    actExposure: TAction;
    actEmboss: TAction;
    actEngrave: TAction;
    S3: TMenuItem;
    P1: TMenuItem;
    V1: TMenuItem;
    X1: TMenuItem;
    B2: TMenuItem;
    N9: TMenuItem;
    actMergeColor: TAction;
    actCover: TAction;
    M1: TMenuItem;
    C1: TMenuItem;
    S4: TMenuItem;
    P2: TMenuItem;
    V2: TMenuItem;
    B3: TMenuItem;
    B4: TMenuItem;
    N10: TMenuItem;
    M2: TMenuItem;
    C2: TMenuItem;
    miAdjust: TMenuItem;
    actAdjRGB: TAction;
    actAdjBrightness: TAction;
    actAdjSaturation: TAction;
    actEnhanceContrast: TAction;
    actWeakenContrast: TAction;
    C3: TMenuItem;
    B5: TMenuItem;
    S5: TMenuItem;
    E1: TMenuItem;
    W1: TMenuItem;
    N11: TMenuItem;
    C4: TMenuItem;
    B6: TMenuItem;
    S6: TMenuItem;
    E2: TMenuItem;
    W2: TMenuItem;
    dlgSavePic: TSavePictureDialog;
    R3: TMenuItem;
    N12: TMenuItem;
    ColorDialog1: TColorDialog;
    actHorzMirror: TAction;
    actVertMirror: TAction;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    actLevorotation90: TAction;
    actDextrorotation90: TAction;
    N90L1: TMenuItem;
    N90D1: TMenuItem;
    actWoodcut: TAction;
    actWatercolor: TAction;
    actCanvasOut: TAction;
    actLight: TAction;
    actBrightDark: TAction;
    W3: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    W4: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    actCoverPercent: TAction;
    actRotate180: TAction;
    N1801: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    pmiDirTreeColor: TMenuItem;
    actMarkRotateInfo: TAction;
    R4: TMenuItem;
    miRecent_0: TMenuItem;
    miRecent_1: TMenuItem;
    miRecent_2: TMenuItem;
    miRecent_3: TMenuItem;
    miRecent_4: TMenuItem;
    miRecent_5: TMenuItem;
    miRecent_6: TMenuItem;
    miRecent_7: TMenuItem;
    miRecent_8: TMenuItem;
    miRecent_9: TMenuItem;
    miRecent_A: TMenuItem;
    miRecent_B: TMenuItem;
    miRecent_C: TMenuItem;
    miRecent_D: TMenuItem;
    miRecent_E: TMenuItem;
    miRecent_F: TMenuItem;
    actLastPicture: TAction;
    actNextPicture: TAction;
    L1: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    pmiShellOpenDir: TMenuItem;
    pmiMultiProps: TMenuItem;
    actShowRecentMenu: TAction;
    O1: TMenuItem;
    tmGifShow: TTimer;
    actSaveCurrPic: TAction;
    R5: TMenuItem;
    R6: TMenuItem;
    actAdjSize: TAction;
    A4: TMenuItem;
    A5: TMenuItem;
    N32: TMenuItem;
    pmiExtraProcess: TMenuItem;
    actQuickGif: TAction;
    actSlowGif: TAction;
    actOriginGif: TAction;
    K1: TMenuItem;
    M3: TMenuItem;
    U2: TMenuItem;
    N33: TMenuItem;
    actShowPicTip: TAction;
    I1: TMenuItem;
    N34: TMenuItem;
    pmiShellEdit: TMenuItem;
    tmAutoBrowse: TTimer;
    actAutoBrowse: TAction;
    R7: TMenuItem;
    R8: TMenuItem;
    actSetAutoBrowse: TAction;
    B7: TMenuItem;
    N35: TMenuItem;
    I2: TMenuItem;
    N36: TMenuItem;
    lbErrMsg: TLabel;
    N37: TMenuItem;
    actFileProp: TAction;
    pmiGraphicProp: TMenuItem;
    actStatusBar: TAction;
    L2: TMenuItem;
    N38: TMenuItem;
    actEditExif: TAction;
    G3: TMenuItem;
    actQuickShow: TAction;
    Q1: TMenuItem;
    actDebugMode: TAction;
    G4: TMenuItem;
    actAdjBrightness2: TAction;
    N210: TMenuItem;
    N211: TMenuItem;
    actAdjHue: TAction;
    H1: TMenuItem;
    H2: TMenuItem;
    { Form Events }
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    { Normal Action & Control Events }
    procedure miRecentClick(Sender: TObject);
    procedure miExitClick(Sender: TObject);
    procedure actOpenDirExecute(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure SkyDirFileTree1Change(Sender: TObject; Node: TTreeNode);
    procedure ThumbViewSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure actSearchSubDirExecute(Sender: TObject);
    procedure actSearchHiddenDirExecute(Sender: TObject);
    procedure actShowRecentMenuExecute(Sender: TObject);
    procedure actFitViewExecute(Sender: TObject);
    procedure actMarkRotateInfoExecute(Sender: TObject);
    procedure actShowPicTipExecute(Sender: TObject);
    procedure actSetAutoBrowseExecute(Sender: TObject);
    procedure actStatusBarExecute(Sender: TObject);
    procedure actQuickShowExecute(Sender: TObject);
    procedure actDebugModeExecute(Sender: TObject);
    procedure actFilePropExecute(Sender: TObject);
    procedure pmiGraphicPropClick(Sender: TObject);
    procedure actEditExifExecute(Sender: TObject);
    procedure actUpdateDirExecute(Sender: TObject);
    procedure actAboutExecute(Sender: TObject);
    procedure pmiDirTreeColorClick(Sender: TObject);
    procedure pmiShellOpenDirClick(Sender: TObject);
    procedure pmiDirPropClick(Sender: TObject);
    procedure pmiMultiPropsClick(Sender: TObject);
    procedure ScrollBox1Resize(Sender: TObject);
    procedure pmiShellOpenClick(Sender: TObject);
    procedure pmiShellEditClick(Sender: TObject);
    procedure pmiOpenAsClick(Sender: TObject);
    procedure pmiDelFileClick(Sender: TObject);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ScrollBox1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure dlgSavePicTypeChange(Sender: TObject);
    procedure ColorDialog1Show(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure actSaveCurrPicExecute(Sender: TObject);
    procedure actAutoBrowseExecute(Sender: TObject);
    procedure actLastPictureExecute(Sender: TObject);
    procedure actNextPictureExecute(Sender: TObject);
    procedure tmGifShowTimer(Sender: TObject);
    procedure tmAutoBrowseTimer(Sender: TObject);
    { GIF Image Extra Process }
    procedure actQuickGifExecute(Sender: TObject);
    procedure actSlowGifExecute(Sender: TObject);
    procedure actOriginGifExecute(Sender: TObject);
    { Image Process Events }
    procedure actBlurSoftExecute(Sender: TObject);
    procedure actSharpenExecute(Sender: TObject);
    procedure actGrayScaleExecute(Sender: TObject);
    procedure actReverseColorExecute(Sender: TObject);
    procedure actExposureExecute(Sender: TObject);
    procedure actEmbossExecute(Sender: TObject);
    procedure actEngraveExecute(Sender: TObject);
    procedure actMergeColorExecute(Sender: TObject);
    procedure actCoverExecute(Sender: TObject);
    procedure actCoverPercentExecute(Sender: TObject);
    procedure actAdjRGBExecute(Sender: TObject);
    procedure actAdjHueExecute(Sender: TObject);
    procedure actAdjBrightnessExecute(Sender: TObject);
    procedure actAdjBrightness2Execute(Sender: TObject);
    procedure actAdjSaturationExecute(Sender: TObject);
    procedure actEnhanceContrastExecute(Sender: TObject);
    procedure actWeakenContrastExecute(Sender: TObject);
    procedure actHorzMirrorExecute(Sender: TObject);
    procedure actVertMirrorExecute(Sender: TObject);
    procedure actLevorotation90Execute(Sender: TObject);
    procedure actDextrorotation90Execute(Sender: TObject);
    procedure actRotate180Execute(Sender: TObject);
    procedure actWoodcutExecute(Sender: TObject);
    procedure actWatercolorExecute(Sender: TObject);
    procedure actCanvasOutExecute(Sender: TObject);
    procedure actLightExecute(Sender: TObject);
    procedure actBrightDarkExecute(Sender: TObject);
    procedure actAdjSizeExecute(Sender: TObject);
  private
    { Private declarations }
    FThumbView: TSkyThumbView;

    FHasOwner:  Boolean;  //用于区分启动模式(是不是正常模式)?
    FSearching: Boolean;
    FStretched: Boolean;
    FNoteProps: Boolean;  //重新加载图片时记录图片大小信息?
    FScaleAdj:  Boolean;  //按比例调整大小?
    FMaximized: Boolean;  //上次退出时是最大化状态？

    FCapInfo: string;
    FLastPicDir: string;
    FLastSelImage: string;
    FLastImgFile:  string;
    FListViewWidth: Integer;
    FColorDlgTitle: string;
    FLastPicWidth:  Integer;
    FLastPicHeight: Integer;

    FZipperNameOrders: string;
    FTreeInitPath: string;
    FTreeHScrollPos: Integer;

    FDrawBmp: TBitmap;
    FRealBmp: TBitmap;    //图片大小超过显示区域时存放原始大小的位图 2011.5.25
    FImageExts: TStringList;

    FGifBitmap:  TGpBitmap;
    FGifFrames:  Integer;
    FGifIndex:   Integer;
    FGifSpeed:   Double;  //用于加快或减慢GIF的显示速度 2011.11.6
    FFrameTimes: array of Cardinal;
    FRotateAngle: Integer;//图片旋转角度
    FOrientation: Integer;//图片拍摄方向 (1..8)

    FOrientationTxts: array[2..8] of string;
    FLanguageDir: string;

    FLeftDown: Boolean;
    FMousePos: TPoint;

    FLastMergeColor: TColor;
    FLastCoverColor: TColor;
    FCoverPercent: TPercent;

    FLightX: Integer;     //灯光X坐标
    FLightY: Integer;     //灯光Y坐标
    FLightM: Integer;     //灯光M参数
    FLightN: Integer;     //灯光N参数

    FCycleBrowse: Boolean;   //循环浏览?

    FRecentMenus: array[0..RECENT_COUNT -1]of TMenuItem; //最近打开目录菜单项

    { Configuration }
    procedure LoadConfig;
    procedure SaveConfig;
    { App functions }
    procedure AppMessage(var Msg: TMsg; var Handled: Boolean);
    { Init functions }
    procedure InitViews;
    procedure InitTerminate(Sender: TObject);
    { Image functions }
    procedure FindDir(const sDir: string);
    procedure FindDirComplete(const ImageDir: string; FileCount: Integer);
    procedure ClearImageProps;  // call when find error
    procedure ShowImageProps(PicWidth, PicHeight: Integer;
      UpdateModifyTime: Boolean = True);
    procedure AdjustSizeForTiff(gpBmp: TGpBitmap);
    procedure LoadByGdiImage(const sImageFile: string);
    procedure LoadByVclImage(const sImageFile: string);
    procedure LoadImageAsBmp(const sImageFile: string);
    procedure JustLoadByGdiImage(const ImageFile: string; Bmp: TBitmap);
    procedure JustLoadByVclImage(const ImageFile: string; Bmp: TBitmap);
    procedure JustLoadImageToBmp(const ImageFile: string; Bmp: TBitmap);
    procedure QuickLoadByGdiImage(const sImageFile: string);
    procedure ShowSelectImage(const sDir, sImageName: string);
    procedure ExtraForGifImage(const ACanvas: TCanvas; const ImageFile: string);
    procedure GetExifInfo(HasExifInfo: Boolean; const EquipMake, EquipModel,
      ExposureTime, FocalLength, DTOrigin: string; Orientation: Integer);
    { Other Show & Change functions }
    procedure ChangeDir(const sDir: string);
    procedure LocateImage(const sImageName: string);
    procedure LocateComplete(const ImageName: string; ImageIndex: Integer);
    procedure AdjustPaintBoxSize(iPicWidth, iPicHeight: Integer);
    procedure AdjustRotateInfo(const ImageFile: string; RotateAngle: Integer);
    procedure ChangeBmp(ChangeProc: TChangeProc); overload;
    procedure ChangeBmp(ChangeObjProc: TChangeObjProc); overload;
    procedure DoMergeColor(const Bmp: TBitmap);
    procedure DoCoverWithColor(const Bmp: TBitmap);
    procedure DoCanvasOut(const Bmp: TBitmap);
    { Recent Menu Item functions }
    function FindMenuItem(Item: TMenuItem; sName: string): TMenuItem;
    procedure InitRecentMenuItems;
    procedure SetRecentMenuItem(Item: TMenuItem; idx: integer;
      const Dir: string);
    procedure AddLastDir(const Dir: string);
  public
    { Public declarations }
  end;

var
  fMain: TfMain;

implementation

{$R *.dfm}
{$R SkyThumbView.res}

{-----------------------------------------------------------------------------
  DLL Interface
-----------------------------------------------------------------------------}

  {$IFDEF UNICODE}
  function OpenAs_RunDLL(const h: hwnd; b: hwnd; const filename: pWideChar;
          sw: integer = SW_SHOW): integer; stdcall;
            external 'shell32.dll' name 'OpenAs_RunDLLW';
  {$ELSE}
  function OpenAs_RunDLL(const h: hwnd; b: hwnd; const filename: pAnsiChar;
          sw: integer = SW_SHOW): integer; stdcall;
            external 'shell32.dll' name 'OpenAs_RunDLLA';
  {$ENDIF}

{-----------------------------------------------------------------------------
  Internal Thread for init
-----------------------------------------------------------------------------}

type
  TInitThread = class(TThread)
  private
    FInitProc: TThreadMethod;
  protected
    procedure Execute; override;
  public
    constructor Create(InitProc: TThreadMethod);
  end;

{ TInitThread }

constructor TInitThread.Create(InitProc: TThreadMethod);
begin
  FInitProc := InitProc;
  FreeOnTerminate := True;
  inherited Create(True);
end;

procedure TInitThread.Execute;
begin
  Synchronize(FInitProc);
end;

{-----------------------------------------------------------------------------
  Internal function
-----------------------------------------------------------------------------}

procedure Swap(var A, B: Integer);
begin
  A := A xor B;
  B := A xor B;
  A := A xor B;
end;

procedure CalcThumbnailJpegScale(vJpeg: TJpegImage; Height, Width: Integer);
var
  iHRatio, iWRatio: Integer;
begin
  iHRatio := vJpeg.Height div Height;
  iWRatio := vJpeg.Width div Width;
  if (iWRatio >= 8) and (iHRatio >= 8) then begin
    vJpeg.Scale := jsEighth;
  end else if (iWRatio >= 4) and (iHRatio >= 4) then begin
    vJpeg.Scale := jsQuarter;
  end else if (iWRatio >= 2) and (iHRatio >= 2) and (iWRatio * iHRatio >= 8) then begin
    vJpeg.Scale := jsHalf;
  end else begin
    vJpeg.Scale := jsFullSize;
  end;
end;

function MyGetFileSize(const sFileName: string): Int64;
var
  iFileSizeHi, iFileSizeLo: DWord; //须用DWord,不能用Integer,否则Int64转换错
  hFile: THandle;
begin
  hFile := FileOpen(sFileName, fmOpenRead or fmShareDenyNone);
  iFileSizeLo := GetFileSize(hFile, @iFileSizeHi);
  Result := (Int64(iFileSizeHi) shl 32) or iFileSizeLo;
  FileClose(hFile);
end;

function MyFmtFileSize(const sFileName: string): string;
const
  cSizeUnits: array[0..2] of string = ('K', 'M', 'G');
var
  iFileSize, iLastSize: Int64;
  idxUnit: Integer;
begin
  iFileSize := MyGetFileSize(sFileName);
  Result := FormatFloat('0.#', iFileSize / 1024.0) + 'K';
  iFileSize := iFileSize shr 10;
  idxUnit := 0;
  iLastSize := 0;
  while (idxUnit +1 < Length(cSizeUnits)) and (iFileSize > 1024) do begin
    Inc(idxUnit);
    iLastSize := iFileSize;
    iFileSize := iFileSize shr 10;
  end;
  if idxUnit > 0 then begin
    Result := FormatFloat('0.###', iLastSize / 1024.0) + cSizeUnits[idxUnit];
  end;
end;

function GetFileModifyTimeStr(const sFileName: String): String;
var                //取文件之“修改时间”
  ftSelf, ftLocal: TFileTime;
  stTmp: TSystemTime;
  iFile: Integer;
begin
  Result := '';
  iFile  := SysUtils.FileOpen(sFileName, fmOpenRead);
  if (iFile >= 0) and GetFileTime(iFile, nil, nil, @ftSelf) then
  begin
    if FileTimeToLocalFileTime(ftSelf, ftLocal)
       and FileTimeToSystemTime(ftLocal, stTmp) then
    begin
      Result := Format('%.4d-%.2d-%.2d %.2d:%.2d:%.2d',
                       [stTmp.wYear, stTmp.wMonth, stTmp.wDay,
                          stTmp.wHour, stTmp.wMinute, stTmp.wSecond]);
    end;
  end;
  FileClose(iFile);
end;

function GetPureFileName(const AFileName: string): string;
var
  idx: Integer;
begin
  idx := LastDelimiter('.', AFileName);
  if idx > 0 then begin
    Result := Copy(AFileName, 1, idx -1);
  end else begin
    Result := AFileName;
  end;
end;

// TBitmap.LoadFromResourceName最大只能支持256色，所以用TResourecStream+TBitmap
procedure LoadBmpFromResToImageList(ImgList: TImageList; const AName: string);

  procedure DrawCenterBitmap(Source, Target: TBitmap);
  begin
    with Target.Canvas do begin
      Brush.Color := Source.Canvas.Pixels[0, 0];
      FillRect(ClipRect);
      Draw((Target.Width - Source.Width)div 2, (Target.Height - Source.Height)div 2, Source);
    end;
  end;

var
  res: TResourceStream;
  bmp1: TBitmap;
  bmp2: TBitmap;
  scale: Double;
begin
  res := TResourceStream.Create(HInstance, AName, 'SkyBmpData');
  bmp1 := TBitmap.Create;
  bmp2 := TBitmap.Create;
  try
    bmp1.LoadFromStream(res);
    if (bmp1.Width = ImgList.Width) and (bmp1.Height = ImgList.Height) then begin
      ImgList.AddMasked(bmp1, bmp1.Canvas.Pixels[0, 0]);
    end else begin
      if bmp1.PixelFormat <> pf24bit then begin
        bmp1.PixelFormat := pf24bit;
      end;
      bmp2.PixelFormat := pf24bit;
      bmp2.Width := ImgList.Width;
      bmp2.Height := ImgList.Height;
      if (bmp2.Width < bmp1.Width) or (bmp2.Height < bmp1.Height) then begin
        scale := Min(bmp2.Width *1.0 / bmp1.Width, bmp2.Height *1.0 / bmp1.Height);
        bmp2.Width := Round(scale * bmp1.Width);
        bmp2.Height := Round(scale * bmp1.Height);
        StretchLinear(bmp2, bmp1);
        bmp1.Width := ImgList.Width;
        bmp1.Height := ImgList.Height;
        DrawCenterBitmap(bmp2, bmp1);
        ImgList.AddMasked(bmp1, bmp1.Canvas.Pixels[0, 0]);
      end else begin
        DrawCenterBitmap(bmp1, bmp2);
        ImgList.AddMasked(bmp2, bmp2.Canvas.Pixels[0, 0]);
      end;
    end;
  finally
    bmp2.Free;
    bmp1.Free;
    res.Free;
  end;
end;

{-----------------------------------------------------------------------------
  Form Events
-----------------------------------------------------------------------------}

procedure TfMain.FormCreate(Sender: TObject);
var
  sDir: string;
begin
  FHasOwner := Assigned(Self.Owner);
  PaintBox1.Width := 0;
  PaintBox1.Height := 0;
  LoadBmpFromResToImageList(Self.ImageList1, 'SKYTHUMVIEWDEFICON');
  {$IFDEF DelphiBerlin10Dot1Up}
  if Assigned(Monitor) and (Self.Monitor.PixelsPerInch > 96) then begin
    ImageList1.SetSize(134, 160);
  end;
  {$ENDIF}
  FThumbView := TSkyThumbView.Create(Self);
  with FThumbView do begin
    Parent := Self;
    Align := alRight;
    Color  := clBtnFace;
    DoubleBuffered := True;
    LargeImages  := Self.ImageList1;
    PopupMenu    := Self.pmPreview;
    OnDblClick   := Self.pmiShellOpen.OnClick;
    OnSelectItem := ThumbViewSelectItem;
    OnFindDir    := FindDir;
    OnFindDirComplete := FindDirComplete;
    OnGetExifInfo:= GetExifInfo;
  end;
  splRight.Left := FThumbView.Left;

  FCapInfo := Caption;
  Application.Title :=
    {$IFDEF Virtual}
    Caption
    {$ELSE}
      {$IFDEF DefDraw}
    Caption + '(Default)'
      {$ELSE}
    Caption + '(Origin)'  
      {$ENDIF}
    {$ENDIF};
  InitRecentMenuItems;

  FImageExts := TStringList.Create;
  FDrawBmp := TBitmap.Create;
  FDrawBmp.PixelFormat := pf24bit;
  FRealBmp := TBitmap.Create;
  FRealBmp.PixelFormat := pf24bit;

  if FHasOwner then LoadConfig;
  FThumbView.ImageExts := Self.FImageExts;

  // init dir tree
  //SetCurrentDirectory(PChar(ExtractFileDir(ParamStr(0))));
  sDir := GetSysTempDir; // for test
  if (sDir <> '') and (sDir[Length(sDir)] <> '\') then sDir := sDir + '\';
  sDir := sDir + '~SkySee\';
  {$IFDEF UNICODE}
  {$WARN SYMBOL_DEPRECATED OFF} // for DirectoryExists
  {$ENDIF}
  ForceDirectories(sDir);
  {$IFDEF UNICODE}
  {$WARN SYMBOL_DEPRECATED ON}
  {$ENDIF}
  SkyDirFileTree1.OnChange := nil;
  SkyDirFileTree1.UnZipDir := sDir;
  SkyDirFileTree1.ZipperPath := ExtractFilePath(ParamStr(0)); //'.\';  2012.8.25
  SkyDirFileTree1.SortZipperByDllName(StringReplace(FZipperNameOrders, ',', sLineBreak, [rfReplaceAll]));

  ScrollBox1.DoubleBuffered := True;

  DragAcceptFiles(Handle,true); //支持文件拖放
  Application.OnMessage := AppMessage;

  if FHasOwner then begin
    InitTranslator(FLanguageDir);
    Translate(Self);
    actQuickShow.OnExecute(actQuickShow);
  end;
end;

procedure TfMain.FormDestroy(Sender: TObject);
begin
  SkyDirFileTree1.OnChange := nil;
  with FThumbView do begin
    LargeImages  := nil;
    OnSelectItem := nil;
    OnFindDir    := nil;
    OnFindDirComplete := nil;
    if FHasOwner then Terminate; //主要防止线程引起地址错 2012.2.24
  end;

  try
    if FHasOwner then begin
      SaveConfig;
    end;
  except
    //忽略无法保存到只读目录的错误
  end;

  FDrawBmp.Free;
  FRealBmp.Free;
  FGifBitmap.Free;
  FImageExts.Free;
end;

procedure TfMain.FormShow(Sender: TObject);
begin
  OnShow := nil;
  if FMaximized then begin
    //在FormCreate中设置最大化会导致在Win10系统中点“向下还原”按钮后窗口缩到很小；
    //而在Win7系统中则会恢复到设计时大小，而不是上次正常显示的大小
    Self.WindowState := wsMaximized;
  end;
  FThumbView.Width := Max(118, FListViewWidth); // 在FormCreate时设置不起作用
  splRight.Left := FThumbView.Left - splRight.Width;
end;

procedure TfMain.FormPaint(Sender: TObject);
begin
  OnPaint := nil;
  if not FHasOwner then Exit;
  //2012.2.22 SkySee在启动后缩略图尚未完全刷新完就退出程序时会报Access Violation
  //          所以将初始化线程放在OnPaint里，似乎放在这里退出时不会出错
  with TInitThread.Create(InitViews) do begin
    OnTerminate := InitTerminate;
    {$IFDEF UNICODE}Start{$ELSE}Resume{$ENDIF};
  end;
end;

{-----------------------------------------------------------------------------
  Configuration
-----------------------------------------------------------------------------}

procedure TfMain.LoadConfig;
var
  i: Integer;
  sDir: string;
begin
  with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do try
    Left := ReadInteger('Form', 'Left', Left);
    Top := ReadInteger('Form', 'Top', Top);
    Height := ReadInteger('Form', 'Height', Height);
    Width := ReadInteger('Form', 'Width', Width);
    FMaximized := ReadBool('Form', 'Maximized', False);

    FZipperNameOrders := ReadString('Tree', 'ZipperNameOrders', 'ZipRar.dll'#13#10'SevenZ.dll');
    FTreeInitPath := ReadString('Tree', 'InitPath', ExtractFilePath(Application.ExeName));
    FLastPicDir := ReadString('Tree', 'LastPicDir', '');

    SkyDirFileTree1.Color := ReadInteger('View', 'TreeColor', SkyDirFileTree1.Color);
    SkyDirFileTree1.Width := Max(25, ReadInteger('View', 'TreeWidth', SkyDirFileTree1.Width));
    FTreeHScrollPos := ReadInteger('View', 'TreeHScrollPos', 0);
    FListViewWidth  := ReadInteger('View', 'ListWidth', FThumbView.Width);
    actSearchSubDir.Checked    := ReadBool('View', 'SearchSubDir', False);
    actSearchHiddenDir.Checked := ReadBool('View', 'SearchHiddenDir', True);
    actSearchHiddenDirExecute(actSearchHiddenDir);
    actShowRecentMenu.Checked  := ReadBool('View', 'ShowRecentMenu', True);
    actStatusBar.Checked       := ReadBool('View', 'StatusBar', True);
    StatusBar1.Visible := actStatusBar.Checked;
    actQuickShow.Checked       := ReadBool('View', 'QuickShow', True);
    //actQuickShow.OnExecute(actQuickShow);    move to FormCreate after translation
    actFitView.Checked         := ReadBool('View', 'FitView', True);
    actMarkRotateInfo.Checked  := ReadBool('View', 'MarkRotateInfo', True);
    actShowPicTip.Checked      := ReadBool('View', 'ShowPicTip', True);
    FThumbView.ShowHint         := actShowPicTip.Checked;
    FLastSelImage := ReadString('View', 'LastSelectImage', '');

    for i := 0 to RECENT_COUNT -1 do begin
      sDir := ReadString('Recent', IntToStr(i), '');
      if sDir <> '' then begin
        SetRecentMenuItem(FRecentMenus[i], i, sDir);
      end else begin
        Break;
      end;
    end;

    FImageExts.CommaText := ReadString('Config', 'ImageExts', 'bmp,ico,jpg,jpeg,jpe,jfif,wmf,emf,exif,tiff,tif,png,gif');
    FImageExts.Sorted := True;
    FLastMergeColor := ReadInteger('Config', 'MergeColor', clNavy);
    FLastCoverColor := ReadInteger('Config', 'CoverColor', clNavy);
    FCoverPercent   := Max(1, Min(100, ReadInteger('Config', 'CoverPercent', 50)));
    FScaleAdj := ReadBool('Config', 'ScaleAdj', True);
    FLightX := ReadInteger('Config', 'LightX', 230);
    FLightY := ReadInteger('Config', 'LightY', 160);
    FLightM := ReadInteger('Config', 'LightM', 200);
    FLightN := ReadInteger('Config', 'LightN', 1);
    FCycleBrowse := ReadBool('Config', 'CycleBrowse', True);
    FLanguageDir := ReadString('Config', 'LanguageDir', '');
    if FLanguageDir = '' then begin
      FLanguageDir := ExtractFilePath(Application.ExeName) + 'Language\';
    end else begin
      if FLanguageDir[1] = '.' then begin
        FLanguageDir := ExpandFileName(FLanguageDir);
      end;
      if FLanguageDir[Length(FLanguageDir)] <> '\' then begin
        FLanguageDir := FLanguageDir + '\';
      end;
    end;
  finally
    Free;
  end;
end;

procedure TfMain.SaveConfig;
var
  i: Integer;
begin
  FTreeHScrollPos := GetScrollPos(SkyDirFileTree1.Handle, SB_HORZ);
  with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do try
    if WindowState <> wsMaximized then begin
      WriteInteger('Form', 'Left', Left);
      WriteInteger('Form', 'Top', Top);
      WriteInteger('Form', 'Height', Height);
      WriteInteger('Form', 'Width', Width);
    end;
    WriteBool('Form', 'Maximized', WindowState = wsMaximized);

    for i := 0 to RECENT_COUNT -1 do begin
      WriteString('Recent', IntToStr(i), FRecentMenus[i].Hint);
    end;

    WriteString('Tree', 'InitPath', SkyDirFileTree1.GetSelectNodeFullPath());
    WriteString('Tree', 'LastPicDir', FLastPicDir);

    WriteInteger('View', 'TreeColor', SkyDirFileTree1.Color);
    WriteInteger('View', 'TreeWidth', SkyDirFileTree1.Width);
    WriteInteger('View', 'TreeHScrollPos', FTreeHScrollPos);
    WriteInteger('View', 'ListWidth', FThumbView.Width);
    WriteBool('View', 'SearchSubDir', actSearchSubDir.Checked);
    WriteBool('View', 'SearchHiddenDir', actSearchHiddenDir.Checked);
    WriteBool('View', 'ShowRecentMenu', actShowRecentMenu.Checked);
    WriteBool('View', 'StatusBar', actStatusBar.Checked);
    WriteBool('View', 'QuickShow', actQuickShow.Checked);
    WriteBool('View', 'FitView', actFitView.Checked);
    WriteBool('View', 'MarkRotateInfo', actMarkRotateInfo.Checked);
    WriteBool('View', 'ShowPicTip', actShowPicTip.Checked);
    if FThumbView.Selected <> nil then begin
      WriteString('View', 'LastSelectImage', FThumbView.Selected.Caption);
    end else if FThumbView.FileInfos.IndexOfName(FLastSelImage) >= 0 then begin
      WriteString('View', 'LastSelectImage', FLastSelImage);
    end else begin
      WriteString('View', 'LastSelectImage', '');
    end;

    WriteString('Config', 'ImageExts', FImageExts.CommaText);
    WriteInteger('Config', 'MergeColor', FLastMergeColor);
    WriteInteger('Config', 'CoverColor', FLastCoverColor);
    WriteInteger('Config', 'CoverPercent', FCoverPercent);
    WriteInteger('Config', 'LightX', FLightX);
    WriteInteger('Config', 'LightY', FLightY);
    WriteInteger('Config', 'LightM', FLightM);
    WriteInteger('Config', 'LightN', FLightN);
    WriteBool('Config', 'CycleBrowse', FCycleBrowse);
  finally
    Free;
  end;
end;

{-----------------------------------------------------------------------------
  App Functions
-----------------------------------------------------------------------------}

procedure TfMain.AppMessage(var Msg: TMsg; var Handled: Boolean);
var
  DroppedFilename : string;
  FileIndex : integer;
  QtyDroppedFiles : integer;
  pDroppedFilename : array [0..MAX_PATH-1] of Char;
begin
  case Msg.Message of
    WM_DROPFILES: begin
      FileIndex := not 0;
      QtyDroppedFiles := DragQueryFile(Msg.WParam, FileIndex,
                                       pDroppedFilename, MAX_PATH);
      if(QtyDroppedFiles=0)then exit;
      for FileIndex := 0 to (QtyDroppedFiles - 1) do begin
        if(DragQueryFile(Msg.WParam, FileIndex,pDroppedFilename, MAX_PATH)<=MAX_PATH) then begin
          DroppedFilename := StrPas(pDroppedFilename);
          if FileExists(DroppedFileName) then begin
            ChangeDir(ExtractFilePath(DroppedFileName));
            LocateImage(ExtractFileName(DroppedFileName));
            Break;
  {$IFDEF UNICODE}
  {$WARN SYMBOL_DEPRECATED OFF} // for DirectoryExists
  {$ENDIF}
          end else if DirectoryExists(DroppedFileName) then begin
  {$IFDEF UNICODE}
  {$WARN SYMBOL_DEPRECATED ON}
  {$ENDIF}
            ChangeDir(DroppedFileName);
            Break;
          end;
        end;
      end;
      Application.BringToFront;
      DragFinish(Msg.WParam);
      Handled := true;
    end;
  end;
end;

{-----------------------------------------------------------------------------
  Init Functions
-----------------------------------------------------------------------------}

procedure TfMain.InitViews;
var
  sTmp: string;
  cmdLoad: Boolean;
  initDir: string;
begin
  {$IFDEF MULTILANGUAGE}
  rsDirFileHint  := Translate('rsDirFileHint', rsDirFileHint);   // '目录树区';
  rsNoFloppyTip  := Translate('rsNoFloppyTip', rsNoFloppyTip);   // '软驱内空空！请在软驱中插入磁盘。';
  rsNoRemoveTip  := Translate('rsNoRemoveTip', rsNoRemoveTip);   // '请确定移动存储器是否插好！';
  rsNoRemoteTip  := Translate('rsNoRemoteTip', rsNoRemoteTip);   // '请确定网络硬盘映射是否正确！';
  rsNoCdromTip   := Translate('rsNoCdromTip', rsNoCdromTip);     // '光驱内空空！请将光盘放入光驱。';
  rsFloppyName   := Translate('rsFloppyName', rsFloppyName);     // '3.5 吋软盘';
  rsRemoveName   := Translate('rsRemoveName', rsRemoveName);     // '移动存储器';
  rsFixedName    := Translate('rsFixedName', rsFixedName);       // '本地硬盘';
  rsRemoteName   := Translate('rsRemoteName', rsRemoteName);     // '网络硬盘';
  rsCdromName    := Translate('rsCdromName', rsCdromName);       // '光驱';
  rsRamDiskName  := Translate('rsRamDiskName', rsRamDiskName);   // '虚拟盘';
  rsDriveNotFind := Translate('rsDriveNotFind', rsDriveNotFind); // '找不到驱动器（%s），不能更新！';
  rsRootName     := Translate('rsRootName', rsRootName);         // '我的电脑';
  {$ENDIF}

  SkyDirFileTree1.OnChange := nil;
  try
    SkyDirFileTree1.RootName := rsRootName; //!!
    SkyDirFileTree1.Hint     := rsDirFileHint;
    cmdLoad := False;
    if ParamCount() > 0 then begin
      sTmp := ExpandFileName(ParamStr(1));
      if FileExists(sTmp) then begin
        cmdLoad := True;
        ChangeDir(ExtractFilePath(sTmp));
        FLastSelImage := ExtractFileName(sTmp);
  {$IFDEF UNICODE}
  {$WARN SYMBOL_DEPRECATED OFF} // for DirectoryExists
  {$ENDIF}
      end else if DirectoryExists(sTmp) then begin
  {$IFDEF UNICODE}
  {$WARN SYMBOL_DEPRECATED ON}
  {$ENDIF}
        cmdLoad := True;
        ChangeDir(sTmp);
      end else begin
        raise Exception.CreateFmt(Translate('Path not found! %s'), [sTmp]);
      end;
    end;
    if not cmdLoad then begin
      SkyDirFileTree1.UpdateOfInitPath(FTreeInitPath);
      initDir := SkyDirFileTree1.GetSelectNodeFullPath();
      if initDir <> FTreeInitPath then begin
        initDir := ExpandFileName(initDir);
        if initDir = '' then begin
          initDir := ExtractFilePath(ParamStr(0));
        end;
        SkyDirFileTree1.UpdateOfInitPath(initDir);
      end;
      SkyDirFileTree1.OnChange := SkyDirFileTree1Change; //!! 如果目录不可访问，必须挂上事件才能做到即时切换目录
      FThumbView.SearchDir(initDir, actSearchHiddenDir.Checked, actSearchSubDir.Checked);
    end;
  except
    on E: Exception do begin
      Application.MessageBox(PChar(Translate('Load Error') + ':' + E.Message), PChar(Translate('Error')));
    end;
  end;
  SkyDirFileTree1.OnChange := SkyDirFileTree1Change;

  //SetScrollPos放在OnTimer里走不起作用？在Form.OnShow可以的
  //SetScrollPos(SkyDirFileTree1.Handle, SB_HORZ, FTreeHScrollPos, True);
  //2011.10.20 Message比SetScrollPos厉害
  if FTreeHScrollPos > 0 then begin
    SkyDirFileTree1.Perform(WM_HSCROLL, MakeLong(SB_THUMBTRACK, FTreeHScrollPos), 0);
  end;

  //LocateImage(FLastSelImage); 在此处调用，则为线程里再创建线程，有时会导致定位失败，只能显示第一个图片，但位置已变  2012.9.17

  //真奇怪,如果将ListView.TabOrder设为0或在FormShow里调用ListView.SetFocus,则当
  //程序启动时若窗口为最大化,则状态虽最大化了,但窗口大小却不能最大化 2011.6.2
  FThumbView.SetFocus;
end;

procedure TfMain.InitTerminate(Sender: TObject);
begin
  LocateImage(FLastSelImage);
end;

{-----------------------------------------------------------------------------
  Image Functions
-----------------------------------------------------------------------------}

procedure TfMain.FindDir(const sDir: string);
begin
  StatusBar1.Panels[0].Text := MinimizeName(ExcludeTrailingBackslash(sDir),
    StatusBar1.Canvas, StatusBar1.Panels[0].Width -2);
  StatusBar1.Update;
end;

procedure TfMain.FindDirComplete(const ImageDir: string; FileCount: Integer);
begin
  StatusBar1.Panels[2].Text := Format('%d %s', [FileCount, Translate('Pieces')]);
  if FileCount > 0 then begin
    AddLastDir(ImageDir);
  end;
end;

procedure TfMain.ClearImageProps;
begin
  with StatusBar1 do begin
    Panels[1].Text := '';
    Panels[3].Text := '';
  end;
end;

procedure TfMain.ShowImageProps(PicWidth, PicHeight: Integer;
  UpdateModifyTime: Boolean);
begin
  StatusBar1.Panels[1].Text := Format('%dX%d (%dX%d) %s', [
    PicWidth, PicHeight, PaintBox1.Width, PaintBox1.Height,
    MyFmtFileSize(FLastImgFile)]);
  if UpdateModifyTime then begin
    StatusBar1.Panels[3].Text := Format('%s (%s:%s)', [FLastImgFile,
      Translate('Modify'),
      GetFileModifyTimeStr(FLastImgFile)]);
  end;
end;

{.$DEFINE MYDEBUG}

procedure TfMain.AdjustSizeForTiff(gpBmp: TGpBitmap);
var
  hr: Integer;
  vr: Integer;
begin
  //以下用分辨率来判断图片的显示方向（某些TIF需要偏转显示）
  hr := Trunc(gpBmp.GetHorizontalResolution);
  vr := Trunc(gpBmp.GetVerticalResolution);
  if hr <= vr then begin
    FLastPicWidth  := gpBmp.GetWidth;
    FLastPicHeight := gpBmp.GetHeight;
  end else begin
    FLastPicHeight := gpBmp.GetWidth;
    FLastPicWidth  := gpBmp.GetHeight;
  end;
end;

procedure TfMain.LoadByGdiImage(const sImageFile: string);
var                 // only for '.exif'#9'.tiff'#9'.tif'#9'.png'
  gpBmp: TGpBitmap;
  g: TGPGraphics;
begin
  gpBmp := TGpBitmap.Create(sImageFile);
  try
    FLastPicWidth := gpBmp.GetWidth;
    FLastPicHeight := gpBmp.GetHeight;
    if '.png' <> LowerCase(ExtractFileExt(sImageFile)) then begin
      AdjustSizeForTiff(gpBmp);
    end;
    AdjustPaintBoxSize(FLastPicWidth, FLastPicHeight);
    FStretched := (FLastPicWidth <> PaintBox1.Width) or (FLastPicHeight <> PaintBox1.Height);
    FDrawBmp.Width := PaintBox1.Width;
    FDrawBmp.Height := PaintBox1.Height;
    FDrawBmp.Canvas.Brush.Color := ScrollBox1.Color;
    FDrawBmp.Canvas.FillRect(FDrawBmp.Canvas.ClipRect);
    g := TGpGraphics.Create(FDrawBmp.Canvas.Handle);
    try
      g.DrawImage(gpBmp, 0, 0, FDrawBmp.Width, FDrawBmp.Height);
    finally
      g.Free;
    end;
  finally
    gpBmp.Free;
  end;
  ShowImageProps(FLastPicWidth, FLastPicHeight);//GDIBitmap打开时得不到文件时间
end;

procedure TfMain.LoadByVclImage(const sImageFile: string);
{$IFDEF MYDEBUG}
const
  cJpegScales: array[TJpegScale] of string = ('FullSize', 'Half', 'Quarter', 'Eighth');
{$ENDIF}
var
  bmp: TBitmap;
  {$IFDEF MYDEBUG}
  s: string;
  dwTick: DWord;
  {$ENDIF}
  img: TImage;
begin
  img := TImage.Create(nil);
  with img do try
    {$IFDEF MYDEBUG}dwTick := GetTickCount;{$ENDIF}
    Picture.LoadFromFile(sImageFile);
    {$IFDEF MYDEBUG}s := Format('Load: %.3f', [(GetTickCount - dwTick)/ 1000.0]);{$ENDIF}
    FLastPicWidth := Picture.Width;
    FLastPicHeight := Picture.Height;
    AdjustPaintBoxSize(FLastPicWidth, FLastPicHeight);
    ShowImageProps(FLastPicWidth, FLastPicHeight);
    if SameText('.gif', ExtractFileExt(sImageFile)) then begin
      ExtraForGifImage(Canvas, sImageFile); //GIF多处理一遍，好在GIF一般不大
    end;
    FStretched := (FLastPicWidth <> PaintBox1.Width) or (FLastPicHeight <> PaintBox1.Height);
    if not FStretched then begin
      FDrawBmp.Width := PaintBox1.Width;
      FDrawBmp.Height := PaintBox1.Height;
      FDrawBmp.Canvas.Brush.Color := ScrollBox1.Color;
      FDrawBmp.Canvas.FillRect(FDrawBmp.Canvas.ClipRect);
      {$IFDEF MYDEBUG}dwTick := GetTickCount;{$ENDIF}
      //FDrawBmp.Assign(Picture.Graphic);      JPEG也可能是8bit?  2011.5.25
      FDrawBmp.Canvas.Draw(0, 0, Picture.Graphic);
      {$IFDEF MYDEBUG}s := Format('%s,Assign: %.3f', [s, (GetTickCount - dwTick)/ 1000.0]);{$ENDIF}
    end else if (Picture.Graphic is TBitmap) and (Picture.Bitmap.PixelFormat = pf24bit) then begin
      FDrawBmp.Width := PaintBox1.Width;
      FDrawBmp.Height := PaintBox1.Height;
      {$IFDEF MYDEBUG}dwTick := GetTickCount;{$ENDIF}
      StretchLinear(FDrawBmp, Picture.Bitmap);
      {$IFDEF MYDEBUG}s := Format('%s,Stretch: %.3f', [s, (GetTickCount - dwTick)/ 1000.0]);{$ENDIF}
    end else begin
      bmp := TBitmap.Create;
      try
        bmp.PixelFormat := pf24bit;
        if Picture.Graphic is TJpegImage then begin
          CalcThumbnailJpegScale(TJpegImage(Picture.Graphic), PaintBox1.Height, PaintBox1.Width);
          {$IFDEF MYDEBUG}s := Format('%s,%s', [s, cJpegScales[TJpegImage(Picture.Graphic).Scale]]);{$ENDIF}
        end;
        {$IFDEF MYDEBUG}dwTick := GetTickCount;{$ENDIF}
        bmp.Width := Picture.Width;
        bmp.Height := Picture.Height;
        bmp.Canvas.Draw(0, 0, Picture.Graphic);   //Draw的速度似乎与Assign差不多  2010.9.19
        //bmp.Assign(Picture.Graphic);  // 速度 == DIBNeeded    Assign可能会改变PixelFormat
        {$IFDEF MYDEBUG}s := Format('%s,Assign: %.3f', [s, (GetTickCount - dwTick)/ 1000.0]);{$ENDIF}
        FDrawBmp.Width := PaintBox1.Width;
        FDrawBmp.Height := PaintBox1.Height;
        {$IFDEF MYDEBUG}dwTick := GetTickCount;{$ENDIF}
        StretchLinear(FDrawBmp, bmp);
        {$IFDEF MYDEBUG}s := Format('%s,Stretch: %.3f', [s, (GetTickCount - dwTick)/ 1000.0]);{$ENDIF}
      finally
        bmp.Free;
      end;
    end;
    {$IFDEF MYDEBUG}Caption := s;{$ENDIF}
  finally
    Free;
  end;
end;

procedure TfMain.LoadImageAsBmp(const sImageFile: string);
var
  ext: string;
begin
  tmGifShow.Enabled := False;
  pmiExtraProcess.Visible := False; //!!
  FreeAndNil(FGifBitmap);
  FRealBmp.Width := 0;
  FRealBmp.Height := 0;
  ext := LowerCase(ExtractFileExt(sImageFile));
  if Pos(ext, '.jpe'#9'.jfif'#9'.exif'#9'.tiff'#9'.tif'#9'.png') > 0 then begin
    LoadByGdiImage(sImageFile);
  end else begin
    LoadByVclImage(sImageFile);
  end;
  if tmGifShow.Enabled then begin
    pmiExtraProcess.Visible := True;
    //actQuickGif, actSlowGif, actOriginGif 不作特殊处理了,因事件里已处理
  end;
end;

procedure TfMain.JustLoadByGdiImage(const ImageFile: string; Bmp: TBitmap);
var                  //对TIF文件，还可进一步优化，比如DrawImage前尽可能调小Bmp
  gpBmp: TGpBitmap;  // only for '.exif'#9'.tiff'#9'.tif'#9'.png'
  g: TGPGraphics;
begin
  gpBmp := TGpBitmap.Create(ImageFile);
  try
    FLastPicWidth := gpBmp.GetWidth;
    FLastPicHeight := gpBmp.GetHeight;
    if '.png' <> LowerCase(ExtractFileExt(ImageFile)) then begin
      AdjustSizeForTiff(gpBmp);
    end;
    Bmp.Width := FLastPicWidth;
    Bmp.Height := FLastPicHeight;
    Bmp.Canvas.Brush.Color := ScrollBox1.Color;
    Bmp.Canvas.FillRect(Bmp.Canvas.ClipRect);
    g := TGpGraphics.Create(Bmp.Canvas.Handle);
    try
      g.DrawImage(gpBmp, 0, 0, FLastPicWidth, FLastPicHeight);
    finally
      g.Free;
    end;
  finally
    gpBmp.Free;
  end;
end;

procedure TfMain.JustLoadByVclImage(const ImageFile: string; Bmp: TBitmap);
begin
  with TImage.Create(nil) do try
    Picture.LoadFromFile(ImageFile);
    FLastPicWidth := Picture.Width;
    FLastPicHeight := Picture.Height;
    if FNoteProps then begin
      AdjustPaintBoxSize(FLastPicWidth, FLastPicHeight);
    end;
    if SameText('.gif', ExtractFileExt(ImageFile)) then begin
      ExtraForGifImage(Canvas, ImageFile); //GIF多处理一遍，好在GIF一般不大
    end;
    if Picture.Graphic is TJpegImage then begin
      CalcThumbnailJpegScale(TJpegImage(Picture.Graphic), PaintBox1.Height, PaintBox1.Width);
    end;
    Bmp.Width := Picture.Width;
    Bmp.Height := Picture.Height;
    Bmp.Canvas.Brush.Color := ScrollBox1.Color;
    Bmp.Canvas.FillRect(FDrawBmp.Canvas.ClipRect);
    Bmp.Canvas.Draw(0, 0, Picture.Graphic);   //Draw的速度似乎与Assign差不多  2010.9.19
  finally
    Free;
  end;
end;

procedure TfMain.JustLoadImageToBmp(const ImageFile: string; Bmp: TBitmap);
var
  ext: string;
begin
  tmGifShow.Enabled := False;
  pmiExtraProcess.Visible := False; //!!
  FreeAndNil(FGifBitmap);
  //Bmp.PixelFormat := pf24bit;
  ext := LowerCase(ExtractFileExt(ImageFile));
  if Pos(ext, '.jpe'#9'.jfif'#9'.exif'#9'.tiff'#9'.tif'#9'.png') > 0 then begin
    JustLoadByGdiImage(ImageFile, Bmp);
  end else begin
    JustLoadByVclImage(ImageFile, Bmp);
  end;
  if tmGifShow.Enabled then begin
    pmiExtraProcess.Visible := True;
    //actQuickGif, actSlowGif, actOriginGif 不作特殊处理了,因事件里已处理
  end;
end;

procedure TfMain.QuickLoadByGdiImage(const sImageFile: string);
var
  gpBmp, bmp: TGpBitmap;
  g: TGPGraphics;
  ext: string;
begin
  tmGifShow.Enabled := False;
  pmiExtraProcess.Visible := False; //!!
  FreeAndNil(FGifBitmap);
  FRealBmp.Width := 0;
  FRealBmp.Height := 0;
  
  gpBmp := TGpBitmap.Create(sImageFile);
  try
    if (FOrientation in [5..8]) then begin
      FLastPicWidth := gpBmp.GetHeight;
      FLastPicHeight := gpBmp.GetWidth;
    end else begin
      FLastPicWidth := gpBmp.GetWidth;
      FLastPicHeight := gpBmp.GetHeight;
    end;
    if (FRotateAngle = 90) or (FRotateAngle = 270) then begin
      Swap(FLastPicWidth, FLastPicHeight);
    end;
    ext := LowerCase(ExtractFileExt(sImageFile));
    if ('.tif' = ext) or ('.tiff' = ext) then begin
      AdjustSizeForTiff(gpBmp);
    end;
    AdjustPaintBoxSize(FLastPicWidth, FLastPicHeight);
    FStretched := (FLastPicWidth <> PaintBox1.Width) or (FLastPicHeight <> PaintBox1.Height);

    bmp := TGpBitmap.Create(PaintBox1.Width, PaintBox1.Height);
    try
      //先缩小
      g := TGpGraphics.Create(bmp);
      try
        g.SetInterpolationMode(InterpolationModeBilinear); //旋转图片时用HighQualityBicubic效果更好，但速度很慢
        g.DrawImage(gpBmp, 0, 0, PaintBox1.Width, PaintBox1.Height);
      finally
        g.Free;
      end;
      FreeAndNil(gpBmp);
      //旋转
      case FOrientation of    //图片拍摄方向
        1: ; //"Top, left side (Horizontal / normal)"
        2: Bmp.RotateFlip(RotateNoneFlipX);    //?? todo.. "Top, right side (Mirror horizontal)"
        3: Bmp.RotateFlip(Rotate180FlipNone) ; //"Bottom, right side (Rotate 180)"
        4: Bmp.RotateFlip(Rotate180FlipX);     //?? todo.. "Bottom, left side (Mirror vertical)"
        5: Bmp.RotateFlip(Rotate270FlipX);     //?? todo.. "Left side, top (Mirror horizontal and rotate 270 CW)
        6: Bmp.RotateFlip(Rotate90FlipNone);   //"Right side, top (Rotate 90 CW)"
        7: Bmp.RotateFlip(Rotate90FlipX);      //?? todo.. "Right side, bottom (Mirror horizontal and rotate 90 CW)"
        8: Bmp.RotateFlip(Rotate270FlipNone);  //"Left side, bottom (Rotate 270 CW)"
      end;
      case FRotateAngle of
        90:  Bmp.RotateFlip(Rotate90FlipNone);
        180: Bmp.RotateFlip(Rotate180FlipNone);
        270: Bmp.RotateFlip(Rotate270FlipNone);
      end;
      //最后才画
      FDrawBmp.Width := PaintBox1.Width;
      FDrawBmp.Height := PaintBox1.Height;
      FDrawBmp.Canvas.Brush.Color := ScrollBox1.Color;
      FDrawBmp.Canvas.FillRect(FDrawBmp.Canvas.ClipRect);
      g := TGpGraphics.Create(FDrawBmp.Canvas.Handle);
      try
        g.DrawImage(bmp, 0, 0, FDrawBmp.Width, FDrawBmp.Height);
      finally
        g.Free;
      end;
    finally
      bmp.Free;
    end;
  finally
    gpBmp.Free;
  end;
  ShowImageProps(FLastPicWidth, FLastPicHeight);//GDIBitmap打开时得不到文件时间
end;

procedure TfMain.ShowSelectImage(const sDir, sImageName: string);
var
  sImageFile: string;

  procedure ShowImage(ChangeProc: TChangeProc);
  begin
    FNoteProps := True;
    FStretched := True;
    FRealBmp.Width := 0;
    FRealBmp.Height := 0;
    ChangeBmp(ChangeProc);
    FStretched := (FRealBmp.Width <> PaintBox1.Width) or (FRealBmp.Height <> PaintBox1.Height);
    FNoteProps := False;
    ShowImageProps(FLastPicWidth, FLastPicHeight);
  end;

  function IsQuickShowImage: Boolean;
  var
    ext: string;
  begin
    ext := Format(#9'%s'#9, [LowerCase(ExtractFileExt(sImageFile))]);
    Result := Pos(ext, #9'.bmp'#9'.jpg'#9'.jpeg'#9'.jpe'#9'.jfif'#9'.tiff'#9'.tif'#9'.png'#9)> 0;
  end;

var
  realRotate: Integer;
  dwTick: DWord;
begin
  lbErrMsg.Visible := False;
  FRotateAngle := 0;
  sImageFile := sDir + sImageName;
  try
    if FileExists(sImageFile) then begin
      if sDir <> '' then begin
        FLastSelImage := sImageName;
        FLastImgFile  := sImageFile;
      end;
      if actMarkRotateInfo.Checked then begin
        if FileExists(sImageFile + '~90') then begin
          FRotateAngle := 90;
        end else if FileExists(sImageFile + '~180') then begin
          FRotateAngle := 180;
        end else if FileExists(sImageFile + '~270') then begin
          FRotateAngle := 270;
        end;
      end;
      if actDebugMode.Checked then begin
        dwTick := GetTickCount;
      end else begin
        dwTick := 0; //消除编译警告
      end;
      if actQuickShow.Checked and IsQuickShowImage then begin
        Self.QuickLoadByGdiImage(sImageFile);
      end else begin
        realRotate := FRotateAngle;
        case FOrientation of
          1: ; //"Top, left side (Horizontal / normal)"
          2: ; //todo.. "Top, right side (Mirror horizontal)"
          3: Inc(realRotate, 180) ; //"Bottom, right side (Rotate 180)"
          4: ; //todo.. "Bottom, left side (Mirror vertical)"
          5: ; //todo.. "Left side, top (Mirror horizontal and rotate 270 CW)
          6: Inc(realRotate, 90);   //"Right side, top (Rotate 90 CW)"
          7: ; //todo.. "Right side, bottom (Mirror horizontal and rotate 90 CW)"
          8: Inc(realRotate, 270);  //"Left side, bottom (Rotate 270 CW)"
        end;
        case realRotate mod 360 of
          90:  ShowImage(QuickDextrorotation90);
          180: ShowImage(Rotate180);
          270: ShowImage(QuickLevorotation90);
        else   LoadImageAsBmp(sImageFile);
        end;
      end;
      if actDebugMode.Checked then begin
        Caption := Format('%s: %.3f%s', [Translate('Debug Mode - Draw Time'),
          (GetTickCount - dwTick) / 1000.0, Translate('s')]); // 调试模式 - 绘制耗时: %.3f秒
      end;
      PaintBox1.Invalidate;
    end else begin
      raise Exception.Create(Translate('Picture not Found.')); //'找不到图片。'
    end;
  except
    On E: Exception do begin
      FDrawBmp.Width := 0;
      FDrawBmp.Height := 0;
      lbErrMsg.Caption := Format('%s：%s', [Translate('Load Picture Error'), E.Message]); // 加载图片出错
      lbErrMsg.Visible := True;
      ClearImageProps;
    end;
  end;
end;

procedure TfMain.ExtraForGifImage(const ACanvas: TCanvas; const ImageFile: string);
var
  g: TGpGraphics;
  timeLen: Integer;
  itemLen: Integer;
  propItem: Pointer;
begin
  FGifSpeed := 1;
  FreeAndNil(FGifBitmap);
  FGifBitmap := TGpBitmap.Create(ImageFile);
  GetGifProps(FGifBitmap, FGifFrames, timeLen, itemLen, propItem);
  if propItem <> nil then begin
    try
      SetLength(FFrameTimes, timeLen div SizeOf(Cardinal));
      GetGifFrameTimes(propItem, FFrameTimes);
    finally
      FreeMem(propItem, itemLen);
    end;
    if FGifFrames > 1 then begin
      FGifIndex := 0;
      if FFrameTimes[0] = 0 then begin
        tmGifShow.Interval := 100;
      end else begin
        tmGifShow.Interval := FFrameTimes[0] * 10;
      end;
      tmGifShow.Enabled := True;
    end;
  end;
  g := TGpGraphics.Create(ACanvas.Handle);
  try
    ACanvas.Brush.Color := ScrollBox1.Color;
    ACanvas.FillRect(ACanvas.ClipRect);
    g.DrawImage(FGifBitmap, ACanvas.ClipRect.Left, ACanvas.ClipRect.Top, FGifBitmap.GetWidth, FGifBitmap.GetHeight);
  finally
    g.Free;
  end;
end;

procedure TfMain.GetExifInfo(HasExifInfo: Boolean; const EquipMake, EquipModel,
  ExposureTime, FocalLength, DTOrigin: string; Orientation: Integer);

  procedure InitTranslateMsgs;
  begin
    if FOrientationTxts[2] = '' then begin
      FOrientationTxts[2] := Translate('Mirror Horizontally');     // 水平镜像
      FOrientationTxts[3] := Translate('Ratate 180 Degrees');      // 旋转180度
      FOrientationTxts[4] := Translate('Mirror Vertically');       // 垂直镜像
      FOrientationTxts[5] := Translate('Mirror Horizontally') + '+' + Translate('Levorotation 270 Degrees'); // 水平镜像+左旋270度
      FOrientationTxts[6] := Translate('Levorotation 90 Degrees'); // 左旋90度
      FOrientationTxts[7] := Translate('Mirror Horizontally') + '+' + Translate('Levorotation 90 Degrees');  // 水平镜像+左旋90度
      FOrientationTxts[8] := Translate('Levorotation 270 Degrees');// 左旋270度
    end;
  end;

var
  s: string;
  idx: Integer;
begin
  if not actShowPicTip.Checked then begin
    PaintBox1.Hint := '';
    Exit;
  end;

  InitTranslateMsgs;
  s := DTOrigin;
  if DTOrigin <> '' then s := Translate('Shoot') + ':' + s;
  idx := LastDelimiter(':', s);
  if idx > 0 then begin
    System.Delete(s, idx, MaxInt); //去掉最后面的秒，只保留到分钟
  end;
  if (Orientation > 1) and (Orientation <= 8) then begin
    if s <> '' then begin
      s := s + sLineBreak;
    end;
    s := s + FOrientationTxts[Orientation];
  end;
  PaintBox1.Hint := s;
end;

{-----------------------------------------------------------------------------
  Other Show & Change functions
-----------------------------------------------------------------------------}

procedure TfMain.ChangeDir(const sDir: string);
begin
  AddLastDir(IncludeTrailingBackslash(sDir));
  FSearching := True;
  try
    SkyDirFileTree1.UpdateOfInitPath(sDir);
  finally
    FSearching := False;
  end;
  FThumbView.SearchDir(SkyDirFileTree1.GetSelectNodeFullPath(), actSearchHiddenDir.Checked, actSearchSubDir.Checked);
end;

procedure TfMain.LocateImage(const sImageName: string);
begin
  FThumbView.LocateImage(sImageName, LocateComplete);
end;

procedure TfMain.LocateComplete(const ImageName: string; ImageIndex: Integer);
begin
  if ImageIndex >= 0 then begin
    FLastSelImage := ImageName;
  end;
end;

procedure TfMain.AdjustPaintBoxSize(iPicWidth, iPicHeight: Integer);
var
  dHScale, dWScale, dScale: Double;
begin
  ScrollBox1.HorzScrollBar.Visible := False;
  ScrollBox1.VertScrollBar.Visible := False;
  if (ScrollBox1.ClientHeight >= iPicHeight) and (ScrollBox1.ClientWidth >= iPicWidth) then begin
    PaintBox1.Width := iPicWidth;
    PaintBox1.Height := iPicHeight;
    PaintBox1.Left := (ScrollBox1.ClientWidth - PaintBox1.Width) div 2;
    PaintBox1.Top := (ScrollBox1.ClientHeight - PaintBox1.Height) div 2;
  end else begin
    if actFitView.Checked then begin
      dHScale := ScrollBox1.ClientHeight * 1.0 / iPicHeight;
      dWScale := ScrollBox1.ClientWidth * 1.0 / iPicWidth;
      if dHScale > dWScale then begin
        dScale := dWScale;
      end else begin
        dScale := dHScale;
      end;
      PaintBox1.Width := Round(iPicWidth * dScale);
      PaintBox1.Height := Round(iPicHeight * dScale);
      PaintBox1.Left := (ScrollBox1.ClientWidth - PaintBox1.Width) div 2;
      PaintBox1.Top := (ScrollBox1.ClientHeight - PaintBox1.Height) div 2;
    end else begin
      PaintBox1.Width := iPicWidth;
      PaintBox1.Height := iPicHeight;
      PaintBox1.Left := 0;
      PaintBox1.Top := 0;
      ScrollBox1.HorzScrollBar.Visible := True;
      ScrollBox1.VertScrollBar.Visible := True;
    end;
  end;
end;

//todo.. 可以改进为将旋转角度保存到当前目录的一个隐藏文件里，而不是每个要旋转的
//       图片保存一个隐藏文件  2012.2.19
procedure TfMain.AdjustRotateInfo(const ImageFile: string; RotateAngle: Integer);
var
  oldRotateFile: string;
  newRotateFile: string;

  procedure DeleteOldRotateFile;
  begin
    if oldRotateFile <> '' then begin
      SetFileAttributes(PChar(oldRotateFile), FILE_ATTRIBUTE_NORMAL);
      DeleteFile(oldRotateFile);
    end;
  end;

begin
  ShowImageProps(FLastPicWidth, FLastPicHeight, False);
  if actMarkRotateInfo.Checked then begin
    if FileExists(ImageFile + '~90') then begin
      oldRotateFile := ImageFile + '~90';
      Inc(RotateAngle, 90);
    end else if FileExists(ImageFile + '~180') then begin
      oldRotateFile := ImageFile + '~180';
      Inc(RotateAngle, 180);
    end else if FileExists(ImageFile + '~270') then begin
      oldRotateFile := ImageFile + '~270';
      Inc(RotateAngle, 270);
    end;
    RotateAngle := RotateAngle mod 360;
    if RotateAngle > 0 then begin
      newRotateFile := Format('%s~%d', [ImageFile, RotateAngle]);
      if not SameText(newRotateFile, oldRotateFile) then begin
        DeleteOldRotateFile;
        try    //如果目录不可写，则文件会创建失败(如光盘上的目录)
          with TFileStream.Create(newRotateFile, fmCreate) do Free;
          SetFileAttributes(PChar(newRotateFile), FILE_ATTRIBUTE_HIDDEN or
            FILE_ATTRIBUTE_READONLY or FILE_ATTRIBUTE_SYSTEM);
        except
          on E: Exception do StatusBar1.Panels[0].Text := E.Message;
        end;
      end;
    end else begin
      DeleteOldRotateFile;
    end;
    FRotateAngle := RotateAngle;
  end;
end;

procedure TfMain.ChangeBmp(ChangeProc: TChangeProc);
var
  M: TChangeObjProc;
begin
  with TMethod(M) do begin
    Data := nil;
    Code := @ChangeProc;
  end;
  ChangeBmp(M);
end;

procedure TfMain.ChangeBmp(ChangeObjProc: TChangeObjProc);
var
  bmp: TBitmap;
  dwTick: DWord;
  W: Integer;
  H: Integer;
begin
  if Assigned(FDrawBmp) then begin
    if FStretched and not actQuickShow.Checked then begin //简单处理一下快速模式
      bmp := FRealBmp;
      if FRealBmp.Width = 0 then begin
        JustLoadImageToBmp(FLastImgFile, bmp);
      end;
    end else begin
      bmp := FDrawBmp;
    end;
    if TMethod(ChangeObjProc).Data = nil then begin
      TMethod(ChangeObjProc).Data := bmp;  //!!!
    end;
    W := bmp.Width;
    H := bmp.Height;

    dwTick := GetTickCount;
    ChangeObjProc(bmp);
    dwTick := GetTickCount - dwTick;
    StatusBar1.Panels[0].Text := Format('%s %.3f %s', [Translate('Time Spent'), dwTick / 1000.0, Translate('s')]); //用时 %.3f 秒

    if (W <> bmp.Width) or (H <> bmp.Height) then begin
      AdjustPaintBoxSize(bmp.Width, bmp.Height);
    end;
    if (bmp <> FDrawBmp) and ((FDrawBmp.Width <> PaintBox1.Width) or (FDrawBmp.Height <> PaintBox1.Height)) then begin
      FDrawBmp.Width := PaintBox1.Width;
      FDrawBmp.Height := PaintBox1.Height;
    end;
    if bmp <> FDrawBmp then begin
      StretchLinear(FDrawBmp, bmp);
    end;
    PaintBox1.Invalidate;
  end;
end;

procedure TfMain.DoMergeColor(const Bmp: TBitmap);
begin
  MergeColor(Bmp, FLastMergeColor);
end;

procedure TfMain.DoCoverWithColor(const Bmp: TBitmap);
begin
  CoverWithColor(Bmp, FLastCoverColor, FCoverPercent);
end;

procedure TfMain.DoCanvasOut(const Bmp: TBitmap);
begin
  CanvasOut2(Bmp, 2);
end;

{-----------------------------------------------------------------------------
  Recent Menu Item functions
-----------------------------------------------------------------------------}
function TfMain.FindMenuItem(Item: TMenuItem; sName: string): TMenuItem;
var
  i: Integer;
begin
  for i := Item.Count -1 downto 0 do
    if Item.Items[i].Name = sName then begin
      Result := Item.Items[i];
      Exit;
    end;
  Result := nil;
end;

procedure TfMain.InitRecentMenuItems;
var
  i: Integer;
begin
  for i := 0 to RECENT_COUNT -1 do begin
    FRecentMenus[i] := FindMenuItem(miFile,
      Format('miRecent_%s', [IntToHex(i, 1)]));
  end;
end;

procedure TfMain.SetRecentMenuItem(Item: TMenuItem; idx: integer;
        const Dir: string);
begin              //设置“最近打开文件”菜单项
  with Item do
  begin
    Hint    := Dir;
    Visible := actShowRecentMenu.Checked;
    Caption := Format('&%s %s', [IntToHex(idx, 1),
                                 MinimizeName(Dir, Canvas, 300)]);
  end;
end;

procedure TfMain.AddLastDir(const Dir: string);
var                //打开文件时将其加入到“最近打开文件”列表缓存中
  idx, k: Integer;
  sDir:  string;

  function IndexOfRecent: Integer;
  begin
    Result := RECENT_COUNT - 1;
    while (Result >= 0) and not SameText(sDir, FRecentMenus[Result].Hint) do
      Dec(Result);
  end;

begin
  if not actShowRecentMenu.Checked then Exit;
  
  sDir := IncludeTrailingBackslash(Dir); //因循环会改变菜单项Hint,而传入的Dir可能正是Hint
  idx := IndexOfRecent();
  if idx <> 0 then        // = 0 时什么也无需做
  begin
    if idx < 0 then       // 未找到
      idx := RECENT_COUNT - 1;

    for k := idx downto 1 do
      if FRecentMenus[k - 1].Hint <> '' then begin
        SetRecentMenuItem(FRecentMenus[k], k, FRecentMenus[k - 1].Hint);
      end;
    SetRecentMenuItem(miRecent_0, 0, sDir);
  end;
end;

{-----------------------------------------------------------------------------
  Normal Action & Control Events
-----------------------------------------------------------------------------}

procedure TfMain.miRecentClick(Sender: TObject);
begin
  ChangeDir(TMenuItem(Sender).Hint);
end;

procedure TfMain.miExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfMain.actOpenDirExecute(Sender: TObject);
var
  sDir: string;
begin
  sDir := FLastPicDir;
  if SelDirEx.SelectDirEx(Translate('Select Picture Folder') + ': ', '', sDir) then begin //请选择图片目录
    FLastPicDir := sDir;
    ChangeDir(sDir);
  end;
end;

procedure TfMain.PaintBox1Paint(Sender: TObject);
begin
  PaintBox1.Canvas.Draw(0, 0, FDrawBmp);
end;

procedure TfMain.SkyDirFileTree1Change(Sender: TObject; Node: TTreeNode);
begin
  if FSearching then Exit;
  FThumbView.SearchDir(SkyDirFileTree1.GetSelectNodeFullPath(), actSearchHiddenDir.Checked, actSearchSubDir.Checked);
end;

procedure TfMain.ThumbViewSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);

  //2012.2.20 暂时想不到更好的方法(ThumbThread里也不合适)，先在这里多取一次吧
  procedure ExtractJpegOrientation(const FileName: string);
  var
    bmp: TGpBitmap;
  begin
    try
      bmp := TGpBitmap.Create(FileName);
      try
        FOrientation := StrToIntDef(GetExifItem(bmp, PropertyTagOrientation), 0);
      finally
        bmp.Free;
      end;
    except
    end;
  end;

var
  sDir:  string;
  sName: string;
  sExt:  string;
begin
  if Selected and (Item <> nil) then begin
    //Application.ProcessMessages;   不能执行此句，否则按住上下键会造成显示错
    PaintBox1.Hint := '';
    try
      FOrientation := FThumbView.FileInfos[Item.Index].Orientation;
    except
      FOrientation := 0; //Ignore Error?!
    end;
    sDir := SkyDirFileTree1.GetSelectNodeFullPath();
    sName := Item.Caption;
    if 0 = FOrientation then begin
      sExt := ExtractFileExt(sName);
      if SameText('.jpg', sExt) or SameText('.jpeg', sExt) or SameText('.jpe', sExt) or SameText('.jfif', sExt) then begin
        ExtractJpegOrientation(sDir + sName);
      end;
    end;
    ShowSelectImage(sDir, sName);
  end;
end;

procedure TfMain.actSearchSubDirExecute(Sender: TObject);
begin
  // need null
end;

procedure TfMain.actSearchHiddenDirExecute(Sender: TObject);
begin
  with SkyDirFileTree1 do begin
    if actSearchHiddenDir.Checked then begin
      Options := Options + [sdfFindHidden];
    end else begin
      Options := Options - [sdfFindHidden];
    end;
  end;
end;

procedure TfMain.actShowRecentMenuExecute(Sender: TObject);
var
  i: Integer;
  b: Boolean;
begin
  b := actShowRecentMenu.Checked;
  for i := 0 to RECENT_COUNT -1 do begin
    FRecentMenus[i].Visible := b and (FRecentMenus[i].Hint <> '');
  end;
end;

procedure TfMain.actFitViewExecute(Sender: TObject);
begin
  ShowSelectImage(IncludeTrailingBackslash(SkyDirFileTree1.GetSelectNodeFullPath()), FLastSelImage);
end;

procedure TfMain.actMarkRotateInfoExecute(Sender: TObject);
begin
  // need null
end;

procedure TfMain.actShowPicTipExecute(Sender: TObject);
begin
  FThumbView.ShowHint := actShowPicTip.Checked;
  PaintBox1.ShowHint := actShowPicTip.Checked;
end;

procedure TfMain.actSetAutoBrowseExecute(Sender: TObject);
begin
  with TfSetAutoBrowse.Create(Self) do try
    sedInterval.Value      := Self.tmAutoBrowse.Interval;
    ckbCycleBrowse.Checked := Self.FCycleBrowse;
    if ShowModal = mrOk then begin
      Self.tmAutoBrowse.Interval := sedInterval.Value;
      Self.FCycleBrowse          := ckbCycleBrowse.Checked;
    end;
  finally
    Free;
  end;
end;

procedure TfMain.actStatusBarExecute(Sender: TObject);
begin
  StatusBar1.Visible := actStatusBar.Checked;
end;

procedure TfMain.actQuickShowExecute(Sender: TObject);
begin
  if actQuickShow.Checked then begin
    StatusBar1.Hint := Translate('Quick Mode'); // 快速模式
  end else begin
    StatusBar1.Hint := '';
  end;
end;

procedure TfMain.actDebugModeExecute(Sender: TObject);
begin
  if not actDebugMode.Checked then begin
    Caption := FCapInfo;
  end;
end;

procedure TfMain.actFilePropExecute(Sender: TObject);
var
  sImageFile: string;
begin
  if FThumbView.Selected <> nil then begin
    sImageFile := IncludeTrailingBackslash(SkyDirFileTree1.GetSelectNodeFullPath())
      + FThumbView.Selected.Caption;
    if FileExists(sImageFile) then begin
      ShowDirFileProperties(sImageFile, Handle);
    end;
  end;
end;

procedure TfMain.pmiGraphicPropClick(Sender: TObject);
begin
  if (FLastImgFile <> '') and FileExists(FLastImgFile) then begin
    ShowDirFileProperties(FLastImgFile, Handle);
  end;
end;

procedure TfMain.actEditExifExecute(Sender: TObject);
var
  img: TGpImage;
  rtIcon: TRect;
begin
  if Assigned(FThumbView.Selected) then begin
    if EditJpegProp(FLastImgFile, FThumbView.FileInfos[FThumbView.Selected.Index]) then begin
      with FThumbView.FileInfos[FThumbView.Selected.Index]^ do begin
        GetExifInfo(HasExifInfo, EquipMake, EquipModel, ExposureTime, FocalLength, DTOrigin, Orientation);
        if Self.FOrientation <> Orientation then begin
          Self.FOrientation := Orientation;
          Self.ShowSelectImage('', FLastImgFile);

          //update thumbnail image
          img := ThumbImage;
          try
            ThumbImage := nil;
            FThumbView.Selected.Data := nil;
          finally
            img.Free;
          end;
          rtIcon := FThumbView.Selected.DisplayRect(drIcon);
          InvalidateRect(FThumbView.Handle, @rtIcon, False);
        end;
      end;
    end;
  end;
end;

procedure TfMain.actUpdateDirExecute(Sender: TObject);
begin
  SkyDirFileTree1.UpdateSelectedNode();
end;

procedure TfMain.actAboutExecute(Sender: TObject);
begin
  with TfAbout.Create(Self) do try
    ShowModal;
  finally
    Free;
  end;
end;

procedure TfMain.pmiDirTreeColorClick(Sender: TObject);
begin
  FColorDlgTitle := Translate('pmiDirTreeColor.Hint', 'Set Background Color of Folder Tree'); // '设置目录树背景色';
  ColorDialog1.Color := SkyDirFileTree1.Color;
  if ColorDialog1.Execute then begin
    SkyDirFileTree1.Color := ColorDialog1.Color;
  end;
end;

procedure TfMain.pmiShellOpenDirClick(Sender: TObject);
var
  sDir: string;
begin
  sDir := SkyDirFileTree1.GetSelectNodeFullPath();
  if sDir <> '' then begin
    ShellExecute(Handle, 'open', PChar(sDir), nil, nil, SW_SHOW);
  end;
end;

procedure TfMain.pmiDirPropClick(Sender: TObject);
var
  sDir: string;
begin
  sDir := SkyDirFileTree1.GetSelectNodeFullPath();
  if sDir <> '' then begin
    ShowDirFileProperties(sDir, Handle, False);
  end;
end;

procedure TfMain.pmiMultiPropsClick(Sender: TObject);
var
  i: Integer;
  dir: string;
  allFiles: TStrings;
begin
  if FThumbView.Items.Count > 0 then begin
    dir := IncludeTrailingBackslash(SkyDirFileTree1.GetSelectNodeFullPath());
    allFiles := TStringList.Create;
    try
      StatusBar1.Panels[0].Text := Translate('Analysising Pictures Infomation...'); // '正在统计图片文件信息……';
      StatusBar1.Update;
      for i := 0 to FThumbView.FileInfos.Count -1 do begin
        allFiles.Add(dir + FThumbView.FileInfos[i].FileName);
      end;
      if ShowFileProperties(allFiles, Handle) then begin
        StatusBar1.Panels[0].Text := Translate('Pictures Statistic is Over'); // '图片文件信息统计完毕';
      end else begin
        StatusBar1.Panels[0].Text := '';
        if allFiles.Count = 0 then begin
          Application.MessageBox(PChar(Translate('System don''t Support multi Files Statistic(API, Need Win2000+).')),  //'当前系统不支持统计多个文件信息的接口(API)调用(需Win2000以上)。',
            PChar(Translate('Incompatible System'))); // '系统不兼容提示');
        end;
      end;
    finally
      allFiles.Free;
    end;
  end;
end;

procedure TfMain.ScrollBox1Resize(Sender: TObject);
begin
  if actFitView.Checked and FileExists(FLastImgFile) and (FDrawBmp.Width > 0) then begin
    ShowSelectImage('', FLastImgFile);
  end;
end;

procedure TfMain.pmiShellOpenClick(Sender: TObject);
var
  sImageFile: string;
begin
  if FThumbView.Selected <> nil then begin
    sImageFile := IncludeTrailingBackslash(SkyDirFileTree1.GetSelectNodeFullPath())
      + FThumbView.Selected.Caption;
    if FileExists(sImageFile) then begin
      ShellExecute(Handle, 'open', PChar(sImageFile), nil, nil, SW_SHOWNORMAL);
    end;
  end;
end;

procedure TfMain.pmiShellEditClick(Sender: TObject);
var
  sImageFile: string;
begin
  if FThumbView.Selected <> nil then begin
    sImageFile := IncludeTrailingBackslash(SkyDirFileTree1.GetSelectNodeFullPath())
      + FThumbView.Selected.Caption;
    if FileExists(sImageFile) then begin
      ShellExecute(Handle, 'edit', PChar(sImageFile), nil, nil, SW_SHOWNORMAL);
    end;
  end;
end;

procedure TfMain.pmiOpenAsClick(Sender: TObject);
var
  sImageFile: string;
begin
  if FThumbView.Selected <> nil then begin
    sImageFile := IncludeTrailingBackslash(SkyDirFileTree1.GetSelectNodeFullPath())
      + FThumbView.Selected.Caption;
    if FileExists(sImageFile) then begin
      OpenAs_RunDLL(Handle, Handle, PChar(sImageFile));
    end;
  end;
end;

procedure TfMain.pmiDelFileClick(Sender: TObject);

  procedure StopGif;
  begin
    if Assigned(Self.FGifBitmap) then begin
      tmGifShow.Enabled := False;
      FreeAndNil(FGifBitmap);
    end;
  end;

const
  ErrCannotDelMsg = 'Cannot delete the selected picture, maybe it is using.';  //Translate??
var
  sImageFile: string;
  opStruct: TSHFileOpStruct;
  isGif: Boolean;
begin
  if FThumbView.Selected <> nil then begin
    sImageFile := IncludeTrailingBackslash(SkyDirFileTree1.GetSelectNodeFullPath())
      + FThumbView.Selected.Caption;
    if FileExists(sImageFile) then begin
      isGif := SameText('.gif', ExtractFileExt(sImageFile));
      if GetKeyState(VK_SHIFT) < 0 then begin
        if (Application.MessageBox(PChar(Translate('Are you sure to delete the selected picture?')), //'你确定删除当前所选图片吗？'
            PChar(Translate('Delete Picture')), //'删除图片',
            MB_OKCANCEL or MB_ICONQUESTION) = IDOK) then begin
          StopGif;
          if SetFileAttributes(PChar(sImageFile), FILE_ATTRIBUTE_NORMAL) and
             DeleteFile(sImageFile) then begin
            FThumbView.DeleteSelected;
          end else begin
            Application.MessageBox(PChar(Translate(ErrCannotDelMsg)), PChar(Translate('Delete Picture')), MB_OK or MB_ICONINFORMATION);
            if isGif then begin
              Self.ShowSelectImage('', sImageFile);
            end;
          end;
        end;
      end else begin
        StopGif;
        with opStruct do begin
          Wnd := Handle;
          wFunc := FO_DELETE;
          pFrom := PChar(sImageFile + #0); //+ #0 => 使得#0#0结尾!!否则可能出错
          pTo := nil;
          fFlags := FOF_ALLOWUNDO;
          fAnyOperationsAborted := False;
        end;
        if (0 = SHFileOperation(opStruct)) and not opStruct.fAnyOperationsAborted then begin
          FThumbView.DeleteSelected;
        end else begin
          // do not need to show message, windows will show it.
          if isGif then begin
            Self.ShowSelectImage('', sImageFile);
          end;
        end;
      end;
    end;
  end;
end;

procedure TfMain.PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) and (ScrollBox1.HorzScrollBar.Visible or ScrollBox1.VertScrollBar.Visible) then begin
    FLeftDown := True;
    GetCursorPos(FMousePos);
    Screen.Cursor := crSizeAll;
  end;
end;

procedure TfMain.PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  pos: TPoint;
begin
  if FLeftDown then begin
    GetCursorPos(pos);
    if PaintBox1.Width > ScrollBox1.ClientWidth then begin
      with ScrollBox1.HorzScrollBar do Position := Position + FMousePos.X - pos.X;
    end;
    if PaintBox1.Height > ScrollBox1.ClientHeight then begin
      with ScrollBox1.VertScrollBar do Position := Position + FMousePos.Y - pos.Y;
    end;
    FMousePos := pos;
  end;
end;

procedure TfMain.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FLeftDown then begin
    FLeftDown := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfMain.ScrollBox1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var                        //为何不触发“滚动”事件?  2011.5.20
  iScroll: Integer;
begin
  iScroll := (WheelDelta div WHEEL_DELTA) shl 3;
  if ssShift in Shift then begin
    with ScrollBox1.HorzScrollBar do Position := Position + iScroll;
  end else begin
    with ScrollBox1.VertScrollBar do Position := Position + iScroll;
  end;
  Handled := true;
end;

procedure TfMain.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin                      //原来是ScrollBox得不到焦点?!  2011.5.20
  MousePos := ScrollBox1.ScreenToClient(MousePos);
  if PtInRect(ScrollBox1.ClientRect, MousePos) then begin //只好这样啦，呵呵
    ScrollBox1MouseWheel(Self, Shift, WheelDelta, MousePos, Handled);
  end;
end;

procedure TfMain.dlgSavePicTypeChange(Sender: TObject);
var                     //文件类型改变时重置默认“文件扩展名”
  i, idx, iTmp: Integer;
begin
  idx  := 1;
  with dlgSavePic do begin
    for i:=1 to Length(Filter) do begin
      if Filter[i] = '|' then begin
        Inc(idx);
        if idx = (FilterIndex shl 1) then begin
          iTmp := Pos('|', Copy(Filter, i + 3, Length(Filter)));
          if iTmp = 0 then begin
            iTmp := Length(Filter) + 1;
          end;
          DefaultExt := Copy(Filter, i + 3, iTmp - 1);
          if Pos('*', DefaultExt) > 0 then begin
            DefaultExt := '';
          end;
          Break;
        end;
      end;
    end;
  end;
end;

procedure TfMain.ColorDialog1Show(Sender: TObject);
begin
  SendMessage(ColorDialog1.Handle, WM_SETTEXT, 0, LongInt(PChar(FColorDlgTitle)));
end;

procedure TfMain.actSaveAsExecute(Sender: TObject);
var
  fileName: string;
  bmp: TBitmap;
begin
  if FileExists(FLastImgFile) then begin
    dlgSavePic.FileName := GetPureFileName(FLastImgFile);
    if dlgSavePic.Execute then begin
      if FRealBmp.Width > 0 then begin
        bmp := FRealBmp;
      end else begin
        bmp := FDrawBmp;
      end;
      fileName := dlgSavePic.FileName;
      if SameText('.jpg', ExtractFileExt(fileName)) then begin
        with TJpegImage.Create do try
          Assign(bmp);
          SaveToFile(fileName);
        finally
          Free;
        end;
      end else begin
        bmp.SaveToFile(fileName);
      end;
    end;
  end else begin
    Application.MessageBox(PChar(Translate('Please select a picture!')), PChar(Translate('Information'))); // '请选择一张图片！', '提示')
  end;
end;

procedure TfMain.actSaveCurrPicExecute(Sender: TObject);
var                  //2011.11.8 和“另存为”效果差不多，隐藏掉
  fileName: string;
begin
  if FileExists(FLastImgFile) then begin
    dlgSavePic.FileName := GetPureFileName(FLastImgFile);
    if dlgSavePic.Execute then begin
      fileName := dlgSavePic.FileName;
      if SameText('.jpg', ExtractFileExt(fileName)) then begin
        with TJpegImage.Create do try
          Assign(FDrawBmp);
          SaveToFile(fileName);
        finally
          Free;
        end;
      end else begin
        FDrawBmp.SaveToFile(fileName);
      end;
    end;
  end else begin
    Application.MessageBox(PChar(Translate('Please select a picture!')), PChar(Translate('Information'))); // '请选择一张图片！', '提示')
  end;
end;

procedure TfMain.actAutoBrowseExecute(Sender: TObject);
begin
  tmAutoBrowse.Enabled := actAutoBrowse.Checked;
end;

procedure TfMain.actLastPictureExecute(Sender: TObject);
var
  idx: Integer;
begin
  if FThumbView.Items.Count > 1 then begin
    idx := FThumbView.ItemIndex - 1;
    if idx < 0 then idx := FThumbView.Items.Count -1;
    FThumbView.ItemIndex := idx;
    FThumbView.Items[idx].Focused := True;
    FThumbView.Items[idx].Selected := True;
    FThumbView.Items[idx].MakeVisible(False);
  end;
end;

procedure TfMain.actNextPictureExecute(Sender: TObject);
var
  idx: Integer;
begin
  if FThumbView.Items.Count > 1 then begin
    idx := FThumbView.ItemIndex + 1;
    if idx >= FThumbView.Items.Count then idx := 0;
    FThumbView.ItemIndex := idx;
    FThumbView.Items[idx].Focused := True;
    FThumbView.Items[idx].Selected := True;
    FThumbView.Items[idx].MakeVisible(False);
  end;
end;

procedure TfMain.tmGifShowTimer(Sender: TObject);

  procedure UpdateImage(ChangeProc: TChangeProc);
  begin
    FStretched := True;
    ChangeBmp(ChangeProc);
    FStretched := (FRealBmp.Width <> PaintBox1.Width) or (FRealBmp.Height <> PaintBox1.Height);
  end;

const
  cGifIntervals: array[Boolean] of Integer = (100, 1);
var
  g: TGpGraphics;
begin
  tmGifShow.Enabled := False;

  Inc(FGifIndex);
  if FGifIndex >= FGifFrames then FGifIndex := 0;
  SelectActiveFrame(FGifBitmap, FGifIndex);

  with FRealBmp do begin
    Width := FGifBitmap.GetWidth;
    Height := FGifBitmap.GetHeight;
    g := TGpGraphics.Create(Canvas.Handle);
    try
      Canvas.Brush.Color := ScrollBox1.Color;
      Canvas.FillRect(Canvas.ClipRect);
      g.DrawImage(FGifBitmap, Canvas.ClipRect.Left, Canvas.ClipRect.Top, FGifBitmap.GetWidth, FGifBitmap.GetHeight);
    finally
      g.Free;
    end;
  end;

  if actMarkRotateInfo.Checked and (FRotateAngle > 0) then begin
    if FRotateAngle = 90 then begin
      UpdateImage(QuickDextrorotation90);
    end else if FRotateAngle = 180 then begin
      UpdateImage(Rotate180);
    end else {if FRotateAngle = 270 then} begin
      UpdateImage(QuickLevorotation90);
    end;
  end else if (FRealBmp.Width = FDrawBmp.Width) and (FRealBmp.Height = FDrawBmp.Height) then begin
    FDrawBmp.Canvas.Draw(0, 0, FRealBmp);
  end else begin
    AdjustPaintBoxSize(FRealBmp.Width, FRealBmp.Height);
    if (FDrawBmp.Width <> PaintBox1.Width) or (FDrawBmp.Height <> PaintBox1.Height) then begin
      FDrawBmp.Width := PaintBox1.Width;
      FDrawBmp.Height := PaintBox1.Height;
    end;
    StretchLinear(FDrawBmp, FRealBmp);
  end;

  PaintBox1.Invalidate;

  tmGifShow.Interval := Round(FFrameTimes[FGifIndex] * 10 * FGifSpeed);
  if tmGifShow.Interval <= 0 then begin
    tmGifShow.Interval := cGifIntervals[Round(FGifSpeed) = 0];
  end;
  tmGifShow.Enabled  := True;
end;

procedure TfMain.tmAutoBrowseTimer(Sender: TObject);

  procedure FocusItem(ListItem: TListItem);
  begin
    with ListItem do begin
      Selected := True;
      Focused  := True;
      MakeVisible(False);
    end;
  end;

begin
  tmAutoBrowse.Enabled := False;
  if FThumbView.Items.Count > 0 then begin
    if Assigned(FThumbView.Selected) then begin
      if FThumbView.Selected.Index + 1 < FThumbView.Items.Count then begin
        FocusItem(FThumbView.Items[FThumbView.Selected.Index + 1]);
      end else if FCycleBrowse then begin
        FocusItem(FThumbView.Items[0]);
      end else begin
        actAutoBrowse.Checked := False;
        Exit;
      end;
    end else begin
      FocusItem(FThumbView.Items[0]);
    end;
    tmAutoBrowse.Enabled := True;
  end;
end;

{-----------------------------------------------------------------------------
  GIF Image Extra Process
-----------------------------------------------------------------------------}

procedure TfMain.actQuickGifExecute(Sender: TObject);
begin
  if tmGifShow.Enabled then begin
    FGifSpeed := FGifSpeed / 2;
  end;
end;

procedure TfMain.actSlowGifExecute(Sender: TObject);
begin
  if tmGifShow.Enabled then begin
    FGifSpeed := FGifSpeed * 2;
  end;
end;

procedure TfMain.actOriginGifExecute(Sender: TObject);
begin
  if tmGifShow.Enabled then begin
    FGifSpeed := 1;
  end;
end;

{-----------------------------------------------------------------------------
  Image Process Events
-----------------------------------------------------------------------------}

procedure TfMain.actBlurSoftExecute(Sender: TObject);
begin
  ChangeBmp(Blur);
end;

procedure TfMain.actSharpenExecute(Sender: TObject);
begin
  ChangeBmp(Sharpen);
end;

procedure TfMain.actGrayScaleExecute(Sender: TObject);
begin
  ChangeBmp(GrayScale);
end;

procedure TfMain.actReverseColorExecute(Sender: TObject);
begin
  ChangeBmp(NotColor);
end;

procedure TfMain.actExposureExecute(Sender: TObject);
begin
  ChangeBmp(Exposure);
end;

procedure TfMain.actEmbossExecute(Sender: TObject);
begin
  ChangeBmp(Emboss);
end;

procedure TfMain.actEngraveExecute(Sender: TObject);
begin
  ChangeBmp(Engrave);
end;

procedure TfMain.actMergeColorExecute(Sender: TObject);
begin
  FColorDlgTitle := Translate('Set Merge Color'); // '设置图片覆盖颜色';
  ColorDialog1.Color := FLastMergeColor;
  if Assigned(FDrawBmp) and ColorDialog1.Execute then begin
    FLastMergeColor := ColorDialog1.Color;
    ChangeBmp(DoMergeColor);
  end;
end;

procedure TfMain.actCoverExecute(Sender: TObject);
begin
  FColorDlgTitle := Translate('Set Cover Color'); // '设置图片遮罩颜色';
  ColorDialog1.Color := FLastCoverColor;
  if Assigned(FDrawBmp) and ColorDialog1.Execute then begin
    FLastCoverColor := ColorDialog1.Color;
    ChangeBmp(DoCoverWithColor);
  end;
  {if Assigned(FDrawBmp) then begin
    with TMyTrackColorDialog.Create(Self) do try
      Color := FLastCoverColor;
      if Execute then begin
        FLastCoverColor := Color;
        CoverWithColor(FDrawBmp, FLastCoverColor, 50); //todo..
        PaintBox1.Invalidate;
      end;
    finally
      Free;
    end;
  end;}
end;

procedure TfMain.actCoverPercentExecute(Sender: TObject);
var
  form: TForm;
  trackBar: TTrackBar;
begin
  form := TForm.Create(Self);
  with form do try
    Caption := Translate('Set Picture Cover Rate'); // '设置图片遮罩程度';
    Font.Assign(Self.Font);
    BorderStyle  := bsDialog;
    Position     := poScreenCenter;
    ClientWidth  := 250;
    ClientHeight := 80;
    trackBar := TTrackBar.Create(form);
    with trackBar do begin
      Parent := form;
      SetBounds(10, 6, 230, 28);
      Frequency := 10;
      Min := Low(TPercent);
      Max := High(TPercent);
      Position := Self.FCoverPercent;
    end;
    with TButton.Create(form) do begin
      Parent := form;
      SetBounds(30, 46, 75, 26);
      Font.Assign(Self.Font);
      Caption := Translate('&OK'); // '确定(&O)';
      Default := True;
      ModalResult := mrOk;
    end;
    with TButton.Create(form) do begin
      Parent := form;
      SetBounds(250 - 30 - 75, 46, 75, 26);
      Font.Assign(Self.Font);
      Caption := Translate('&Cancel'); // '取消(&C)';
      Cancel := True;
      ModalResult := mrCancel;
    end;
    if ShowModal = mrOk then begin
      FCoverPercent := trackBar.Position;
    end;
  finally
    Free;
  end;
end;

procedure TfMain.actAdjRGBExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    if AdjustRGB(FDrawBmp) then begin
      PaintBox1.Invalidate;
    end;
  end;
end;

procedure TfMain.actAdjHueExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    if AdjustHue(FDrawBmp) then begin
      PaintBox1.Invalidate;
    end;
  end;
end;

procedure TfMain.actAdjBrightnessExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    if AdjustBrightness(FDrawBmp) then begin
      PaintBox1.Invalidate;
    end;
  end;
end;

procedure TfMain.actAdjBrightness2Execute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    if AdjustBrightness2(FDrawBmp) then begin
      PaintBox1.Invalidate;
    end;
  end;
end;

procedure TfMain.actAdjSaturationExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    if AdjustSaturation(FDrawBmp) then begin
      PaintBox1.Invalidate;
    end;
  end;
end;

procedure TfMain.actEnhanceContrastExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    EnhanceContrast(FDrawBmp, 3);
    PaintBox1.Invalidate;
  end;
end;

procedure TfMain.actWeakenContrastExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    WeakenContrast(FDrawBmp, 3);
    PaintBox1.Invalidate;
  end;
end;

procedure TfMain.actHorzMirrorExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    HorzMirror(FDrawBmp);
    PaintBox1.Invalidate;
  end;
end;

procedure TfMain.actVertMirrorExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    VertMirror(FDrawBmp);
    PaintBox1.Invalidate;
  end;
end;

procedure TfMain.actLevorotation90Execute(Sender: TObject);
begin            //对TIF，用GDI+直接旋转速度更快
  ChangeBmp(QuickLevorotation90);
  AdjustRotateInfo(FLastImgFile, 270);
end;

procedure TfMain.actDextrorotation90Execute(Sender: TObject);
begin            //对TIF，用GDI+直接旋转速度更快
  ChangeBmp(QuickDextrorotation90);
  AdjustRotateInfo(FLastImgFile, 90);
end;

procedure TfMain.actRotate180Execute(Sender: TObject);
begin            //对TIF，用GDI+直接旋转速度更快
  ChangeBmp(Rotate180);
  AdjustRotateInfo(FLastImgFile, 180);
end;

procedure TfMain.actWoodcutExecute(Sender: TObject);
begin
  ChangeBmp(Woodcut);
end;

procedure TfMain.actWatercolorExecute(Sender: TObject);
begin
  ChangeBmp(Watercolor);
end;

procedure TfMain.actCanvasOutExecute(Sender: TObject);
begin
  ChangeBmp(DoCanvasOut);
end;

procedure TfMain.actLightExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    if RunLight(FDrawBmp, FLightX, FLightY, FLightM, FLightN) then begin
      PaintBox1.Invalidate;
    end;
  end;
end;

procedure TfMain.actBrightDarkExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) then begin
    BrightDark(FDrawBmp, 2);
    PaintBox1.Invalidate;
  end;
end;

procedure TfMain.actAdjSizeExecute(Sender: TObject);
begin
  if Assigned(FDrawBmp) and (FLastPicWidth > 0) then begin
    if AdjImageSize(FDrawBmp, ScrollBox1.Color, FLastImgFile, FLastPicWidth, FLastPicHeight, FScaleAdj) then begin
      AdjustPaintBoxSize(FDrawBmp.Width, FDrawBmp.Height);
      ShowImageProps(FLastPicWidth, FLastPicHeight, False);
      PaintBox1.Invalidate;
    end;
  end;
end;

end.
