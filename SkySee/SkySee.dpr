program SkySee;

uses
  Forms,
  ssMain in 'ssMain.pas' {fMain},
  SelDirEx in '..\_MyLib\SelDirEx.pas',
  PicFun in '..\_MyLib\PicFun.pas',
  SkyPub in '..\_MyLib\SkyPub.pas',
  GDIPAPI in '..\_gdi+\GDIPAPI.pas',
  GDIPOBJ in '..\_gdi+\GDIPOBJ.pas',
  GdipUtil in '..\_gdi+\GdipUtil.pas',
  GdipBitmap in '..\_gdi+\GdipBitmap.pas',
  ShellFun in '..\_MyLib\ShellFun.pas',
  MyGraphic in '..\_MyLib\MyGraphic.pas',
  HashTable in '..\_MyLib\HashTable.pas',
  SkyTranslator in '..\_MyLib\SkyTranslator.pas',
  ssAdjRGB in 'ssAdjRGB.pas' {fAdjRGB},
  ssDblView in 'ssDblView.pas' {frmDblView: TFrame},
  ssAdjBrightness in 'ssAdjBrightness.pas' {fAdjBrightness},
  ssAdjSaturation in 'ssAdjSaturation.pas' {fAdjSaturation},
  ssLightParam in 'ssLightParam.pas' {fLightParam},
  ssAbout in 'ssAbout.pas' {fAbout},
  MultiFileProps in '..\_MyLib\MultiFileProps.pas',
  RGBHSV in '..\_MyLib\RGBHSV.pas',
  MyGifPub in '..\_gdi+\MyGifPub.pas',
  ssAdjSize in 'ssAdjSize.pas' {fAdjSize};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfMain, fMain);
  Application.Run;
end.
