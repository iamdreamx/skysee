unit ssAdjRGB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, CommCtrl, MyGraphic, ssDblView, SkyTranslator;

type
  TfAdjRGB = class(TForm)
    frmAdjView: TfrmDblView;
    gbChgRGB: TGroupBox;
    lbRedVal: TLabel;
    lbGreenVal: TLabel;
    lbBlueVal: TLabel;
    lbGreenTip: TLabel;
    lbBlueTip: TLabel;
    lbRedTip: TLabel;
    tkbRed: TTrackBar;
    tkbGreen: TTrackBar;
    tkbBlue: TTrackBar;
    btOk: TButton;
    btCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure tkbRGBChange(Sender: TObject);
  private
    { Private declarations }
    FBitmap: TBitmap;
  public
    { Public declarations }
    property Bitmap: TBitmap read FBitmap write FBitmap;
  end;

  function AdjustRGB(const Bmp: TBitmap): Boolean;

var
  fAdjRGB: TfAdjRGB;

implementation

{$R *.dfm}

function AdjustRGB(const Bmp: TBitmap): Boolean;
begin
  with TfAdjRGB.Create(Application) do try
    Bitmap := Bmp;
    Result := ShowModal = mrOk;
    if Result then begin
      AdjRGB(Bmp, Bmp, tkbRed.Position, tkbGreen.Position, tkbBlue.Position);
    end;
  finally
    Free;
  end;
end;

procedure TfAdjRGB.FormShow(Sender: TObject);
begin
  with tkbRed do begin   //去掉TrackBar那个丑陋的选择区
    SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE) and not TBS_ENABLESELRANGE);
  end;
  with tkbGreen do begin //去掉TrackBar那个丑陋的选择区
    SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE) and not TBS_ENABLESELRANGE);
  end;
  with tkbBlue do begin  //去掉TrackBar那个丑陋的选择区
    SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE) and not TBS_ENABLESELRANGE);
  end;

  tkbRed.Tag   := Integer(lbRedVal);
  tkbGreen.Tag := Integer(lbGreenVal);
  tkbBlue.Tag  := Integer(lbBlueVal);

  frmAdjView.InitBitmap(FBitmap);
  AdjRGB(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj, 0, 0, 0);
  frmAdjView.InvalidateBmpOfBeforeAdj;
  frmAdjView.InvalidateBmpOfAfterAdj;

  if Assigned(Self.Owner) then Translate(Self);
end;

procedure TfAdjRGB.tkbRGBChange(Sender: TObject);
begin
  with Sender as TTrackBar do begin
    if Tag <> 0 then begin
      TLabel(Tag).Caption := IntToStr(Position);
    end;
  end;
  AdjRGB(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj,
    tkbRed.Position, tkbGreen.Position, tkbBlue.Position);
  frmAdjView.InvalidateBmpOfAfterAdj;
end;

end.
