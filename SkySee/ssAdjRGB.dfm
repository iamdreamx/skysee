object fAdjRGB: TfAdjRGB
  Left = 205
  Top = 104
  BorderStyle = bsDialog
  BorderWidth = 2
  Caption = #35843#25972#19977#21407#33394
  ClientHeight = 423
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inline frmAdjView: TfrmDblView
    Left = 0
    Top = 0
    Width = 430
    Height = 241
    TabOrder = 0
    inherited pnView: TPanel
      Height = 241
    end
  end
  object gbChgRGB: TGroupBox
    Left = 0
    Top = 247
    Width = 430
    Height = 138
    Caption = ' '#19977#21407#33394#35843#25972' '
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object lbRedVal: TLabel
      Tag = -1
      Left = 102
      Top = 28
      Width = 6
      Height = 12
      Caption = '0'
    end
    object lbGreenVal: TLabel
      Tag = -1
      Left = 102
      Top = 64
      Width = 6
      Height = 12
      Caption = '0'
    end
    object lbBlueVal: TLabel
      Tag = -1
      Left = 102
      Top = 100
      Width = 6
      Height = 12
      Caption = '0'
    end
    object lbGreenTip: TLabel
      Left = 48
      Top = 64
      Width = 24
      Height = 12
      Caption = #32511#65306
    end
    object lbBlueTip: TLabel
      Left = 48
      Top = 100
      Width = 24
      Height = 12
      Caption = #34013#65306
    end
    object lbRedTip: TLabel
      Left = 48
      Top = 28
      Width = 24
      Height = 12
      Caption = #32418#65306
    end
    object tkbRed: TTrackBar
      Left = 128
      Top = 24
      Width = 281
      Height = 33
      Max = 255
      Min = -255
      PageSize = 10
      Frequency = 10
      TabOrder = 0
      OnChange = tkbRGBChange
    end
    object tkbGreen: TTrackBar
      Left = 128
      Top = 60
      Width = 281
      Height = 25
      Max = 255
      Min = -255
      PageSize = 10
      Frequency = 10
      TabOrder = 1
      OnChange = tkbRGBChange
    end
    object tkbBlue: TTrackBar
      Left = 128
      Top = 96
      Width = 281
      Height = 30
      Max = 255
      Min = -255
      PageSize = 10
      Frequency = 10
      TabOrder = 2
      OnChange = tkbRGBChange
    end
  end
  object btOk: TButton
    Left = 272
    Top = 392
    Width = 75
    Height = 26
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object btCancel: TButton
    Left = 352
    Top = 392
    Width = 75
    Height = 26
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
