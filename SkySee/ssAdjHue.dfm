object fAdjHue: TfAdjHue
  Left = 205
  Top = 104
  BorderStyle = bsDialog
  BorderWidth = 2
  Caption = #35843#25972#33394#35843
  ClientHeight = 347
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inline frmAdjView: TfrmDblView
    Left = 0
    Top = 0
    Width = 430
    Height = 239
    TabOrder = 0
  end
  object gbChgHue: TGroupBox
    Left = 0
    Top = 242
    Width = 430
    Height = 66
    Caption = ' '#33394#35843#35843#25972' '
    TabOrder = 1
    object lbHueTip: TLabel
      Left = 22
      Top = 28
      Width = 42
      Height = 12
      Caption = #33394#35843#65306' '
    end
    object lbHueVal: TLabel
      Tag = -1
      Left = 96
      Top = 28
      Width = 18
      Height = 12
      Caption = '180'
    end
    object tkbHue: TTrackBar
      Left = 130
      Top = 24
      Width = 281
      Height = 31
      Max = 360
      PageSize = 10
      Frequency = 10
      Position = 180
      TabOrder = 0
      OnChange = tkbHueChange
    end
  end
  object btOk: TButton
    Left = 274
    Top = 315
    Width = 75
    Height = 26
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object btCancel: TButton
    Left = 354
    Top = 315
    Width = 75
    Height = 26
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
