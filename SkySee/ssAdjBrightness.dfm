object fAdjBrightness: TfAdjBrightness
  Left = 205
  Top = 104
  BorderStyle = bsDialog
  BorderWidth = 2
  Caption = #35843#25972#20142#24230
  ClientHeight = 347
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inline frmAdjView: TfrmDblView
    Left = 0
    Top = 0
    Width = 430
    Height = 241
    TabOrder = 0
    inherited pnView: TPanel
      Height = 241
    end
  end
  object btOk: TButton
    Left = 274
    Top = 315
    Width = 75
    Height = 26
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object btCancel: TButton
    Left = 354
    Top = 315
    Width = 75
    Height = 26
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object gbChgBrightness: TGroupBox
    Left = 0
    Top = 245
    Width = 430
    Height = 62
    Caption = ' '#20142#24230#35843#25972' '
    TabOrder = 1
    object lbBrightnessTip: TLabel
      Left = 22
      Top = 25
      Width = 36
      Height = 12
      Caption = #20142#24230#65306
    end
    object lbBrightnessVal: TLabel
      Tag = -1
      Left = 96
      Top = 24
      Width = 7
      Height = 16
      Caption = '0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object tkbBrightness: TTrackBar
      Left = 128
      Top = 20
      Width = 281
      Height = 33
      Max = 255
      Min = -255
      PageSize = 10
      Frequency = 10
      TabOrder = 0
      OnChange = tkbBrightnessChange
    end
  end
end
