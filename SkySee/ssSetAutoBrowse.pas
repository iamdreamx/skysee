unit ssSetAutoBrowse;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Spin, SkyTranslator;

type
  TfSetAutoBrowse = class(TForm)
    lbInterval: TLabel;
    sedInterval: TSpinEdit;
    lbUnitTip: TLabel;
    ckbCycleBrowse: TCheckBox;
    btOk: TButton;
    btCancel: TButton;
    Bevel1: TBevel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSetAutoBrowse: TfSetAutoBrowse;

implementation

{$R *.dfm}

procedure TfSetAutoBrowse.FormCreate(Sender: TObject);
begin
  if Assigned(Self.Owner) then Translate(Self);
end;

end.
