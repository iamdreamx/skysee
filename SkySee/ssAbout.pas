unit ssAbout;

interface

  {At the XE4 release, the Delphi compilers were changed to accept either $IFEND
   or $ENDIF to close $IF statements. Before XE4, only $IFEND could be used to
   close $IF statements.
  The $LEGACYIFEND directive allows you to restore the old behavior, which is
   useful if your code is emitting E2029 related to nested $IF and $IFDEF
   statements.}
  {.$IF CompilerVersion >= 25.0} //Delphi XE4+
  {$IFDEF LEGACYIFEND}
  {$LEGACYIFEND ON}
  {$ENDIF}
  {.$IFEND}

  {$IFDEF conditionalexpressions}
  {$IF CompilerVersion >= 22.0} //Delphi XE+
    {$DEFINE DelphiXEUp}
  {$IFEND}
  {$IF CompilerVersion >= 23.0} //Delphi XE2+
    {$DEFINE  DelphiXE2Up}
  {$IFEND}
  {$IF CompilerVersion >= 24.0} //Delphi XE3+
    {$DEFINE  DelphiXE3Up}
  {$IFEND}
  {$IF CompilerVersion >= 31.0} //Delphi Berlin
    {$DEFINE  DelphiBerlin10Dot1Up}
  {$IFEND}
  {$IF CompilerVersion >= 33.0} //Delphi Rio
    {$DEFINE  DelphiRio10Dot3Up}
  {$IFEND}
  {$ENDIF}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
{$IFDEF UNICODE}
  {$IF CompilerVersion >= 23.0} //Delphi XE2+
  System.Types, System.UITypes,
  {$IFEND}
{$ENDIF}
  Dialogs, jpeg, ExtCtrls, StdCtrls, ShellApi, Math, PicFun, SkyTranslator;

type
  TfAbout = class(TForm)
    imgAbout: TImage;
    lbEMailBack: TLabel;
    lbEMailFront: TLabel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure imgAboutMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgAboutMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbEMailMouseEnter(Sender: TObject);
    procedure lbEMailMouseLeave(Sender: TObject);
    procedure lbEMailFrontClick(Sender: TObject);
  private
    { Private declarations }
    FLeftDown: Boolean;
    FVerTip: string;
    FLogoTip: string;
    FRightTip: string;
    {$IFNDEF DelphiRio10Dot3Up}
    FScaleFactor: Single;
    {$ENDIF}
    FBackBmp: TBitmap;
    procedure InitBackgroundBmp;
  protected
    procedure ChangeScale(M, D: Integer{$IFDEF DelphiBerlin10Dot1Up}; isDpiChange: Boolean{$ENDIF}); override;
    {$IFNDEF DelphiRio10Dot3Up}
    property ScaleFactor: Single read FScaleFactor;
    {$ENDIF}
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  fAbout: TfAbout;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Internal functions
-----------------------------------------------------------------------------}

procedure DrawHollowText(ARect: TRect; ACanvas: TCanvas; const S: string);
const
  Offset = 1;
var                     //画空心字
  r: TRect;
  cl: TColor;
  clSolid: TColor;
begin
  with ACanvas do begin
    Lock;
    try
      clSolid := Brush.Color;
      Brush.Style := bsClear;
      r := ARect;
      cl := Font.Color;
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, 0, -Offset);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, -Offset, 0);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, Offset*2, 0);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, 0, Offset);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, 0, Offset);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, -Offset, 0);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, -Offset, 0);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      InflateRect(ARect, 0, -Offset);
      DrawText(Handle, PChar(S), Length (S), ARect, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      Font.Color := clSolid;
      InflateRect(ARect, 0, Offset);
      DrawText(Handle, PChar(S), Length (S), r, DT_LEFT or DT_SINGLELINE or DT_VCENTER);
      Font.Color := cl;
    finally
      Unlock;
    end;
  end;
end;

function GetFileVersion(const FileName: string; var ma, mi, r ,b: Integer): Boolean;
var
  pver: PVSFixedFileInfo;
  len: LongWord;
  buf: string;
  pBuf: PChar;
begin
  Result := False;
  len := GetFileVersionInfoSize(PChar(FileName), len);
  if len > 0 then begin
    SetLength(buf, len);
    pBuf := @buf[1];
    ZeroMemory(pBuf, len);
    if GetFileVersionInfo(PChar(FileName), 0, len, Pointer(pBuf)) then begin
      if VerQueryValue(pBuf, '\', Pointer(pver), len) then begin
        ma := pver.dwFileVersionMS shr 16;
        mi := pver.dwFileVersionMS and $0000FFFF;
        r  := pver.dwFileVersionLS shr 16;
        b  := pver.dwFileVersionLS and $0000FFFF;
        Result := True;
      end;
    end;
  end;
end;

function GetFileVerString(const VerTip, FileName: string): string;
var
  ma, mi, r ,b: Integer;
begin
  if GetFileVersion(FileName, ma, mi, r, b) then begin
    Result := Format(VerTip + ': %d.%d.%d.%d', [ma, mi, r, b]);
  end else begin
    Result := '';
  end;
end;

{-----------------------------------------------------------------------------
  Form Events
-----------------------------------------------------------------------------}

constructor TfAbout.Create(AOwner: TComponent);
{$IFNDEF DelphiRio10Dot3Up}
var
  currPPI: Integer;
{$ENDIF}
begin
  inherited;

  {$IFNDEF DelphiRio10Dot3Up}
  {$IFDEF DelphiBerlin10Dot1Up}
  if Assigned(Monitor) then
    currPPI := Monitor.PixelsPerInch
  else
  {$ENDIF}
    currPPI := Screen.PixelsPerInch;

  FScaleFactor := currPPI / 96.0;
  {$ENDIF}

  FBackBmp := TBitmap.Create;
  InitBackgroundBmp;
end;

destructor TfAbout.Destroy;
begin
  FBackBmp.Free;
  inherited;
end;

procedure TfAbout.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Word(Key) in [VK_RETURN, VK_ESCAPE] then begin
    Close;
  end;
end;

procedure TfAbout.FormShow(Sender: TObject);
begin
  Self.DoubleBuffered := True;

  if Assigned(Self.Owner) then Translate(Self);
  lbEMailFront.Hint := Translate(lbEMailFront.Hint); //作者不保证回信哦，呵呵^o^
  FVerTip := Translate('Version');
  FLogoTip := Translate('LongWind Picture Browser'); // '长风图片浏览器'
  FRightTip := Translate('Copy Right, Copy OK');     // '版权所有  翻印不究'
end;

procedure TfAbout.FormPaint(Sender: TObject);

  function myScaleValue(Value: Integer): Integer;
  begin
    Result := Round(Value * ScaleFactor);
  end;

var
  r: TRect;
  sVer: string;
begin
  Canvas.Draw(0, 0, FBackBmp);

  Canvas.Font.Assign(Font);
  Canvas.Font.Style := [fsBold];
  Canvas.Font.Size := myScaleValue(14);
  Canvas.Font.Color := clWindowText;
  Canvas.Brush.Color := clWindow;
  DrawHollowText(Rect((ClientWidth - Canvas.TextWidth(FLogoTip)) div 2 , myScaleValue(100), ClientWidth, myScaleValue(132)), Canvas, FLogoTip);

  Canvas.Font.Size := myScaleValue(10);
  r := Rect(myScaleValue(10), myScaleValue(136), ClientWidth -myScaleValue(15), myScaleValue(160));
  sVer := GetFileVerString(FVerTip, Application.ExeName);
  DrawText(Canvas.Handle, PChar(sVer), Length(sVer), r, DT_SINGLELINE or DT_CENTER or DT_VCENTER);

  r := Rect(myScaleValue(10), myScaleValue(265), ClientWidth -myScaleValue(10), myScaleValue(285));
  DrawText(Canvas.Handle, PChar(FRightTip), Length(FRightTip), r, DT_SINGLELINE or DT_RIGHT or DT_VCENTER);
end;

procedure TfAbout.ChangeScale(M, D: Integer{$IFDEF DelphiBerlin10Dot1Up}; isDpiChange: Boolean{$ENDIF});
begin
  inherited;
  {$IFNDEF DelphiRio10Dot3Up}
  if M <> D then begin
    FScaleFactor := FScaleFactor * M / D;
    InitBackgroundBmp;
  end;
  {$ENDIF}
end;

{-----------------------------------------------------------------------------
  Control Events
-----------------------------------------------------------------------------}

procedure TfAbout.imgAboutMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FLeftDown := Button = mbLeft;
end;

procedure TfAbout.imgAboutMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FLeftDown then begin
    FLeftDown := True;
    Close;
  end;
end;

procedure TfAbout.InitBackgroundBmp;
{$IFDEF DelphiBerlin10Dot1Up}
var
  bmpTmp: TBitmap;
{$ENDIF}
begin
  {$IFDEF DelphiBerlin10Dot1Up}
  if ScaleFactor > 1 then begin
    bmpTmp := TBitmap.Create;
    try
      bmpTmp.SetSize(imgAbout.Width, imgAbout.Height);
      bmpTmp.PixelFormat := pf24bit;
      bmpTmp.Canvas.Draw(0, 0, imgAbout.Picture.Graphic);
      FBackBmp.SetSize(Width, Height);
      FBackBmp.PixelFormat := pf24bit;
      StretchLinear(FBackBmp, bmpTmp);
    finally
      bmpTmp.Free;
    end;
  end else begin
  {$ENDIF}
    FBackBmp.Assign(Self.imgAbout.Picture.Graphic);
  {$IFDEF DelphiBerlin10Dot1Up}
  end;
  {$ENDIF}
end;

procedure TfAbout.lbEMailMouseEnter(Sender: TObject);
begin
  lbEMailBack.Font.Style := [fsUnderline];
  lbEMailFront.Font.Style := [fsUnderline];
  lbEMailFront.Font.Color := clRed;
end;

procedure TfAbout.lbEMailMouseLeave(Sender: TObject);
begin
  lbEMailBack.Font.Style := [];
  lbEMailFront.Font.Style := [];
  lbEMailFront.Font.Color := clBlue;
end;

procedure TfAbout.lbEMailFrontClick(Sender: TObject);
begin
  ShellExecute(0, 'open', PChar('mailto:' + lbEMailFront.Caption), nil, nil, SW_SHOW);
end;

end.
