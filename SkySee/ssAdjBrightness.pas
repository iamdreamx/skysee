unit ssAdjBrightness;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, CommCtrl, MyGraphic, ssDblView, SkyTranslator;

type
  TAdjBrightnessProc = procedure(SrcBmp, DstBmp: TBitmap; AdjVar: Integer);

  TfAdjBrightness = class(TForm)
    frmAdjView: TfrmDblView;
    btOk: TButton;
    btCancel: TButton;
    gbChgBrightness: TGroupBox;
    lbBrightnessTip: TLabel;
    lbBrightnessVal: TLabel;
    tkbBrightness: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tkbBrightnessChange(Sender: TObject);
  private
    { Private declarations }
    FBitmap: TBitmap;
    FAdjProc: TAdjBrightnessProc;
  public
    { Public declarations }
    property Bitmap: TBitmap read FBitmap write FBitmap;
  end;

  function AdjustBrightness(const Bmp: TBitmap): Boolean;
  function AdjustBrightness2(const Bmp: TBitmap): Boolean;

var
  fAdjBrightness: TfAdjBrightness;

implementation

{$R *.dfm}

function AdjustBrightness(const Bmp: TBitmap): Boolean;
begin
  with TfAdjBrightness.Create(Application) do try
    Bitmap := Bmp;
    FAdjProc := AdjBrightness;
    Result := ShowModal = mrOk;
    if Result then begin
      AdjBrightness(Bmp, Bmp, tkbBrightness.Position);
    end;
  finally
    Free;
  end;
end;

function AdjustBrightness2(const Bmp: TBitmap): Boolean;
begin
  with TfAdjBrightness.Create(Application) do try
    Caption := Caption + Format(' (%s)', [Translate('Algorithm may from PhotoShop')]); //算法据说来自PhotoShop
    Bitmap := Bmp;
    FAdjProc := AdjBrightness2;
    Result := ShowModal = mrOk;
    if Result then begin
      AdjBrightness2(Bmp, Bmp, tkbBrightness.Position);
    end;
  finally
    Free;
  end;
end;

procedure TfAdjBrightness.FormCreate(Sender: TObject);
begin
  if Assigned(Self.Owner) then Translate(Self);
end;

procedure TfAdjBrightness.FormShow(Sender: TObject);
begin
  with tkbBrightness do begin //去掉TrackBar那个丑陋的选择区
    SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE) and not TBS_ENABLESELRANGE);
  end;

  frmAdjView.InitBitmap(FBitmap);
  //AdjBrightness(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj, 0);
  frmAdjView.InvalidateBmpOfBeforeAdj;
  frmAdjView.InvalidateBmpOfAfterAdj;
end;

procedure TfAdjBrightness.tkbBrightnessChange(Sender: TObject);
begin
  lbBrightnessVal.Caption := IntToStr(tkbBrightness.Position);
  if Assigned(FAdjProc) then begin
    FAdjProc(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj, tkbBrightness.Position);
  end;
  frmAdjView.InvalidateBmpOfAfterAdj;
end;

end.
