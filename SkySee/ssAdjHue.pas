unit ssAdjHue;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, CommCtrl, MyGraphic, ssDblView, SkyTranslator;

type
  TfAdjHue = class(TForm)
    frmAdjView: TfrmDblView;
    gbChgHue: TGroupBox;
    lbHueTip: TLabel;
    lbHueVal: TLabel;
    tkbHue: TTrackBar;
    btOk: TButton;
    btCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure tkbHueChange(Sender: TObject);
  private
    { Private declarations }
    FBitmap: TBitmap;
  public
    { Public declarations }
    property Bitmap: TBitmap read FBitmap write FBitmap;
  end;

  function AdjustHue(const Bmp: TBitmap): Boolean;

var
  fAdjHue: TfAdjHue;

implementation

{$R *.dfm}

function AdjustHue(const Bmp: TBitmap): Boolean;
begin
  with TfAdjHue.Create(Application) do try
    Bitmap := Bmp;
    Result := ShowModal = mrOk;
    if Result then begin
      AdjHue(Bmp, Bmp, tkbHue.Position);
    end;
  finally
    Free;
  end;
end;

procedure TfAdjHue.FormShow(Sender: TObject);
begin
  with tkbHue do begin //去掉TrackBar那个丑陋的选择区
    SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE) and not TBS_ENABLESELRANGE);
  end;
  
  frmAdjView.InitBitmap(FBitmap);
  AdjHue(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj, tkbHue.Position);
  frmAdjView.InvalidateBmpOfBeforeAdj;
  frmAdjView.InvalidateBmpOfAfterAdj;

  if Assigned(Self.Owner) then Translate(Self);
end;

procedure TfAdjHue.tkbHueChange(Sender: TObject);
begin
  lbHueVal.Caption := IntToStr(tkbHue.Position);
  AdjHue(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj, tkbHue.Position);
  frmAdjView.InvalidateBmpOfAfterAdj;
end;

end.
