object fAdjSize: TfAdjSize
  Left = 205
  Top = 104
  BorderStyle = bsDialog
  Caption = #35843#25972#22270#29255#22823#23567
  ClientHeight = 192
  ClientWidth = 229
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object lbImgWidth: TLabel
    Left = 30
    Top = 59
    Width = 87
    Height = 12
    Alignment = taRightJustify
    Caption = #22270#29255#23485#24230'(&K): '
    FocusControl = edImgWidth
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbImgHeight: TLabel
    Left = 30
    Top = 107
    Width = 87
    Height = 12
    Alignment = taRightJustify
    Caption = #22270#29255#39640#24230'(&G): '
    FocusControl = edImgHeight
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbOrgWidthTip: TLabel
    Left = 30
    Top = 37
    Width = 84
    Height = 12
    Alignment = taRightJustify
    Caption = #22270#29255#21407#22987#23485#24230': '
  end
  object lbOrgWidth: TLabel
    Tag = -1
    Left = 120
    Top = 37
    Width = 18
    Height = 12
    Caption = '800'
  end
  object lbOrgHeightTip: TLabel
    Left = 30
    Top = 85
    Width = 84
    Height = 12
    Alignment = taRightJustify
    Caption = #22270#29255#21407#22987#39640#24230': '
  end
  object lbOrgHeight: TLabel
    Tag = -1
    Left = 120
    Top = 85
    Width = 18
    Height = 12
    Caption = '600'
  end
  object bvlImgWidth: TBevel
    Left = 183
    Top = 56
    Width = 16
    Height = 20
  end
  object bvlImgHeight: TBevel
    Left = 183
    Top = 104
    Width = 16
    Height = 20
  end
  object lbScaleAdjTip: TLabel
    Left = 17
    Top = 11
    Width = 100
    Height = 12
    Alignment = taRightJustify
    Caption = #25353#27604#20363#35843#25972'(&D): '
    FocusControl = ckbScaleAdj
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edImgWidth: TEdit
    Left = 118
    Top = 56
    Width = 66
    Height = 20
    TabOrder = 1
    Text = '1'
    OnExit = edImgWidthExit
  end
  object udImgWidth: TUpDown
    Left = 184
    Top = 56
    Width = 14
    Height = 20
    Associate = edImgWidth
    Min = 1
    Max = 10000
    Position = 1
    TabOrder = 2
    OnChanging = udImgWidthChanging
  end
  object edImgHeight: TEdit
    Left = 118
    Top = 104
    Width = 66
    Height = 20
    TabOrder = 3
    Text = '1'
    OnExit = edImgHeightExit
  end
  object udImgHeight: TUpDown
    Left = 184
    Top = 104
    Width = 14
    Height = 20
    Associate = edImgHeight
    Min = 1
    Max = 10000
    Position = 1
    TabOrder = 4
    OnChanging = udImgHeightChanging
  end
  object btOk: TButton
    Left = 30
    Top = 148
    Width = 75
    Height = 26
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object btCancel: TButton
    Left = 123
    Top = 148
    Width = 75
    Height = 26
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object ckbScaleAdj: TCheckBox
    Tag = -1
    Left = 118
    Top = 9
    Width = 21
    Height = 21
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
end
