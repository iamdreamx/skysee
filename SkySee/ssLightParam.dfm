object fLightParam: TfLightParam
  Left = 205
  Top = 104
  BorderStyle = bsDialog
  Caption = #35774#32622#28783#20809#25928#26524#21442#25968
  ClientHeight = 188
  ClientWidth = 276
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    276
    188)
  PixelsPerInch = 96
  TextHeight = 12
  object lbX: TLabel
    Tag = -1
    Left = 4
    Top = 9
    Width = 32
    Height = 16
    Alignment = taRightJustify
    Caption = ' &X: '
    FocusControl = edX
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbY: TLabel
    Tag = -1
    Left = 4
    Top = 39
    Width = 32
    Height = 16
    Alignment = taRightJustify
    Caption = ' &Y: '
    FocusControl = edY
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbM: TLabel
    Tag = -1
    Left = 4
    Top = 80
    Width = 32
    Height = 16
    Alignment = taRightJustify
    Caption = ' &M: '
    FocusControl = edM
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbN: TLabel
    Tag = -1
    Left = 4
    Top = 110
    Width = 32
    Height = 16
    Alignment = taRightJustify
    Caption = ' &N: '
    FocusControl = edN
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object bvlX: TBevel
    Left = 102
    Top = 8
    Width = 16
    Height = 20
  end
  object Bevel1: TBevel
    Left = 102
    Top = 37
    Width = 16
    Height = 20
  end
  object Bevel2: TBevel
    Left = 102
    Top = 78
    Width = 16
    Height = 20
  end
  object Bevel3: TBevel
    Left = 102
    Top = 108
    Width = 16
    Height = 20
  end
  object Bevel4: TBevel
    Left = 102
    Top = 7
    Width = 16
    Height = 20
  end
  object edX: TEdit
    Left = 37
    Top = 8
    Width = 66
    Height = 20
    TabOrder = 0
    Text = '230'
  end
  object udX: TUpDown
    Left = 103
    Top = 8
    Width = 14
    Height = 20
    Associate = edX
    Max = 30000
    Position = 230
    TabOrder = 1
  end
  object edY: TEdit
    Left = 37
    Top = 38
    Width = 66
    Height = 20
    TabOrder = 2
    Text = '160'
  end
  object udY: TUpDown
    Left = 103
    Top = 38
    Width = 14
    Height = 20
    Associate = edY
    Max = 30000
    Position = 160
    TabOrder = 3
  end
  object edM: TEdit
    Left = 37
    Top = 79
    Width = 66
    Height = 20
    TabOrder = 4
    Text = '200'
  end
  object udM: TUpDown
    Left = 103
    Top = 79
    Width = 14
    Height = 20
    Associate = edM
    Max = 10000
    Position = 200
    TabOrder = 5
  end
  object edN: TEdit
    Left = 37
    Top = 109
    Width = 66
    Height = 20
    TabOrder = 6
    Text = '6'
  end
  object udN: TUpDown
    Left = 103
    Top = 109
    Width = 14
    Height = 20
    Associate = edN
    Max = 1000
    Position = 6
    TabOrder = 7
  end
  object btOk: TButton
    Left = 48
    Top = 146
    Width = 75
    Height = 26
    Anchors = [akLeft, akBottom]
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 8
  end
  object btCancel: TButton
    Left = 153
    Top = 146
    Width = 75
    Height = 26
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 9
  end
  object pnTip: TPanel
    Left = 122
    Top = 8
    Width = 151
    Height = 121
    BevelOuter = bvLowered
    TabOrder = 10
    object lbTipTitle: TLabel
      Left = 1
      Top = 1
      Width = 149
      Height = 20
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = #28783#20809#28388#38236#31639#27861'  '
      Layout = tlCenter
    end
    object lbTipContent: TLabel
      Left = 1
      Top = 21
      Width = 149
      Height = 99
      Align = alClient
      Caption = '    '#22312#22270#20687#21306#22495#20869#21462#19968#28857#20809#28304#65292#20174#20809#32447#26411#31471#24320#22987#21521#20809#28304#28857#22686#21152#20142#24230#65292#21363#21521#30333#33394#25509#36817#12290#13#10'    '#22686#21152#20142#24230#30340#31243#24230#19982#31163#20809#28304#30340#36317#31163#26377#20851#12290
      WordWrap = True
    end
  end
end
