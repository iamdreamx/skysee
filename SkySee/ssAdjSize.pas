unit ssAdjSize;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, GdiPObj, ExtCtrls, SkyTranslator;

type
  TfAdjSize = class(TForm)
    lbImgWidth: TLabel;
    lbImgHeight: TLabel;
    edImgWidth: TEdit;
    udImgWidth: TUpDown;
    edImgHeight: TEdit;
    udImgHeight: TUpDown;
    btOk: TButton;
    btCancel: TButton;
    ckbScaleAdj: TCheckBox;
    lbOrgWidthTip: TLabel;
    lbOrgWidth: TLabel;
    lbOrgHeightTip: TLabel;
    lbOrgHeight: TLabel;
    bvlImgWidth: TBevel;
    bvlImgHeight: TBevel;
    lbScaleAdjTip: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure udImgWidthChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure udImgHeightChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure edImgWidthExit(Sender: TObject);
    procedure edImgHeightExit(Sender: TObject);
  private
    { Private declarations }
    FRatioOfWH: Double;
    procedure HookChangeEvent;
    procedure UnhookChangeEvent;
  public
    { Public declarations }
  end;

var
  fAdjSize: TfAdjSize;

function AdjImageSize(const Bmp: TBitmap; vBkColor: TColor;
  const ImageFile: string; OrgImgWidth, OrgImgHeight: Integer;
  var ScaleAdj: Boolean): Boolean;

implementation

{$R *.dfm}

function AdjImageSize(const Bmp: TBitmap; vBkColor: TColor;
  const ImageFile: string; OrgImgWidth, OrgImgHeight: Integer;
  var ScaleAdj: Boolean): Boolean;

  procedure DoAdjSize(AdjWidth, AdjHeight: Integer);
  var
    gpBmp: TGpBitmap;
    g: TGPGraphics;
  begin
    gpBmp := TGpBitmap.Create(ImageFile);
    try
      Bmp.Canvas.Lock;
      try
        Bmp.Width := AdjWidth;
        Bmp.Height := AdjHeight;
        Bmp.Canvas.Brush.Color := vBkColor;
        Bmp.Canvas.FillRect(Bmp.Canvas.ClipRect); //!! ok,可以处理PNG.2015.1.5
        g := TGpGraphics.Create(Bmp.Canvas.Handle);
        try
          //g.Clear(ColorToRGB(vBkColor)); 此方法会让透明PNG的背景不再透明
          g.DrawImage(gpBmp, 0, 0, Bmp.Width, Bmp.Height);
        finally
          g.Free;
        end;
      finally
        Bmp.Canvas.Unlock;
      end;
    finally
      gpBmp.Free;
    end;
  end;

begin
  with TfAdjSize.Create(Application) do try
    ckbScaleAdj.Checked := ScaleAdj;
    lbOrgWidth.Caption  := IntToStr(OrgImgWidth);
    lbOrgHeight.Caption := IntToStr(OrgImgHeight);
    UnhookChangeEvent;
    udImgWidth.Position := OrgImgWidth;
    udImgHeight.Position:= OrgImgHeight;
    HookChangeEvent;
    FRatioOfWH := OrgImgWidth * 1.0 / OrgImgHeight;
    Result := ShowModal = mrOk;
    if Result then begin
      DoAdjSize(udImgWidth.Position, udImgHeight.Position);
      ScaleAdj := ckbScaleAdj.Checked;
    end;
  finally
    Free;
  end;
end;

{ TfAdjSize }

procedure TfAdjSize.FormCreate(Sender: TObject);
begin
  if Assigned(Self.Owner) then Translate(Self);
end;

procedure TfAdjSize.HookChangeEvent;
begin
  udImgHeight.OnChanging := udImgHeightChanging;
  udImgWidth.OnChanging := udImgWidthChanging;
end;

procedure TfAdjSize.UnhookChangeEvent;
begin
  udImgHeight.OnChanging := nil;
  udImgWidth.OnChanging := nil;
end;

procedure TfAdjSize.udImgWidthChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  edImgWidthExit(Sender);
end;

procedure TfAdjSize.udImgHeightChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  edImgHeightExit(Sender);
end;

procedure TfAdjSize.edImgWidthExit(Sender: TObject);
begin
  UnhookChangeEvent;
  try
    if ckbScaleAdj.Checked then begin
      udImgHeight.Position := Round(udImgWidth.Position / FRatioOfWH);
    end;
  finally
    HookChangeEvent;
  end;
end;

procedure TfAdjSize.edImgHeightExit(Sender: TObject);
begin
  UnhookChangeEvent;
  try
    if ckbScaleAdj.Checked then begin
      udImgWidth.Position := Round(udImgHeight.Position * FRatioOfWH);
    end;
  finally
    HookChangeEvent;
  end;
end;

end.
