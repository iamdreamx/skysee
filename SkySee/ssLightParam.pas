unit ssLightParam;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, MyGraphic, SkyTranslator;

type
  TfLightParam = class(TForm)
    lbX: TLabel;
    edX: TEdit;
    udX: TUpDown;
    lbY: TLabel;
    edY: TEdit;
    udY: TUpDown;
    lbM: TLabel;
    edM: TEdit;
    udM: TUpDown;
    lbN: TLabel;
    edN: TEdit;
    udN: TUpDown;
    btOk: TButton;
    btCancel: TButton;
    pnTip: TPanel;
    lbTipTitle: TLabel;
    lbTipContent: TLabel;
    bvlX: TBevel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function RunLight(const Bmp: TBitmap; var X, Y, M, N: Integer): Boolean;

var
  fLightParam: TfLightParam;

implementation

{$R *.dfm}

function RunLight(const Bmp: TBitmap; var X, Y, M, N: Integer): Boolean;
begin
  with TfLightParam.Create(Application) do try
    udX.Position := X;
    udY.Position := Y;
    udM.Position := M;
    udN.Position := N;
    Result := ShowModal = mrOk;
    if Result then begin
      X := udX.Position;
      Y := udY.Position;
      M := udM.Position;
      N := udN.Position;
      Light(Bmp, X, Y, M, N);
    end;
  finally
    Free;
  end;
end;

procedure TfLightParam.FormCreate(Sender: TObject);
begin
  SetWindowLong(edX.Handle, GWL_STYLE, GetWindowLong(edX.Handle, GWL_STYLE) or ES_NUMBER);
  SetWindowLong(edY.Handle, GWL_STYLE, GetWindowLong(edY.Handle, GWL_STYLE) or ES_NUMBER);
  SetWindowLong(edM.Handle, GWL_STYLE, GetWindowLong(edM.Handle, GWL_STYLE) or ES_NUMBER);
  SetWindowLong(edN.Handle, GWL_STYLE, GetWindowLong(edN.Handle, GWL_STYLE) or ES_NUMBER);
  
  if Assigned(Self.Owner) then Translate(Self);
end;

end.
