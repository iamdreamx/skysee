unit ssAdjSaturation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, CommCtrl, MyGraphic, ssDblView, SkyTranslator;

type
  TfAdjSaturation = class(TForm)
    frmAdjView: TfrmDblView;
    gbChgSaturation: TGroupBox;
    lbSaturationTip: TLabel;
    lbSaturationVal: TLabel;
    tkbSaturation: TTrackBar;
    btOk: TButton;
    btCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure tkbSaturationChange(Sender: TObject);
  private
    { Private declarations }
    FBitmap: TBitmap;
  public
    { Public declarations }
    property Bitmap: TBitmap read FBitmap write FBitmap;
  end;

  function AdjustSaturation(const Bmp: TBitmap): Boolean;

var
  fAdjSaturation: TfAdjSaturation;

implementation

{$R *.dfm}

function AdjustSaturation(const Bmp: TBitmap): Boolean;
begin
  with TfAdjSaturation.Create(Application) do try
    Bitmap := Bmp;
    Result := ShowModal = mrOk;
    if Result then begin
      AdjSaturation(Bmp, Bmp, tkbSaturation.Position);
    end;
  finally
    Free;
  end;
end;

procedure TfAdjSaturation.FormShow(Sender: TObject);
begin
  with tkbSaturation do begin //去掉TrackBar那个丑陋的选择区
    SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE) and not TBS_ENABLESELRANGE);
  end;

  frmAdjView.InitBitmap(FBitmap);
  //AdjSaturation(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj, tkbSaturation.Position);
  frmAdjView.InvalidateBmpOfBeforeAdj;
  frmAdjView.InvalidateBmpOfAfterAdj;

  if Assigned(Self.Owner) then Translate(Self);
end;

procedure TfAdjSaturation.tkbSaturationChange(Sender: TObject);
begin
  lbSaturationVal.Caption := IntToStr(tkbSaturation.Position);
  AdjSaturation(frmAdjView.BmpOfBeforeAdj, frmAdjView.BmpOfAfterAdj, tkbSaturation.Position);
  frmAdjView.InvalidateBmpOfAfterAdj;
end;

end.
