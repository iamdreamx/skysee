program SkySeeV;

uses
  SysUtils,
  Forms,
  GDIPAPI in '..\_gdi+\GDIPAPI.pas',
  GDIPOBJ in '..\_gdi+\GDIPOBJ.pas',
  GdipUtil in '..\_gdi+\GdipUtil.pas',
  GdipBitmap in '..\_gdi+\GdipBitmap.pas',
  MyGifPub in '..\_gdi+\MyGifPub.pas',
  MyExifPub in '..\_gdi+\MyExifPub.pas',
  MultiFileProps in '..\_MyLib\MultiFileProps.pas',
  MyGraphic in '..\_MyLib\MyGraphic.pas',
  PicFun in '..\_MyLib\PicFun.pas',
  SelDirEx in '..\_MyLib\SelDirEx.pas',
  ShellFun in '..\_MyLib\ShellFun.pas',
  SkyPub in '..\_MyLib\SkyPub.pas',
  RGBHSV in '..\_MyLib\RGBHSV.pas',
  {$IFDEF MYDEBUG}
  SkyLog in '..\_MyLib\SkyLog.pas',
  {$ENDIF}
  HashTable in '..\_MyLib\HashTable.pas',
  SkyTranslator in '..\_MyLib\SkyTranslator.pas',
  ssAdjRGB in 'ssAdjRGB.pas' {fAdjRGB},
  ssAdjHue in 'ssAdjHue.pas' {fAdjHue},
  ssAdjBrightness in 'ssAdjBrightness.pas' {fAdjBrightness},
  ssAdjSaturation in 'ssAdjSaturation.pas' {fAdjSaturation},
  ssAdjSize in 'ssAdjSize.pas' {fAdjSize},
  ssDblView in 'ssDblView.pas' {frmDblView: TFrame},
  ssLightParam in 'ssLightParam.pas' {fLightParam},
  ssAbout in 'ssAbout.pas' {fAbout},
  ssSetAutoBrowse in 'ssSetAutoBrowse.pas' {fSetAutoBrowse},
  ssMainX in 'ssMainX.pas' {fMain},
  ssEditProp in 'ssEditProp.pas' {fEditProp};

{$R *.res}

function RunGenerator: Boolean;

  function GetOutFile: string;
  begin
    if ParamCount() > 1 then begin
      Result := ParamStr(2);
    end else begin
      Result := ChangeFileExt(ParamStr(0), '.skl');
    end;
  end;

begin
  Result := False;
  if (ParamCount() > 0) and SameText('/G', ParamStr(1)) then begin
    Result := True;
    with TSkyGenerator.Create do try
      Scan(TfMain.Create(nil)).Free;
      Scan(TfAbout.Create(nil)).Free;
      Scan(TfAdjRGB.Create(nil)).Free;
      Scan(TfAdjHue.Create(nil)).Free;
      Scan(TfAdjBrightness.Create(nil)).Free;
      Scan(TfAdjSaturation.Create(nil)).Free;
      Scan(TfAdjSize.Create(nil)).Free;
      Scan(TfLightParam.Create(nil)).Free;
      Scan(TfSetAutoBrowse.Create(nil)).Free;
      Scan(TfEditProp.Create(nil)).Free;
      SaveToFile(GetOutFile);
    finally
      Free;
    end;
  end;
end;

begin
  Application.Initialize;
  if RunGenerator then Exit;

  Application.CreateForm(TfMain, fMain);
  Application.Run;
end.
