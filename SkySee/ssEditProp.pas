unit ssEditProp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, StdCtrls, ValEdit, ActiveX,
  {$IFDEF Virtual}
  SkyThumbViewVirtual
  {$ELSE}
    {$IFDEF DefDraw}
  SkyThumbViewDefDraw
    {$ELSE}
  SkyThumbViewOrigin
    {$ENDIF}
  {$ENDIF},
  GdiPObj, GdipAPI, GdipUtil, SkyTranslator;

type
  TfEditProp = class(TForm)
    lePicProps: TValueListEditor;
    pnBottom: TPanel;
    btOk: TButton;
    btCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FEquipMakeName:   string;
    FEquipModelName:  string;
    FFocalLengthName: string;
    FShootDateName:   string;
    FOrientationName: string;
    procedure InitByFileInfo(FileInfo: PSkyFileInfo);
  public
    { Public declarations }
  end;

function EditJpegProp(const AImgFile: string; FileInfo: PSkyFileInfo): Boolean;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Internal Interface
-----------------------------------------------------------------------------}

{ File Time functions }

function GetFileLastModifyTime(const FileName: string): TFileTime;
var
  hFile: Integer;
begin
  ZeroMemory(@Result, SizeOf(Result));
  hFile := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
  if hFile >= 0 then begin
    GetFileTime(hFile, nil, nil, @Result);
    FileClose(hFile);
  end;
end;

function SetFileLastModifyTime(const FileName: string;
  const LastModify: TFileTime): Boolean;
var
  hFile: Integer;
begin
  hFile := FileOpen(FileName, fmOpenWrite or fmShareDenyWrite);
  if hFile >= 0 then begin
    Result := SetFileTime(hFile, nil, nil, @LastModify);
  FileClose(hFile);
  end else begin
    Result := False;
  end;
end;

{  DateTime Functions }

function MyStrToDateTime(const S: string): TDateTime;
begin          //todo..  improve??
  try
    Result := StrToDateTime(S);
  except
    Result := 0;
  end;
end;

function MyFmtDateTime(const Fmt: string; dt: TDateTime): string;
begin
  if dt > 0 then begin
    Result := FormatDateTime(Fmt, dt);
  end else begin
    Result := '';
  end;
end;

{ Property Item Functions }

procedure SaveStrPropItem(Bmp: TGpBitmap; const Value: string; PId: PROPID);
var
  item: TPropertyItem;
  s: AnsiString;
begin
  s := AnsiString(Value); // for unicode version of delphi
  item.id     := PId;
  item.type_  := PropertyTagTypeASCII;
  item.value  := PAnsiChar(s);
  item.length := Length(s) +1;
  Bmp.SetPropertyItem(item);
end;

procedure SaveShortPropItem(Bmp: TGpBitmap; Value: Short; PId: PROPID);
var
  item: TPropertyItem;
begin
  item.id     := PId;
  item.type_  := PropertyTagTypeShort;
  item.value  := @Value;
  item.length := SizeOf(Value);
  Bmp.SetPropertyItem(item);
end;

procedure SavePropsToJpeg(const AImgFile, Maker, Model, FocalLen: string;
  DTOrigin: TDateTime; Orientation: Short);
var
  ms: TMemoryStream;
  adapt: TStreamAdapter;
  bmp: TGpBitmap;
  encoderClsid: TGUID;
begin
  ms := TMemoryStream.Create;
  try
    ms.LoadFromFile(AImgFile);
    adapt := TStreamAdapter.Create(ms);
    bmp := TGpBitmap.Create(adapt as IStream);
    try
      // save property items
      SaveStrPropItem(bmp, Maker, PropertyTagEquipMake);
      SaveStrPropItem(bmp, Model, PropertyTagEquipModel);
      //FocalLength  :=  todo..
      //yyyy-mm-dd 则windows explorer识别出来的日期不对 2012.2.23
      SaveStrPropItem(bmp, MyFmtDateTime('yyyy:mm:dd hh:nn:ss', DTOrigin), PropertyTagExifDTOrig);
      SaveShortPropItem(Bmp, Orientation, PropertyTagOrientation);
      // save to file
      GetEncoderClsid('image/jpeg', encoderClsid);
      bmp.Save(AImgFile, encoderClsid); //ImageFormatJPEG不行?!
    finally
      bmp.Free;
    end;
  finally
    ms.Free;
  end;
end;

{ Image Functions }

procedure BackupImageFile(const AImgFile: string);
var                     // *.jpg => *.~jpg
  bakFile: string;
begin
  bakFile := AImgFile;
  System.Insert('~', bakFile, LastDelimiter('.', bakFile) +1);
  CopyFile(PChar(AImgFile), PChar(bakFile), True);
end;

{-----------------------------------------------------------------------------
  Public Interface
-----------------------------------------------------------------------------}

function EditJpegProp(const AImgFile: string; FileInfo: PSkyFileInfo): Boolean;
var
  ext: string;
  valMake:  string;
  valModel: string;
  valFocal: string;
  valDTOrg: TDateTime;
  valOrien: Short;
  idxOrn: Integer;
  lastModify: TFileTime;
begin
  Result := False;
  with FileInfo^ do begin
    ext := LowerCase(ExtractFileExt(FileName));
    if (ext <> '.jpg') and (ext <> '.jpeg') and (ext <> '.jpe') and (ext <> '.jfif') then begin
      Application.MessageBox(PChar(Translate('NotJpegFormat',
        'The Picture is not JPEG Format, can''t Edit it''s properties.')),
        PChar(Translate('Information')), MB_OK);
      Exit;
    end;
    with TfEditProp.Create(Application) do try
      InitByFileInfo(FileInfo);
      Result := ShowModal = mrOk;
      if Result then begin
        Application.ProcessMessages;
        lastModify := GetFileLastModifyTime(AImgFile);
        BackupImageFile(AImgFile);
        lePicProps.FindRow(FOrientationName, idxOrn);
        Dec(idxOrn);
        valMake  := Trim(lePicProps.Values[FEquipMakeName]);
        valModel := Trim(lePicProps.Values[FEquipModelName]);
        valFocal := Trim(lePicProps.Values[FFocalLengthName]);
        System.Delete(valFocal, Pos(Translate('mm'){'毫米'}, valFocal), MaxInt);
        valDTOrg := MyStrToDateTime(Trim(lePicProps.Values[FShootDateName]));
        valOrien := lePicProps.ItemProps[idxOrn].PickList.IndexOf(lePicProps.Values[FOrientationName]) + 1;
        SavePropsToJpeg(AImgFile, valMake, valModel, valFocal, valDTOrg, valOrien);
        EquipMake   := valMake;
        EquipModel  := valModel;
        FocalLength := valFocal;
        DTOrigin    := MyFmtDateTime('yyyy-mm-dd hh:nn:ss', valDTOrg);
        Orientation := valOrien;
        HasExifInfo  := (EquipMake <> '') or (EquipModel <> '') or
          (ExposureTime <> '') or (FocalLength <>'') or (DTOrigin <> '');
        SetFileLastModifyTime(AImgFile, lastModify);
      end;
    finally
      Free;
    end;
  end;
end;

{-----------------------------------------------------------------------------
  TfEditProp
-----------------------------------------------------------------------------}

procedure TfEditProp.FormCreate(Sender: TObject);
begin
  if Assigned(Self.Owner) then Translate(Self);
  FEquipMakeName := Translate('EquipmentMaker');
  FEquipModelName := Translate('EquipmentModel');
  FFocalLengthName := Translate('FocalLength');
  FShootDateName   := Translate('ShootDate');
  FOrientationName := Translate('ShootOrientation');
end;

procedure TfEditProp.FormShow(Sender: TObject);
begin
  btCancel.Left := pnBottom.ClientWidth - (btCancel.Width +1);
  btOk.Left     := btCancel.Left - (btOk.Width + 3);
end;

procedure TfEditProp.InitByFileInfo(FileInfo: PSkyFileInfo);
const
  cMillimeters: array[Boolean] of string = ('', 'mm'); //'毫米');
var
  i: Integer;
  idxOrn: Integer;
  orientationTxts: array[0..8] of string;
begin
  orientationTxts[0] := '';
  orientationTxts[1] := Translate('Normal');                  // 正常
  orientationTxts[2] := Translate('Mirror Horizontally');     // 水平镜像
  orientationTxts[3] := Translate('Ratate 180 Degrees');      // 旋转180度
  orientationTxts[4] := Translate('Mirror Vertically');       // 垂直镜像
  orientationTxts[5] := Translate('Mirror Horizontally') + '+' + Translate('Levorotation 270 Degrees'); // 水平镜像+左旋270度
  orientationTxts[6] := Translate('Levorotation 90 Degrees'); // 左旋90度
  orientationTxts[7] := Translate('Mirror Horizontally') + '+' + Translate('Levorotation 90 Degrees');  // 水平镜像+左旋90度
  orientationTxts[8] := Translate('Levorotation 270 Degrees');// 左旋270度

  with FileInfo^ do begin
    lePicProps.InsertRow(FEquipMakeName, EquipMake, True);
    lePicProps.InsertRow(FEquipModelName, EquipModel, True);
    lePicProps.InsertRow(FFocalLengthName, FocalLength + Translate(cMillimeters[FocalLength <> '']), True);
    lePicProps.InsertRow(FShootDateName, DTOrigin, True);
    lePicProps.InsertRow(FOrientationName, orientationTxts[Orientation], True);
    lePicProps.FindRow(FOrientationName, idxOrn);
    Dec(idxOrn);
    lePicProps.ItemProps[idxOrn] := TItemProp.Create(lePicProps);
    for i := 1 to 8 do begin
      lePicProps.ItemProps[idxOrn].PickList.Add(orientationTxts[i]);
    end;
  end;
end;

end.
