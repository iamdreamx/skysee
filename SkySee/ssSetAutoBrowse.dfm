object fSetAutoBrowse: TfSetAutoBrowse
  Left = 205
  Top = 104
  BorderStyle = bsDialog
  Caption = #35774#32622#33258#21160#27983#35272#36873#39033
  ClientHeight = 145
  ClientWidth = 229
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Bevel1: TBevel
    Left = 0
    Top = 94
    Width = 230
    Height = 2
    Shape = bsFrame
  end
  object lbInterval: TLabel
    Left = 16
    Top = 17
    Width = 86
    Height = 12
    Caption = #27983#35272#38388#38548'(&I)'#65306
    FocusControl = sedInterval
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbUnitTip: TLabel
    Left = 132
    Top = 36
    Width = 96
    Height = 12
    Caption = #27627#31186' (0.5-60'#31186') '
  end
  object sedInterval: TSpinEdit
    Left = 16
    Top = 31
    Width = 113
    Height = 21
    Increment = 500
    MaxValue = 60000
    MinValue = 500
    TabOrder = 0
    Value = 2500
  end
  object ckbCycleBrowse: TCheckBox
    Left = 16
    Top = 63
    Width = 113
    Height = 17
    Alignment = taLeftJustify
    Caption = #24490#29615#27983#35272'(&R)'
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object btOk: TButton
    Left = 16
    Top = 108
    Width = 81
    Height = 27
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object btCancel: TButton
    Left = 129
    Top = 108
    Width = 81
    Height = 27
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
