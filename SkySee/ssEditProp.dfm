object fEditProp: TfEditProp
  Left = 205
  Top = 104
  BorderStyle = bsDialog
  BorderWidth = 3
  Caption = #32534#36753#22270#29255#23646#24615
  ClientHeight = 241
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object lePicProps: TValueListEditor
    Left = 0
    Top = 0
    Width = 337
    Height = 209
    Align = alClient
    DropDownRows = 16
    TabOrder = 0
    TitleCaptions.Strings = (
      'Property'
      'Value')
    ColWidths = (
      97
      234)
  end
  object pnBottom: TPanel
    Left = 0
    Top = 209
    Width = 337
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      337
      32)
    object btOk: TButton
      Left = 182
      Top = 4
      Width = 77
      Height = 27
      Anchors = [akTop, akRight]
      Caption = '&OK'
      ModalResult = 1
      TabOrder = 0
    end
    object btCancel: TButton
      Left = 262
      Top = 4
      Width = 75
      Height = 27
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
