unit SkyThumbViewDefDraw;

interface

{$IFNDEF VER130}
  {$WARN UNIT_PLATFORM OFF} //避免出现 FileCtrl is platform... 的警告
  {$WARN SYMBOL_PLATFORM OFF}
{$ENDIF}

{$IFDEF VER150}            //取消D7警告
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$ENDIF}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Forms, ComCtrls, Math,
  FileCtrl, Contnrs, SyncObjs, GdiPObj, GdipAPI, MyExifPub, SkyTranslator;

{-----------------------------------------------------------------------------
  File Info List
-----------------------------------------------------------------------------}

const
  cZipDelimiter = '|';

type
  TSkyThumbView = class;

  PSkyFileInfo = ^TSkyFileInfo;
  TSkyFileInfo = record
    FileName:   string;
    ImageProp:  string;
    ThumbImage: TGpImage;
    ThumbError: Boolean;
    HasExifInfo:  Boolean;
    HasThumbImg:  Boolean;
    EquipMake:    string; //设备制造商
    EquipModel:   string; //设备型号
    ExposureTime: string; //曝光时间 (秒)
    FocalLength:  string; //焦距     (毫米)
    DTOrigin:     string; //拍摄日期
    Orientation:  Integer;//图片拍摄方向(1..8)
  end;

  TSkyFileInfoList = class(TList)
  protected
    function Get(Index: Integer): PSkyFileInfo;
    procedure Put(Index: Integer; Item: PSkyFileInfo);
  public
    function Add(const FileName: string): Integer;
    procedure Clear; override;
    procedure Delete(Index: Integer);
    procedure AlphaSort;
    function IndexOfName(const FileName: string): Integer;
    property Items[Index: Integer]: PSkyFileInfo read Get write Put; default;
  end;

{-----------------------------------------------------------------------------
  Extract Thumbnail Images Thread
-----------------------------------------------------------------------------}

  TSkyGetExifInfoEvent = procedure (HasExifInfo: Boolean; const EquipMake,
    EquipModel, ExposureTime, FocalLength, DTOrigin: string;
    Orientation: Integer) of object;

  TSkyThumbThread = class(TThread)
  private
    FWorking: Boolean;
    FStopped: Boolean;
    FImgWidth:  Integer;
    FImgHeight: Integer;
    FImageDir: string;
    FListView: TSkyThumbView;
    FFileInfos: TSkyFileInfoList;
    FQueue: TQueue;
    FCrtSec: TCriticalSection;
    FCurrItemIndex: Integer;
    FOnGetExifInfo: TSkyGetExifInfoEvent;
    procedure DoGetExifInfo;
    procedure CalcThumbImageRect(vGpBmp: TGpImage; AHasThumb: Boolean; var iWidth, iHeight: Integer);
    procedure ExtractThumbImage(const ImageFile: string; ListItem: TListItem);
    procedure ExtractAllThumbImages;
  protected
    procedure SetThumbSize(iImgWidth, iImgHeight: Integer);
    procedure PushItem(ListItem: TListItem);
    function PopItem: TListItem;
    procedure EmptyQueue;
    procedure Execute; override;
  public
    constructor Create(ListView: TSkyThumbView; FileInfos: TSkyFileInfoList);
    destructor Destroy; override;
    procedure ResumeIt(const ImageDir: string; ListItem: TListItem);
    procedure Stop;
    property OnGetExifInfo: TSkyGetExifInfoEvent read FOnGetExifInfo write FOnGetExifInfo;
  end;

{-----------------------------------------------------------------------------
  Search Directories/Files Thread
-----------------------------------------------------------------------------}

  TFindDirEvent = procedure (const ImageDir: string) of object;
  TFindFileEvent = procedure (const ImageFile: string) of object;

  TSkySearchDirThread = class(TThread)
  private
    FImageDir: string;
    FSearchHiddenDir: Boolean;
    FSearchSubDir: Boolean;
    FStopped:  Boolean;
    FCurrDir:  string;
    FCurrFile: string;
    FOnFindDir:  TFindDirEvent;
    FOnFindFile: TFindFileEvent;
    procedure DoFindDir;
    procedure DoFindFile;
    procedure SearchImageFiles(const sDir, sSubDir: string);
  protected
    procedure Execute; override;
  public
    constructor Create(const ImageDir: string; OnFindDir: TFindDirEvent;
      OnFindFile: TFindFileEvent; SearchHiddenDir, SearchSubDir: Boolean);
    procedure ResumeIt(const ImageDir: string; SearchHiddenDir, SearchSubDir: Boolean);
    procedure Stop;
  end;

{-----------------------------------------------------------------------------
  Thumbnail Images View
-----------------------------------------------------------------------------}

  TFindDirCompleteEvent = procedure (const ImageDir: string; FileCount: Integer) of object;
  TLocateCompleteEvent = procedure (const ImageName: string; ImageIndex: Integer) of object;

  TSkyThumbView = class(TListView)
  private
    FShadeBmp: TBitmap;
    FTextHeight: Integer;

    FThumbColor:  TColor;
    FImageExts:   TStrings;
    FFileInfos:   TSkyFileInfoList;
    FThumbThread: TSkyThumbThread;
    FSearchDirThread: TSkySearchDirThread;

    FImageDir: string;
    FSearchHiddenDir: Boolean;
    FSearchSubDir: Boolean;

    FEquipMakeTip: string;
    FEquipModelTip: string;
    FExposureTimeTip: string;
    FFocalLengthTip: string;
    FDTOriginTip: string;
    FSecondTip: string;
    FMillimeterTip: string;
    FOrientationTxts: array[2..8] of string;

    FOnCustomDrawItem: TLVCustomDrawItemEvent;
    FOnInfoTip: TLVInfoTipEvent;
    FOnSelectItem: TLVSelectItemEvent;

    FOnFindDir: TFindDirEvent;
    FOnFindFile: TFindFileEvent;
    FOnFindDirComplete: TFindDirCompleteEvent;
    FOnLocateComplete: TLocateCompleteEvent;

    procedure SetImageDir(const Value: string);
    procedure SetThumbColor(const Value: TColor);
    procedure SetImageExts(const Value: TStrings);
    procedure MyCustomDrawItem(Sender: TCustomListView; Item: TListItem;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure MySelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure MyInfoTip(Sender: TObject; Item: TListItem; var InfoTip: string);
    procedure FindDir(const ImageDir: string);
    procedure FindFile(const ImageFile: string);
    procedure FindDirComplete(const ImageDir: string; FileCount: Integer);
    procedure LocateTerminate(Sender: TObject);
    function GetGetExifInfo: TSkyGetExifInfoEvent;
    procedure SetGetExifInfo(const Value: TSkyGetExifInfoEvent);
  protected
    function IsImageFile(const sFileName: string): Boolean;
    property ShadeBmp: TBitmap read FShadeBmp;
    property ImageDir: string read FImageDir write SetImageDir;
    property SearchHiddenDir: Boolean read FSearchHiddenDir write FSearchHiddenDir;
    property SearchSubDir: Boolean read FSearchSubDir write FSearchSubDir;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Terminate;   //退出程序时调用，防止线程引起的地址错 2012.2.24
    procedure DeleteSelected; override;
    procedure LocateImage(const sImageName: string; LocateComplete: TLocateCompleteEvent);
    procedure SearchDir(const AImageDir: string; ASearchHiddenDir, ASearchSubDir: Boolean);
    property FileInfos: TSkyFileInfoList read FFileInfos;
  published
    property ThumbColor: TColor read FThumbColor write SetThumbColor default clBtnFace;
    property ImageExts: TStrings read FImageExts write SetImageExts;
    property OnCustomDrawItem: TLVCustomDrawItemEvent read FOnCustomDrawItem write FOnCustomDrawItem;
    property OnInfoTip: TLVInfoTipEvent read FOnInfoTip write FOnInfoTip;
    property OnSelectItem: TLVSelectItemEvent read FOnSelectItem write FOnSelectItem;
    property OnFindDir: TFindDirEvent read FOnFindDir write FOnFindDir;
    property OnFindFile: TFindFileEvent read FOnFindFile write FOnFindFile;
    property OnFindDirComplete: TFindDirCompleteEvent read FOnFindDirComplete write FOnFindDirComplete;
    property OnGetExifInfo: TSkyGetExifInfoEvent read GetGetExifInfo write SetGetExifInfo;
  end;

implementation

type
  TQueueCrack = class(TQueue);

var
  g_NewGDIPlus: Boolean = False;  //用于取缩略图，因Win7 GDI+ 取缩略图与之前不一样，对不是4*3比例的图片会出现黑边 2012.9.11

procedure CheckIfNewGDIPlus;
begin
  g_NewGDIPlus := (Win32MajorVersion >= 6); //Vista? Win7!
end;

{-----------------------------------------------------------------------------
  Internal function
-----------------------------------------------------------------------------}

procedure BuildShadeBitmap(vBmp: TBitmap; vColor: TColor);
var
  i, j: Integer;
  p: PRGBTriple;
  L: Cardinal;
  r, g, b: Byte;
  modVal: Integer;
begin
  L := ColorToRGB(vColor);
  r := GetRValue(L);
  g := GetGValue(L);
  b := GetBValue(L);
  for i := 0 to vBmp.Height - 1 do begin
    p := vBmp.ScanLine[i];
    modVal := i mod 2;
    for j := 0 to vBmp.Width - 1 do begin
      if (modVal <> j mod 2) then begin
        p^.rgbtRed   := r;
        p^.rgbtGreen := g;
        p^.rgbtBlue  := b;
      end else begin
        p^.rgbtRed   := 0;
        p^.rgbtGreen := 0;
        p^.rgbtBlue  := 0;
      end;
      Inc(p);
    end;
  end;
end;

procedure WaitThreadStopped(Thread: TThread);
begin
  while not Thread.Suspended do begin
    Application.ProcessMessages;
    Sleep(1);  //晕，由Sleep(10) 改为 Sleep(1) 在Win7下显示速度就正常了?!之前在Win7切换目录好慢，但XP正常 2012.9.9
  end;
end;

{-----------------------------------------------------------------------------
  TLocateThread
  注：只有用TThread.Synchronize才能保证不出Access Violcation  2012.2.16
-----------------------------------------------------------------------------}

type
  TLocateThread = class(TThread)
  private
    FImageName: string;
    FListView: TSkyThumbView;
    FLastImage: string;
    FLastIndex: Integer;
    procedure DoLocateImage;
  protected
    procedure Execute; override;
  public
    constructor Create(ListView: TSkyThumbView; const ImageName: string);
    property LastImage: string read FLastImage;
    property LastIndex: Integer read FLastIndex;
  end;

constructor TLocateThread.Create(ListView: TSkyThumbView;
  const ImageName: string);
begin
  FListView  := ListView;
  FImageName := ImageName;
  FLastImage := '';
  FLastIndex := -1;
  inherited Create(True);
end;

procedure TLocateThread.DoLocateImage;
begin
  FLastIndex := FListView.FFileInfos.IndexOfName(FImageName);
  if FLastIndex >= 0 then begin
    with FListView.Items[FLastIndex] do begin
      Selected := True;
      Focused := True;
      MakeVisible(False);
    end;
  end;
end;

procedure TLocateThread.Execute;
begin
  Synchronize(DoLocateImage);
end;

{-----------------------------------------------------------------------------
  TSkyFileInfoList
-----------------------------------------------------------------------------}

function TSkyFileInfoList.Add(const FileName: string): Integer;
var
  fi: PSkyFileInfo;
begin
  System.New(fi);
  fi.FileName := FileName;
  fi.ImageProp := '';
  fi.ThumbImage := nil;
  fi.ThumbError := False;
  fi.Orientation := 0;
  Result := inherited Add(fi);
end;

procedure TSkyFileInfoList.Clear;
var
  i: Integer;
  fi: PSkyFileInfo;
begin
  for i := Count -1 downto 0 do begin
    fi := Items[i];
    fi.ThumbImage.Free;
    System.Dispose(fi);
  end;
  inherited;
end;

procedure TSkyFileInfoList.Delete(Index: Integer);
var
  fi: PSkyFileInfo;
begin
  fi := Items[Index];
  fi.ThumbImage.Free;
  System.Dispose(fi);
  inherited Delete(Index);
end;

function FileSortProc(Item1, Item2: Pointer): Integer;
begin
  //以下先按目录排，再按文件名排
  Result   := AnsiCompareText(ExtractFilePath(PSkyFileInfo(Item1).FileName),
                              ExtractFilePath(PSkyFileInfo(Item2).FileName));
  if Result = 0 then
    Result := AnsiCompareText(ExtractFileName(PSkyFileInfo(Item1).FileName),
                              ExtractFileName(PSkyFileInfo(Item2).FileName));
end;

procedure TSkyFileInfoList.AlphaSort;
begin
  Sort(FileSortProc);
end;

function TSkyFileInfoList.IndexOfName(const FileName: string): Integer;
var
  i: Integer;
begin
  for i := 0 to Count -1 do begin
    if SameText(FileName, Items[i].FileName) then begin
      Result := i;
      Exit;
    end;
  end;
  Result := -1;
end;

function TSkyFileInfoList.Get(Index: Integer): PSkyFileInfo;
begin
  Result := inherited Get(Index);
end;

procedure TSkyFileInfoList.Put(Index: Integer; Item: PSkyFileInfo);
begin
  inherited Put(Index, Item);
end;

{-----------------------------------------------------------------------------
  TSkyThumbThread
-----------------------------------------------------------------------------}

constructor TSkyThumbThread.Create(ListView: TSkyThumbView;
  FileInfos: TSkyFileInfoList);
begin
  FListView := ListView;
  FFileInfos := FileInfos;
  FQueue := TQueue.Create;
  FCrtSec := TCriticalSection.Create;
  //2012.2.9 数码相机拍出来的照片里一般有缩略图，其大小默认为160*120，所以
  //缩略图大小应使其为80*60，那样抽取缩略图的速度应该会快些
  FImgWidth := 80;
  FImgHeight := 60;
  inherited Create(True);
end;

destructor TSkyThumbThread.Destroy;
begin
  FQueue.Free;
  FCrtSec.Free;
  inherited;
end;

procedure TSkyThumbThread.DoGetExifInfo;
begin
  with FFileInfos[FCurrItemIndex]^ do begin
    FOnGetExifInfo(HasExifInfo, EquipMake, EquipModel, ExposureTime,
      FocalLength, DTOrigin, Orientation);
  end;
end;

procedure TSkyThumbThread.CalcThumbImageRect(vGpBmp: TGpImage;
  AHasThumb: Boolean; var iWidth, iHeight: Integer);
var
  dHScale, dWScale, dScale: Double;
begin
  if (UInt(iHeight) > vGpBmp.GetHeight) and (UInt(iWidth) > vGpBmp.GetWidth) then begin
    iWidth := vGpBmp.GetWidth;
    iHeight := vGpBmp.GetHeight;
  end else begin
    dHScale := iHeight * 1.0 / vGpBmp.GetHeight;
    dWScale := iWidth * 1.0 / vGpBmp.GetWidth;
    if dHScale > dWScale then begin
      dScale := dWScale;
    end else begin
      dScale := dHScale;
    end;
    if AHasThumb and g_NewGDIPlus then begin
      if vGpBmp.GetWidth > vGpBmp.GetHeight then begin
        iWidth := Round(vGpBmp.GetWidth * dScale);
        iHeight := Round(iWidth * (60/80));
      end else begin
        iHeight := Round(vGpBmp.GetHeight * dScale);
        iWidth := Round(60/80 * iHeight);
      end;
    end else begin
      iWidth := Round(vGpBmp.GetWidth * dScale);
      iHeight := Round(vGpBmp.GetHeight * dScale);
    end;
  end;
end;

procedure TSkyThumbThread.ExtractThumbImage(const ImageFile: string;
  ListItem: TListItem);

  function GetPixelFmtStr(PixelFmt: Integer): string;
  begin
    case PixelFmt of
      PixelFormatUndefined:      Result := '??';
      PixelFormat1bppIndexed:    Result := '1';
      PixelFormat4bppIndexed:    Result := '16';
      PixelFormat8bppIndexed:    Result := '256';
      PixelFormat16bppGrayScale: Result := '512G';
      PixelFormat16bppRGB555:    Result := '32K';
      PixelFormat16bppRGB565:    Result := '64K';
      PixelFormat16bppARGB1555:  Result := '64K';
      PixelFormat24bppRGB:       Result := '16M';
      PixelFormat32bppRGB:       Result := '32B';
      PixelFormat32bppARGB:      Result := '32B';
      PixelFormat32bppPARGB:     Result := '32B';
      PixelFormat48bppRGB:       Result := '48B';
      PixelFormat64bppARGB:      Result := '64B';
      PixelFormat64bppPARGB:     Result := '64B';
    else Result := '';
    end;
  end;

var
  bmp: TGpImage;
  rtIco: TRect;
  imgWidth, imgHeight: Integer;
  imgProp: string;
  ext: string;
begin
  rtIco := ListItem.DisplayRect(drIcon);
  bmp := TGpBitmap.Create(ImageFile);
  try
    imgProp := Format('%dX%d*%s', [bmp.GetWidth, bmp.GetHeight, GetPixelFmtStr(bmp.GetPixelFormat)]);
    ext := LowerCase(ExtractFileExt(ImageFile));
    with FFileInfos[ListItem.Index]^ do begin
      ImageProp := imgProp;
      HasExifInfo  := (ext = '.jpg') or (ext = '.jpeg') or (ext = '.jpe') or (ext = '.jfif');
      HasThumbImg  := False;
      if HasExifInfo then begin
        HasThumbImg  := HasExifItem(bmp, PropertyTagThumbnailData);
        EquipMake    := GetExifItem(bmp, PropertyTagEquipMake);
        EquipModel   := GetExifItem(bmp, PropertyTagEquipModel);
        ExposureTime := ZipFraction(GetExifItem(bmp, PropertyTagExifExposureTime));  //秒
        FocalLength  := RoundFraction(GetExifItem(bmp, PropertyTagExifFocalLength)); //毫米
        DTOrigin     := FmtExifDateTime(GetExifItem(bmp, PropertyTagExifDTOrig));
        Orientation  := StrToIntDef(GetExifItem(bmp, PropertyTagOrientation), 0);    //图片拍摄方向
        HasExifInfo  := (Orientation > 0) or (EquipMake <> '') or (EquipModel <> '') or
          (ExposureTime <> '') or (FocalLength <>'') or (DTOrigin <> '');
      end;
      if Orientation in [5..8] then begin
        imgWidth  := FImgHeight;
        imgHeight := FImgWidth;
      end else begin
        imgWidth  := FImgWidth;
        imgHeight := FImgHeight;
      end;
      CalcThumbImageRect(bmp, HasThumbImg, imgWidth, imgHeight);
      ThumbImage := bmp.GetThumbnailImage(imgWidth, imgHeight);
      if HasExifInfo then begin
        case Orientation of    //图片拍摄方向
          1: ; //"Top, left side (Horizontal / normal)"
          2: ThumbImage.RotateFlip(RotateNoneFlipX);    //"Top, right side (Mirror horizontal)"
          3: ThumbImage.RotateFlip(Rotate180FlipNone) ; //"Bottom, right side (Rotate 180)"
          4: ThumbImage.RotateFlip(Rotate180FlipX);     //"Bottom, left side (Mirror vertical)"
          5: ThumbImage.RotateFlip(Rotate270FlipX);     //"Left side, top (Mirror horizontal and rotate 270 CW)
          6: ThumbImage.RotateFlip(Rotate90FlipNone);   //"Right side, top (Rotate 90 CW)"
          7: ThumbImage.RotateFlip(Rotate90FlipX);      //"Right side, bottom (Mirror horizontal and rotate 90 CW)"
          8: ThumbImage.RotateFlip(Rotate270FlipNone);  //"Left side, bottom (Rotate 270 CW)"
        end;
        if Assigned(FOnGetExifInfo) and ListItem.Focused then begin
          Self.FCurrItemIndex := ListItem.Index;
          Synchronize(DoGetExifInfo);
        end;
      end;
    end;
    ListItem.Data := Pointer(ListItem.Index +1); //!!??
  finally
    bmp.Free;
  end;
end;

procedure TSkyThumbThread.ExtractAllThumbImages;

  procedure ExtractOneThumbImage(Item: TListItem);
  var
    imgFile: string;
    r: TRect;
  begin
    if Item.Data = nil then begin  //??
      imgFile := FImageDir + Item.Caption;
      if FileExists(imgFile) then begin
        try
          ExtractThumbImage(imgFile, Item);
          r := Item.DisplayRect(drIcon);
          InvalidateRect(FListView.Handle, @r, False);
          //Application.ProcessMessages; 不能加，否则不能全部刷新 2010.9.28
        except
          Item.Data := Pointer(-1);
        end;
      end else begin
        Item.Data := Pointer(-1);
      end;
      FFileInfos[Item.Index].ThumbError := Integer(Item.Data) = -1;
    end;
  end;

  function ItemVisible(Item: TListItem): Boolean;
  var
    rtIcon: TRect;
  begin
    rtIcon := Item.DisplayRect(drIcon);
    Result := (rtIcon.Top < FListView.ClientHeight) and (rtIcon.Bottom > 0);
  end;

var
  item: TListItem;
begin
  if FListView.Items.Count = 0 then begin
    FWorking := False;
    Exit;
  end;

  repeat
    item := PopItem;
    try
      if (item <> nil) and ItemVisible(item) then begin
        ExtractOneThumbImage(item);
      end;
    except
      //忽略切换目录时可能的错误，因为不用Synchronize了  2011.3.22
      //其实可以用FListView.Items.IndexOf(item)的,不过那样比较慢,且不能保证对
    end;
  until FStopped or (item = nil);

  FWorking := False;
end;

procedure TSkyThumbThread.PushItem(ListItem: TListItem);
begin
  FCrtSec.Enter;
  try
    //??有更快的方法吗? Queue未排序哦，不能用二分查找 2011.5.19
    if TQueueCrack(FQueue).List.IndexOf(ListItem) = -1 then begin
      FQueue.Push(ListItem);
    end;
  finally
    FCrtSec.Leave;
  end;
end;

function TSkyThumbThread.PopItem: TListItem;
begin
  FCrtSec.Enter;
  try
    if FQueue.Count > 0 then begin
      Result := TListItem(FQueue.Pop);
    end else begin
      Result := nil;
    end;
  finally
    FCrtSec.Leave;
  end;
end;

procedure TSkyThumbThread.EmptyQueue;
begin
  FCrtSec.Enter;
  try
    TQueueCrack(FQueue).List.Clear;
  finally
    FCrtSec.Leave;
  end;
end;

procedure TSkyThumbThread.Execute;
begin
  while not Terminated do begin
    FStopped := False;
    ExtractAllThumbImages;
    //Synchronize(ExtractAllThumbImages);
    if not Terminated then begin
      Self.Suspended := True;
    end;
  end;
end;

procedure TSkyThumbThread.ResumeIt(const ImageDir: string; ListItem: TListItem);
begin
  PushItem(ListItem);
  if FWorking and not Suspended then Exit;

  FWorking := True;
  FImageDir := IncludeTrailingBackslash(ImageDir);
  Self.Suspended := False;
end;

procedure TSkyThumbThread.SetThumbSize(iImgWidth, iImgHeight: Integer);
begin
  FImgWidth := iImgWidth;
  FImgHeight := iImgHeight;
end;

procedure TSkyThumbThread.Stop;
begin
  FStopped := True;
end;

{-----------------------------------------------------------------------------
  TSkySearchDirThread
-----------------------------------------------------------------------------}

constructor TSkySearchDirThread.Create(const ImageDir: string;
  OnFindDir: TFindDirEvent; OnFindFile: TFindFileEvent;
  SearchHiddenDir, SearchSubDir: Boolean);
begin
  FImageDir := ImageDir;
  FOnFindDir  := OnFindDir;
  FOnFindFile := OnFindFile;
  FSearchHiddenDir := SearchHiddenDir;
  FSearchSubDir := SearchSubDir;
  inherited Create(True);
end;

procedure TSkySearchDirThread.DoFindDir;
begin
  if Assigned(FOnFindDir) then begin
    FOnFindDir(FCurrDir);
  end;
end;

procedure TSkySearchDirThread.DoFindFile;
begin
  if Assigned(FOnFindFile) then begin
    FOnFindFile(FCurrFile);
  end;
end;

procedure TSkySearchDirThread.SearchImageFiles(const sDir,
  sSubDir: string);
var
  sr: TSearchRec;
  sRealPath: string;
begin
  if Terminated or FStopped then Exit;
  if Assigned(FOnFindDir) then begin
    FCurrDir := sDir + sSubDir;
    Synchronize(DoFindDir);
  end;
  if Pos({SkyZipPlugIn.}cZipDelimiter, sDir + sSubDir) > 0 then begin
    //todo.. search zip files
  end else begin
    sRealPath := IncludeTrailingBackslash(sDir + sSubDir);
    if FindFirst(sRealPath + '*.*', faAnyFile, sr) = 0 then try
      repeat
        if Terminated or FStopped then Break;
        if (sr.Attr and faDirectory) = faDirectory then begin
          if FSearchSubDir and (sr.Name <> '.') and (sr.Name <> '..') and
             (FSearchHiddenDir or ((sr.Attr and faHidden) <> faHidden)) then begin
            SearchImageFiles(sDir, sSubDir + sr.Name + '\');
          end;
        end else if Assigned(FOnFindFile) then begin
          FCurrFile := sSubDir + sr.Name;
          Synchronize(DoFindFile);
        end;
      until FindNext(sr) <> 0;
    finally
      FindClose(sr);
    end;
  end;
end;

procedure TSkySearchDirThread.Execute;
begin
  while not Terminated do begin
    FStopped := False;
    SearchImageFiles(FImageDir, '');
    if Assigned(FOnFindDir) then begin
      FCurrDir := '';
      Synchronize(DoFindDir);
    end;
    if not Terminated then begin
      Self.Suspended := True;
    end;
  end;
end;

procedure TSkySearchDirThread.ResumeIt(const ImageDir: string;
  SearchHiddenDir, SearchSubDir: Boolean);
begin
  if {FWorking and} not Suspended then Exit;

  //FWorking := True;
  FImageDir := ImageDir;
  FSearchHiddenDir := SearchHiddenDir;
  FSearchSubDir := SearchSubDir;
  Self.Suspended := False;
end;

procedure TSkySearchDirThread.Stop;
begin
  FStopped := True;
end;

{-----------------------------------------------------------------------------
  TSkyThumbView
-----------------------------------------------------------------------------}

constructor TSkyThumbView.Create(AOwner: TComponent);
begin
  inherited;
  FTextHeight := 10;
  FThumbColor := clBtnFace;
  FImageExts := TStringList.Create;
  FImageExts.CommaText := 'bmp,ico,jpg,jpeg,jpe,jfif,wmf,emf,exif,tiff,tif,png,gif';
  TStringList(FImageExts).Sorted := True;
  FFileInfos := TSkyFileInfoList.Create;
  FThumbThread := TSkyThumbThread.Create(Self, FFileInfos);

  //OwnerDraw := True;  高版本Delphi设置了OwnerDraw会导致从底部开始倒排?!
  ReadOnly  := True;
  inherited OnCustomDrawItem := MyCustomDrawItem;
  inherited OnSelectItem := MySelectItem;
  inherited OnInfoTip  := MyInfoTip;
  HideSelection := False;
  IconOptions.AutoArrange := True;
  ShowHint := True;
end;

destructor TSkyThumbView.Destroy;
begin
  FThumbThread.Free;
  FSearchDirThread.Free;
  FFileInfos.Free;
  FImageExts.Free;
  FShadeBmp.Free;
  inherited;
end;

procedure TSkyThumbView.Terminate;
begin
  FThumbThread.Stop;
  WaitThreadStopped(FThumbThread);
  FSearchDirThread.Stop;
  WaitThreadStopped(FSearchDirThread);
end;

procedure TSkyThumbView.DeleteSelected;
var
  idx: Integer;
begin
  if Assigned(Selected) then begin
    idx := Selected.Index;
    //inherited DeleteSelected;
    Self.Delete(Selected); //only delete one item
    FFileInfos.Delete(idx);
    if (idx >= Items.Count) and (idx > 0) then Dec(idx);
    if (idx < Items.Count) then begin
      with Items[idx] do begin
        Selected := True;  //will launch select item event
        //Focused := True;
      end;
    end;
  end;
end;

procedure TSkyThumbView.LocateImage(const sImageName: string;
  LocateComplete: TLocateCompleteEvent);
begin
  FOnLocateComplete := LocateComplete;
  with TLocateThread.Create(Self, sImageName) do begin
    FreeOnTerminate := True;
    OnTerminate := LocateTerminate;
    {$IFDEF UNICODE}Start{$ELSE}Resume{$ENDIF};
  end;
  {
  Result := FFileInfos.IndexOfName(sImageName);
  if Result >= 0 then begin
    with Items[Result] do begin
      Selected := True;
      Focused := True;
      MakeVisible(False);
    end;
  end;}
end;

procedure TSkyThumbView.SearchDir(const AImageDir: string;
  ASearchHiddenDir, ASearchSubDir: Boolean);
begin
  FSearchHiddenDir := ASearchHiddenDir;
  FSearchSubDir := ASearchSubDir;
  Self.ImageDir := AImageDir;
end;

procedure TSkyThumbView.SetImageDir(const Value: string);
var
  i: Integer;
begin
  FTextHeight := Canvas.TextHeight('AWX') -1;
  FImageDir := IncludeTrailingBackslash(Value);

  FThumbThread.Stop;
  WaitThreadStopped(FThumbThread);
  FThumbThread.EmptyQueue;
  if Assigned(LargeImages) then begin
    FThumbThread.SetThumbSize(LargeImages.Width + 13, LargeImages.Height - (FTextHeight +1));
  end;

  if FSearchDirThread = nil then begin
    FSearchDirThread := TSkySearchDirThread.Create(FImageDir, FindDir, FindFile, FSearchHiddenDir, FSearchSubDir);
  end else begin
    FSearchDirThread.Stop;
    WaitThreadStopped(FSearchDirThread);
  end;

  Items.BeginUpdate;
  try
    ItemFocused := nil;
    Selected := nil;
    Items.Clear;
    FFileInfos.Clear;
    FSearchDirThread.ResumeIt(FImageDir, FSearchHiddenDir, FSearchSubDir);
    WaitThreadStopped(FSearchDirThread);

    Items.Clear; //!!当一个目录未扫描完即切换到另一个目录时起作用，没有的话可能会出现文件重复(只DefaultDraw) 2012.2.6
    FindDirComplete(Value, FFileInfos.Count);
    if FFileInfos.Count > 0 then begin
      FFileInfos.AlphaSort;
      for i := 0 to FFileInfos.Count -1 do begin
        with Items.Add do begin
          Caption := FFileInfos[i].FileName;
          ImageIndex := -1;
        end;
      end;
      Items[0].MakeVisible(False);
    end;
    //Invalidate;
  finally
    Items.EndUpdate;
  end;
end;

procedure TSkyThumbView.SetThumbColor(const Value: TColor);
begin
  FThumbColor := Value;
  Invalidate;
end;

procedure TSkyThumbView.SetImageExts(const Value: TStrings);
begin
  FImageExts.Assign(Value);
end;

procedure TSkyThumbView.MyCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);

  procedure AdjWidth(var r: TRect);
  var
    w: Integer;
    gap: Integer;
  begin
    if Assigned(Self.LargeImages) then begin
      w := Max(LargeImages.Width, LargeImages.Height) + 4;
      if r.Right - r.Left > w then begin
        gap := (r.Right - r.Left - w) div 2;
        Inc(r.Left, gap);
        Dec(r.Right, gap);
      end;
    end;
  end;

  procedure DrawFrame;
  var
    rtIco: TRect;
  begin
    with Sender.Canvas do begin
      rtIco := Item.DisplayRect(drIcon);
      AdjWidth(rtIco);
      Brush.Style := bsSolid;
      Brush.Color := FThumbColor;
      FillRect(rtIco);
      Pen.Color := clBtnHighlight;
      MoveTo(rtIco.Left, rtIco.Bottom);
      LineTo(rtIco.Left, rtIco.Top);
      LineTo(rtIco.Right, rtIco.Top);
      Pen.Color := clBlack;
      LineTo(rtIco.Right, rtIco.Bottom);
      LineTo(rtIco.Left, rtIco.Bottom);
    end;
  end;

  procedure DrawShade(r: TRect);
  begin
    AdjWidth(r);
    if FShadeBmp = nil then begin
      FShadeBmp := TBitmap.Create;
      FShadeBmp.PixelFormat := pf24bit;
      FShadeBmp.Height := r.Bottom - r.Top;
      FShadeBmp.Width  := r.Right - r.Left;
      BuildShadeBitmap(FShadeBmp, clNavy);
      FShadeBmp.TransparentColor := RGB(0, 0, 0);
      FShadeBmp.Transparent := True;
    end else if FShadeBmp.Height > r.Bottom - r.Top then begin
      FShadeBmp.Height := r.Bottom - r.Top; //!!
    end;
    Sender.Canvas.Draw(r.Left, r.Top, FShadeBmp);
  end;

  procedure DrawThumbImage;
  var
    ThumbImage: TGpImage;
    g: TGpGraphics;
    rtIco, rtThumb, rtImgProp: TRect;
    index: Integer;
    shadeH: Integer;
    imgProp: string;
  begin
    rtIco := item.DisplayRect(drIcon);
    shadeH := rtIco.Bottom - rtIco.Top;
    if (Item.Data = nil) or (Integer(Item.Data) = -1) then begin
      //...
      if Assigned(LargeImages) then begin
        LargeImages.Draw(Sender.Canvas,
          rtIco.Left + (rtIco.Right - rtIco.Left - LargeImages.Width) div 2,
          rtIco.Top + (rtIco.Bottom - rtIco.Top - LargeImages.Height) div 2,
          0);
        shadeH := LargeImages.Height;
      end;
      FThumbThread.ResumeIt(FImageDir, Item);
    end else begin
      // 画缩略图
      index := Item.Index; //.. = Integer(Item.Data) -1;
      shadeH  := rtIco.Bottom - (rtIco.Top + FTextHeight +1);
      ThumbImage := FFileInfos[index].ThumbImage;
      rtThumb := Rect(rtIco.Left + (rtIco.Right - rtIco.Left - Integer(ThumbImage.GetWidth) +1) div 2,
        rtIco.Top + (rtIco.Bottom - rtIco.Top - Integer(ThumbImage.GetHeight) - FTextHeight) div 2,
        ThumbImage.GetWidth,
        ThumbImage.GetHeight);
      g := TGpGraphics.Create(Sender.Canvas.Handle);
      try
        g.DrawImage(ThumbImage, rtThumb.Left, rtThumb.Top, rtThumb.Right, rtThumb.Bottom);
      finally
        g.Free;
      end;
      // 画缩略图属性信息
      imgProp := FFileInfos[index].ImageProp;
      rtImgProp := Rect(rtIco.Left +1, rtIco.Top + Integer(ThumbImage.GetHeight) -1, rtIco.Right -1, rtIco.Bottom -1);
      Sender.Canvas.Brush.Style := bsClear;
      DrawText(Sender.Canvas.Handle, PChar(imgProp), Length(imgProp),
        rtImgProp, DT_CENTER or DT_BOTTOM or DT_SINGLELINE);
    end;
    if (cdsFocused in State) or Item.Selected then begin
      DrawShade(Rect(rtIco.Left +1, rtIco.Top +1, rtIco.Right, rtIco.Top + shadeH));
    end;
  end;

  procedure DrawImageName;
  const
    cFontColors: array[Boolean] of TColor = (clWindowText, clHighlightText);
    cBackColors: array[Boolean] of TColor = (clWindow, clHighlight);
  var
    rtTxt: TRect;
    itemFocused: Boolean;
  begin
    with Sender.Canvas do begin
      itemFocused := (cdsFocused in State) or Item.Selected;
      rtTxt := Item.DisplayRect(drLabel);
      Brush.Color := cBackColors[itemFocused];
      FillRect(rtTxt);
      if itemFocused and Sender.Focused then begin
        DrawFocusRect(rtTxt);
      end;
      SetBkMode(Handle, TRANSPARENT); //设定文字为透明(Brush.Style设为bsClear没用)
      SetTextColor(Handle, ColorToRGB(cFontColors[itemFocused])); //设Font.Color没用
      DrawText(Handle, PChar(Item.Caption), Length(Item.Caption), rtTxt,
        DT_CENTER or DT_TOP or DT_END_ELLIPSIS or DT_WORDBREAK or DT_EDITCONTROL);
    end;
  end;

begin
  Canvas.Lock;
  try
    if Item.Index >= FFileInfos.Count then Exit;
    if Assigned(FOnCustomDrawItem) then begin
      FOnCustomDrawItem(Sender, Item, State, DefaultDraw);
    end;
    if DefaultDraw then try
      with Item.DisplayRect(drIcon) do begin
        if (Top < Sender.ClientHeight) and (Bottom > 0) then begin
          DrawFrame;
          DrawThumbImage; // draw thumbnail image
          DrawImageName;
          DefaultDraw := False;
        end;
      end;
    except
    end;
  finally
    Canvas.Unlock;
  end;
end;

procedure TSkyThumbView.MySelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  if Assigned(FOnSelectItem) then begin
    FOnSelectItem(Sender, Item, Selected);
  end;
  if Selected and Assigned(Item) and Assigned(OnGetExifInfo) then begin
    with FFileInfos[Item.Index]^ do begin
      FThumbThread.OnGetExifInfo(HasExifInfo, EquipMake, EquipModel,
        ExposureTime, FocalLength, DTOrigin, Orientation);
    end;
  end;
end;

procedure TSkyThumbView.MyInfoTip(Sender: TObject; Item: TListItem;
  var InfoTip: string);

  procedure InitTranslateMsgs;
  begin
    if FEquipMakeTip = '' then begin
      FEquipMakeTip    := Translate('EquipMakeShort', 'Maker') + ':';       //'厂商:'
      FEquipModelTip   := Translate('EquipModelShort', 'Model') + ':';      //'型号:'
      FExposureTimeTip := Translate('ExposureTimeShort', 'Exposure') + ':'; //'曝光:'
      FFocalLengthTip  := Translate('FocalLength', 'Focal Length') + ':';   //'焦距:'
      FDTOriginTip     := Translate('Shoot') + ':'; //'拍摄:'
      FSecondTip       := Translate('s');
      FMillimeterTip   := Translate('mm');
      FOrientationTxts[2] := Translate('Mirror Horizontally');     // 水平镜像
      FOrientationTxts[3] := Translate('Ratate 180 Degrees');      // 旋转180度
      FOrientationTxts[4] := Translate('Mirror Vertically');       // 垂直镜像
      FOrientationTxts[5] := Translate('Mirror Horizontally') + '+' + Translate('Levorotation 270 Degrees'); // 水平镜像+左旋270度
      FOrientationTxts[6] := Translate('Levorotation 90 Degrees'); // 左旋90度
      FOrientationTxts[7] := Translate('Mirror Horizontally') + '+' + Translate('Levorotation 90 Degrees');  // 水平镜像+左旋90度
      FOrientationTxts[8] := Translate('Levorotation 270 Degrees');// 左旋270度
    end;
  end;
  function GetExifInfoString: string;
  begin
    InitTranslateMsgs;
    Result := '';
    with FFileInfos[Item.Index]^ do begin
      if not HasExifInfo then Exit;
      if EquipMake <> '' then begin
        Result := Result + sLineBreak + FEquipMakeTip + Trim(EquipMake);
      end;
      if EquipModel <> '' then begin
        Result := Result + sLineBreak + FEquipModelTip + Trim(EquipModel);
      end;
      if ExposureTime <> '' then begin
        Result := Result + sLineBreak + FExposureTimeTip + ExposureTime + FSecondTip;
      end;
      if FocalLength <> '' then begin
        Result := Result + sLineBreak + FFocalLengthTip + FocalLength + FMillimeterTip;
      end;
      if DTOrigin <> '' then begin
        Result := Result + sLineBreak + FDTOriginTip + DTOrigin;
      end;
      if (Orientation > 1) and (Orientation <= 8) then begin
        Result := Result + sLineBreak + FOrientationTxts[Orientation];
      end;
    end;
    System.Delete(Result, 1, Length(sLineBreak));
  end;

var
  sTip: string;
begin
  if Assigned(Item) and Assigned(FFileInfos[Item.Index].ThumbImage) then begin
    sTip := GetExifInfoString;
    if sTip <> '' then begin
      InfoTip := InfoTip + sLineBreak + sTip;
    end else begin
      InfoTip := '';
    end;
  end else begin
    InfoTip := '';
  end;
  if Assigned(FOnInfoTip) then begin
    FOnInfoTip(Sender, Item, InfoTip);
  end;
end;

procedure TSkyThumbView.FindDir(const ImageDir: string);
begin
  if Assigned(FOnFindDir) then begin
    FOnFindDir(ImageDir);
  end;
end;

procedure TSkyThumbView.FindFile(const ImageFile: string);
begin
  if IsImageFile(ImageFile) then begin
    FFileInfos.Add(ImageFile);
  end;
  if Assigned(FOnFindFile) then begin
    FOnFindFile(ImageFile);
  end;
end;

procedure TSkyThumbView.FindDirComplete(const ImageDir: string;
  FileCount: Integer);
begin
  if Assigned(FOnFindDirComplete) then begin
    FOnFindDirComplete(ImageDir, FileCount);
  end;
end;

procedure TSkyThumbView.LocateTerminate(Sender: TObject);
begin
  with Sender as TLocateThread do begin
    if Assigned(FOnLocateComplete) and (LastImage <> '') then begin
      FOnLocateComplete(LastImage, LastIndex);
    end;
  end;
end;

function TSkyThumbView.GetGetExifInfo: TSkyGetExifInfoEvent;
begin
  Result := FThumbThread.OnGetExifInfo;
end;

procedure TSkyThumbView.SetGetExifInfo(const Value: TSkyGetExifInfoEvent);
begin
  FThumbThread.OnGetExifInfo := Value;
end;

function TSkyThumbView.IsImageFile(const sFileName: string): Boolean;

  function MyExtractFileExt(const FileName: string): string;
  var
    I: Integer;
  begin
    I := LastDelimiter('.' + PathDelim + DriveDelim, FileName);
    if (I > 0) and (FileName[I] = '.') then
      Result := Copy(FileName, I +1, MaxInt)
    else
      Result := '';
  end;

begin
  Result := FImageExts.IndexOf(MyExtractFileExt(sFileName)) >= 0;
end;

initialization
  CheckIfNewGDIPlus;

end.
