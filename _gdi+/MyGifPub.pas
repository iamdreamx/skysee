unit MyGifPub;

interface

uses
  Classes, SysUtils, Windows, Graphics, GdiPObj, GdipAPI;

procedure GetGifProps(GifBmp: TGpBitmap; var GifFrams, FrameTimeLen,
  ItemLen: Integer; var PropertyItem: Pointer);

procedure GetGifFrameTimes(PropertyItem: Pointer; var FrameTimes: array of Cardinal);

procedure SelectActiveFrame(GifBmp: TGpBitmap; FrameIndex: Integer);

implementation

procedure GetGifProps(GifBmp: TGpBitmap; var GifFrams, FrameTimeLen,
  ItemLen: Integer; var PropertyItem: Pointer);
var
  propSize: Integer;
begin
  GifFrams := GifBmp.GetFrameCount(FrameDimensionTime);
  propSize := GifBmp.GetPropertyItemSize(PropertyTagFrameDelay);
  ItemLen  := propSize * SizeOf(TPropertyItem);
  GetMem(PropertyItem, ItemLen);
  try
    GifBmp.GetPropertyItem(PropertyTagFrameDelay, propSize, PPropertyItem(PropertyItem));
    FrameTimeLen := PPropertyItem(PropertyItem)^.Length;
  except
    FreeMem(PropertyItem);
    PropertyItem := nil;
  end;
end;

procedure GetGifFrameTimes(PropertyItem: Pointer; var FrameTimes: array of Cardinal);
begin
  with PPropertyItem(PropertyItem)^ do begin
    CopyMemory(@FrameTimes[0], Value, Length);
  end;
end;

procedure SelectActiveFrame(GifBmp: TGpBitmap; FrameIndex: Integer);
begin
  GifBmp.SelectActiveFrame(FrameDimensionTime, FrameIndex);
end;

end.
