unit MyExifPub;

interface

uses
  Classes, SysUtils, Windows, Graphics, ActiveX, GdiPObj, GdipAPI;

function GetExifItem(GPImage: TGPImage; ItemID: PROPID): string;
function HasExifItem(GPImage: TGPImage; ItemID: PROPID): Boolean;

function RoundFraction(const S: string): string;
function ZipFraction(const S: string): string;
function FmtExifDateTime(const S: string): string;

implementation

{ Get Exif Item Info of JPEG }

function GetExifItem(GPImage: TGPImage; ItemID: PROPID): string;
var
  PropSize: UINT;
  PropItem: PPropertyItem;
  S: PByteArray;
  d1, d2: Cardinal;
begin
  Result := '';
  try
    PropSize := GPImage.GetPropertyItemSize(ItemID);
    if PropSize=0 then Exit;

    GetMem(PropItem, PropSize);
    try
      GPImage.GetPropertyItem(ItemID, PropSize, PropItem);
      case PropItem.type_ of
        2 {PropertyTagTypeASCII}: begin  //字符
          Result := string(PAnsiChar(PropItem.Value));
        end;
        5 {PropertyTagTypeRational},     //分数
        10 {PropertyTagTypeSRational}: begin
          S  := PByteArray(PropItem.Value);
          d1 := (S[3] shl 24) or (S[2] shl 16) or (S[1] shl 8) or S[0];
          d2 := (S[7] shl 24) or (S[6] shl 16) or (S[5] shl 8) or S[4];
          Result := Format('%d/%d', [d1, d2]);
        end;
        3 {PropertyTagTypeShort}: begin  //16位数字
          S  := PByteArray(PropItem.Value);
          Result := Format('%u', [(S[1] shl 8) or S[0]]);
        end;
        4 {PropertyTagTypeLong},         //32位数字
        9 {PropertyTagTypeSLONG}: begin
          S  := PByteArray(PropItem.value);
          Result := IntToStr((S[3] shl 24) or (S[2] shl 16) or (S[1] shl 8) or S[0]);
        end;
      end;
    except
    end;
    FreeMem(PropItem);
  except
  end;
end;

{ check if has an exif item }

function HasExifItem(GPImage: TGPImage; ItemID: PROPID): Boolean;
begin
  Result := False;
  try
    Result := (GPImage.GetPropertyItemSize(ItemID) > 0);
  except
  end;
end;

{ asistant exif functions }

function RoundFraction(const S: string): string;
var
  idx: Integer;
begin
  idx := Pos('/', S);
  if idx > 0 then begin
    Result := IntToStr(Round(StrToInt(Copy(S, 1, idx -1))* 1.0 / StrToInt(Copy(S, idx +1, MaxInt))));
  end else begin
    Result := S;
  end;
end;

(* 2013.8.21
   改成类似Windows图片属性中曝光时间格式，即取近似分数，尽可能使分子为1
*)
function ZipFraction(const S: string): string;
var
  idx: Integer;
  m, n: Integer;
  intPart: Integer;
  numerator, denominator: Integer;
begin
  idx := Pos('/', S);
  if idx > 0 then begin
    m := idx -1;
    n := Length(s);
    while (m > 0) and (n > idx) and (S[m] = '0') and (S[n] = '0') do begin
      Dec(m);
      Dec(n);
    end;
    numerator := StrToIntDef(Copy(S, 1, m), 0);
    denominator := StrToIntDef(Copy(S, idx +1, n - idx), 0);
    if numerator = denominator then begin
      Result := '1';
    end else begin
      if numerator > denominator then begin
        intPart := numerator div denominator;
        numerator := numerator mod denominator;
      end else begin
        intPart := 0;
      end;
      if (numerator > 1) and (denominator >= 10) then begin
        denominator := Round(denominator * 1.0 / numerator);
        numerator := 1;
      end;
      if (numerator > 1) and (denominator > 1) then begin
        while ((numerator mod 2) = 0) and ((denominator mod 2) = 0) do begin
          numerator := numerator div 2;
          denominator := denominator div 2;
        end;
      end;
      Result := Format('%d/%d', [numerator, denominator]);
      if intPart > 0 then begin
        Result := Format('%d %s', [intPart, Result]);
      end;
    end;
  end else begin
    Result := S;
  end;
end;

function FmtExifDateTime(const S: string): string;
var              //"S" Format: yyyy?mm?dd hh:nn:ss => yyyy-mm-dd hh:nn:ss
  i: Integer;
begin
  Result := S;
  i := 1;
  while (i < Length(S)) and (S[i] <> ' ') do begin
    if not{$IFDEF UNICODE} CharInSet(S[i],{$ELSE}(S[i] in{$ENDIF} ['0'..'9', '-']) then begin
      Result[i] := '-';
    end;
    Inc(i);
  end;
end;

end.
