# SkySee

#### 介绍
SkySee长风图片浏览器是一个以缩略图+预览图方式查看大量图片的小工具

#### 软件架构
    没有用到特别的软件架构，只是正常Delphi程序的写法。代码中使用到的一些库来自于网络，版权说明见各个文件。
    本软件代码简单，小软件嘛，简单才是美。
    SkySee.dpr/SkySee.bdsproj/SkySee.dproj是最初建立的工程，代码未分离清楚
    SkySeeV.dpr/SkySeeV.bdsproj/SkySeeV.dproj是采用Virtual ListView+Thread方式搜索目录实现的，速度最快；
    SkySeeD.dpr/SkySeeD.bdsproj/SkySeeD.dproj是采用ListView默认绘制+Thread方式搜索目录实现的，速度次之，但切换目录的响应速度未有改善；
    SkySeeO.dpr/SkySeeO.bdsproj/SkySeeO.dproj是采用ListView默认绘制+事件触发方式搜索目录实现的，速度最慢（可将它与SkySee.dpr/bdsproj/dproj比较，查看改进痕迹）。
    注意：如果你在D2007/D2010的高版本Delphi环境中打开dpr工程文件，则可能无法直接编译，这是因为高版本Delphi忽略了cfg/dof中的一些选项。你需要在工程的Options中找到Unit output directory/Search path/Conditional defines这些选项并重新设置。

#### 安装教程
    所有工程支持Delph 7~Delphi RAD 10.x的各个版本。
1.  在Delphi中打开各个工程或工程组，忽略掉控件未安装的提示，可以直接编译；
2.  较高版本的Delphi建议Build All，这样可以重新生成res文件，可以在Win10等系统中支持较大的缩略图；
3.  方便起见，可以将SkyDirFileTree目录下的所有pas文件加到已有或新建的控件包，编译安装，然后再打开工程或工程组，就不会有警告消息了；

#### 使用说明

1.  打开编译好的程序，在左侧的树状列表选择一个目录，就可以开始浏览了。
2.  一些图片处理效果同样来自于网络，仅供测试用。
3.  可以在文件菜单下选择自动浏览。

#### 参与贡献

1.  顾中军


#### 实现历程

     图片浏览器可以说已经“泛滥成灾”了，网上到处都是，那么我为什么还要自己写一个呢？因为专业一些的图片浏览器如ACDSee之类对我来说功能太多，实际上绝大多数时间我只是想顺序浏览一下数码照片而已；而很多开放源代码的浏览器似乎又不太符合我的看图习惯，呵呵，正好想学一下GDI+的使用，于是就自己动手写了一个。
     以下粗略的实现与改进过程。
     2010.9  初步实现缩略图+预览方式查看功能
     2011.2~2011.5 改进缩略图显示方法以提高响应速度，修正一些错误
     2012.2~2012.4 显示缩略图的ListView部分独立出来，并改用Virtual方式，提高显示速度；搜索目录及文件也改用Thread，不但加快软件响应速度，也可以随时快速切换目录（即使目录未搜索完）
     2012.12 改进代码，以便可以在Unicode版本的Delphi下编译，测试了D2010，编译运行正常，加上简单的多语言支持
     2013.1~2019.5 一些小的改进，包括PNG/GIF图片显示的改进，以及用高版本Delphi编译时缩略图大小不对的改进
     2021.3.9~2021.4.3 改进代码以支持高清屏，并在高清屏下显示大一些的缩略图 (Delphi 10.x+), 增加对JFIF/JPE文件的支持
     2021.9.23~2021.12.20 修正还原时窗口太小的问题，让窗口在多屏下显示到当前屏幕的中间而不是虚拟大屏的中间，让“搜索隐藏目录”选项也用于图片文件的搜索
     2022.7.27 改进关于窗口以更好地支持高清屏：根据当前屏幕按比例放大显示 (Delphi 10.x+)