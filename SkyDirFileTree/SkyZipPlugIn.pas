unit SkyZipPlugIn;

interface

// ========================== Determine compiler ========================== //
{$IFDEF VER80} Sorry, Delphi 1 is not supported !  {$ENDIF} // D1
{$IFDEF VER90} Sorry, Delphi 2 is not supported !  {$ENDIF} // D2
{$IFDEF VER93} Sorry, BCB 1 is not supported !     {$ENDIF} // CPPB 1
{$IFDEF VER100} Sorry, Delphi 3 is not supported ! {$ENDIF} // D3
{$IFDEF VER110} Sorry, BCB 3 is not supported !    {$ENDIF} // CPPB 3
{$IFDEF VER120} Sorry, Delphi 4 is not supported ! {$ENDIF} // D4
{$IFDEF VER125} Sorry, BCB 4 is not supported !    {$ENDIF} // CPPB 4
{$IFDEF VER130}        //D5
  {$DEFINE DELPHI5UP}
{$ENDIF}
{$IFDEF VER135}        //BCB5
  {$DEFINE DELPHI5UP}
{$ENDIF}
{$IFDEF VER140}        //D6
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
{$ENDIF}
{$IFDEF VER145}        //BCB6
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
{$ENDIF}
{$IFDEF VER150}        //D7
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
{$ENDIF}
{$IFDEF VER170}        //D9(D2005)
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
{$ENDIF}
{$IFDEF VER180}        //D10(D2006)|BCB10
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
{$ENDIF}
{$IFDEF VER185}        //D11(D2007)|BCB11
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
{$ENDIF}
{$IFDEF VER200}        //D12(D2009)|BCB12
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
{$ENDIF}
{$IFDEF VER210}        //D14(D2010)|BCB14
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
{$ENDIF}
{$IFDEF VER220}        //DelphiXE
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
{$ENDIF}
{$IFDEF VER230}        //DelphiXE2
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
  {$DEFINE DELPHIXE2UP}
{$ENDIF}
{$IFDEF VER240}        //DelphiXE3
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
  {$DEFINE DELPHIXE2UP}
  {$DEFINE DELPHIXE3UP}
{$ENDIF}
{$IFDEF VER250}        //DelphiXE4
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
  {$DEFINE DELPHIXE2UP}
  {$DEFINE DELPHIXE3UP}
  {$DEFINE DELPHIXE4UP}
{$ENDIF}
{$IFDEF UNICODE}
  {$IF CompilerVersion > 25}        //DelphiXE4 later
    {$DEFINE DELPHI5UP}
    {$DEFINE DELPHI6UP}
    {$DEFINE DELPHI7UP}
    {$DEFINE D2005UP}
    {$DEFINE D2006UP}
    {$DEFINE D2007UP}
    {$DEFINE D2009UP}
    {$DEFINE D2010UP}
    {$DEFINE DELPHIXEUP}
    {$DEFINE DELPHIXE2UP}
    {$DEFINE DELPHIXE3UP}
    {$DEFINE DELPHIXE4UP}
  {$IFEND}
{$ENDIF}

{$IFDEF DELPHI6UP}
  {$WARN SYMBOL_PLATFORM OFF}
{$ENDIF}
{$IFDEF DELPHI7UP}            //取消D7警告
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
  {$WARN SYMBOL_PLATFORM OFF}
{$ENDIF}
// ========================== Determine compiler ========================== //

uses
  Windows, Messages, SysUtils, Classes, Forms{$IFDEF VER130}, FileCtrl{$ENDIF};

const
  cpGetZipDef = 'SkyGetZipDef';
  cpUnZipFile = 'SkyUnZipFile';
  cpEnumZipFiles = 'SkyEnumZipFiles';
  cpGetAllZipFiles = 'SkyGetAllZipFiles';
  cZipDelimiter = '|';

type
  TGetZipDefProc = function (sDef: PChar; var iLen: Integer): Boolean; stdcall;
  TUnZipFileProc = function (sZipFile, sSubFile, sZipDir: PChar): Boolean; stdcall;
  TZipFileCallBack = procedure (Node: Pointer; sZipDir, sSub: PChar;
    bFindFirst, bIsDir: Boolean; var bHandled: Boolean); stdcall;
  TEnumZipFilesProc = function (Node: Pointer; sZipFile, sZipDir: PChar;
    cbZipFile: TZipFileCallBack; bFindFirst: Boolean): Integer; stdcall;

  TMatchFileProc = function (Sender: Pointer; sFileName: PChar;
    var bStop: Boolean): Boolean; stdcall;
  TProcessFileProc = function (Sender: Pointer; sZipFile, sZipName,
    sOutFile: PChar; iFileSize: Integer; bError: Boolean): Integer; stdcall;
  TGetAllZipFilesProc = function (Sender: Pointer; sZipFile, sZipDir: PChar;
    cbMatchFile: TMatchFileProc; cbProcessFile: TProcessFileProc;
    var bError: Boolean): Integer; stdcall;

  PUnZipInfo = ^TUnZipInfo;
  TUnZipInfo = record
    ZipPath:    string;
    RealPath:   string;
    CreateTime: TFileTime;
  end;

  TUnZipInfos = class(TList)
  private
    FAutoDel:  Boolean;
    FUnZipDir: string;
    function Get(Index: Integer): PUnZipInfo;
    procedure Put(Index: Integer; Item: PUnZipInfo);
  protected
    procedure SimpleDelete(Index: Integer);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Clear; override;
    function AddFirst(const sZipPath, sRealPath: string): Integer;
    procedure Delete(Index: Integer);
    procedure DeleteUnZip(const sZipPath: string);
    function IndexOfZipPath(const sZipPath: string): Integer;
    property AutoDelete: Boolean read FAutoDel write FAutoDel default True;
    property Items[Index: Integer]: PUnZipInfo read Get write Put; default;
    property UnZipDir: string read FUnZipDir write FUnZipDir;
  end;

  PZipper = ^TZipper;
  TZipper = record
    DllName: string;
    DllHandle: THandle;
    UnZipFile: TUnZipFileProc;
    EnumZipFiles: TEnumZipFilesProc;
    GetAllZipFiles: TGetAllZipFilesProc;
  end;

  TZippers = class(TStringList)
  private
    FOwner: TComponent;
    FStop: Boolean;
    FPath: string;
    FUnZipDir: string;
    FUnZips: TUnZipInfos;
    FDllZippers: TList;
    procedure SetPath(const Value: string);
    procedure SetStop(Value: Boolean);
    function GetZipperName(Index: Integer): string;
    function GetZipperDef(Index: Integer): string;
    function GetZipper(Index: Integer): PZipper;
    procedure DoSearchZippers;
    procedure SetUnZipDir(const Value: string);
  public
    constructor Create(AOwner: TComponent); virtual;
    destructor Destroy; override;
    procedure Clear; override;
    function GetRealPath(const sFullPath: string): string;
    function IsMatchZipFile(const sFileName: string; var idx: Integer): Boolean;
    procedure SortByDllName(const sNameOrders: string);
    property ZipperPath: string read FPath write SetPath;
    property UnZipDir: string read FUnZipDir write SetUnZipDir;
    property Stop: Boolean read FStop write SetStop default False;
    property UnZips: TUnZipInfos read FUnZips;
    property ZipperName[Index: Integer]: string read GetZipperName; //Zip ..
    property ZipperDef[Index: Integer]: string read GetZipperDef;   //*.zip ..
    property Zipper[Index: Integer]: PZipper read GetZipper; default;
  end;

  function MatchFileName(const Source, Pattern: string; bNoCase: Boolean):Boolean;
  function ExtractFileNameAny(const FileName: string): string;
  function GetFileCreateTime(const sFileName: string): TFileTime;
  function SameFileTime(A, B: TFileTime): Boolean;
  function DirIsEmpty(sDir: string; bDelNil: Boolean): Boolean;

implementation

{ Internal functions }

function MatchFileName(const Source, Pattern: string; bNoCase: Boolean):Boolean;
         //文件名的模糊匹配，支持 ? 和 *   2005.11.26 由MatchStrings改进而来
  function MatchPattern(element, pattern: PChar): Boolean;
  const
    ALPHA = ['a'..'z', 'A'..'Z'];
  begin
    if 0 = StrComp(pattern, '*') then
      Result := True
    else if element^ = #0 then
      Result := (pattern^ = #0)
    else
    begin
      case pattern^ of
        '*':
          if MatchPattern(element, @pattern[1]) then
            Result := True
          else
            Result := MatchPattern(@element[1], pattern);
        '?': Result := MatchPattern(@element[1], @pattern[1]);
      else
        if (element^ = pattern^)
            or bNoCase and {$IFDEF UNICODE}CharInSet(element^, ALPHA) and CharInSet(pattern^, ALPHA){$ELSE}(element^ in ALPHA) and (pattern^ in ALPHA){$ENDIF}
                and (Ord(element^)and $df = Ord(pattern^) and $df) then
          Result := MatchPattern(@element[1], @pattern[1])
        else
          Result := False;
      end;
    end;
  end;
begin
  if (Pattern <> '') and (Pattern[Length(Pattern)] = '.') then
  begin
    if Pattern = '*.' then            //表示无扩展名
      Result := Pos('.', Source) = 0
    else if (Pos('.', Pattern) = Length(Pattern)) and (Pos('.', Source) > 0)then
      Result := False                 //处理 Pattern = f*. 的情况
    else
      Result := MatchPattern(PChar(Source + '.'), PChar(Pattern));
  end
  else
    Result := MatchPattern(PChar(Source), PChar(Pattern));
end;

function ExtractFileNameAny(const FileName: string): string;
{$IFDEF VER130}
const
  DriveDelim = ':';
{$ENDIF}
var                       //可处理Windows/Linux目录分隔符的 ExtractFileName
  I: Integer;
begin
  I := LastDelimiter('\|/' + DriveDelim, FileName);
  Result := Copy(FileName, I + 1, MaxInt);
end;

function GetFileCreateTime(const sFileName: string): TFileTime;
var
  iFile: Integer;
begin
  ZeroMemory(@Result, SizeOf(Result));
  iFile  := SysUtils.FileOpen(sFileName, fmOpenRead or fmShareDenyNone);
  if iFile >= 0 then try
    GetFileTime(iFile, nil, nil, @Result);
  finally
    FileClose(iFile);
  end;
end;

function SameFileTime(A, B: TFileTime): Boolean;
begin
  Result := (A.dwHighDateTime = B.dwHighDateTime) and
            (A.dwLowDateTime = B.dwLowDateTime);
end;

function DirIsEmpty(sDir: string; bDelNil: Boolean): Boolean;
var
  sr: TSearchRec;
begin
  Result := True;
  if (sDir <> '') and (sDir[Length(sDir)] <> '\') then sDir := sDir + '\';
  if FindFirst(sDir + '*.*', faAnyFile, sr) = 0 then try
    repeat
      if sr.Attr and faDirectory = faDirectory then begin
        if (sr.Name <> '.') and (sr.Name <> '..') then begin
          if bDelNil and DirIsEmpty(sDir + sr.Name, bDelNil) then begin
            RmDir(sDir + sr.Name);   //删掉空的子目录
          end else begin
            Result := False;
            if not bDelNil then Break;
          end;
        end;
      end else begin
        Result := False;
        if not bDelNil then Break;
      end;
    until FindNext(sr) <> 0;
  finally
    FindClose(sr);
  end;
end;

function MyFetch(var S: string; const sDelimiter: string): string;
var
  idx: Integer;
begin
  idx := Pos(sDelimiter, S);
  if idx > 0 then begin
    Result := Copy(S, 1, idx -1);
    System.Delete(S, 1, idx + Length(sDelimiter) -1);
  end else begin
    Result := S;
    S := '';
  end;
end;

{ TUnZipInfos }

constructor TUnZipInfos.Create;
begin
  FAutoDel := True;
end;

destructor TUnZipInfos.Destroy;
begin
  Clear;
  inherited;
end;

function TUnZipInfos.AddFirst(const sZipPath, sRealPath: string): Integer;
var
  p: PUnZipInfo;
begin
  Result := IndexOfZipPath(sZipPath);
  if Result >= 0 then begin
    {if FAutoDel then begin
      SetFileAttributes(PChar(Items[Result].RealPath), FILE_ATTRIBUTE_NORMAL);
      DeleteFile(Items[Result].RealPath);
    end; }
    Items[Result].RealPath := sRealPath;
  end else begin
    New(p);
    p.ZipPath  := sZipPath;
    p.RealPath := sRealPath;
    p.CreateTime := GetFileCreateTime(sRealPath);
    Insert(0, p);
    Result := 0;
  end;
end;

procedure TUnZipInfos.Clear;
var
  i: Integer;
begin
  for i := 0 to Count -1 do begin
    SimpleDelete(i);
  end;
  if FUnZipDir <> '' then DirIsEmpty(FUnZipDir, True);
  inherited;
end;

procedure TUnZipInfos.SimpleDelete(Index: Integer);
var
  p: PUnZipInfo;
begin
  p := PUnZipInfo(inherited Items[Index]);
  if FAutoDel then begin
    SetFileAttributes(PChar(p.RealPath), FILE_ATTRIBUTE_NORMAL);
    DeleteFile(p.RealPath);
    DirIsEmpty(ExtractFilePath(p.RealPath), True);
  end;
  Dispose(p);
end;

procedure TUnZipInfos.Delete(Index: Integer);
begin
  SimpleDelete(Index);
  inherited Delete(Index);
end;

procedure TUnZipInfos.DeleteUnZip(const sZipPath: string);
var
  idx: Integer;
begin
  idx := IndexOfZipPath(sZipPath);
  if idx >= 0 then Delete(idx);
end;

function TUnZipInfos.IndexOfZipPath(const sZipPath: string): Integer;
var
  i: Integer;
begin
  for i := 0 to Count -1 do begin
    if SameText(sZipPath, PUnZipInfo(inherited Items[i]).ZipPath) then begin
      Result := i;
      Exit;
    end;
  end;
  Result := -1;
end;

function TUnZipInfos.Get(Index: Integer): PUnZipInfo;
begin
  if (Index >= 0) and (Index < Count) then begin
    Result := PUnZipInfo(inherited Items[Index]);
  end else begin
    Result := nil;
  end;
end;

procedure TUnZipInfos.Put(Index: Integer; Item: PUnZipInfo);
begin
  if (Index >= 0) and (Index < Count) then begin
    inherited Items[Index] := Item;
  end;
end;

{ TZippers }

constructor TZippers.Create(AOwner: TComponent);
begin
  inherited Create;
  FOwner  := AOwner;
  FUnZips := TUnZipInfos.Create;
  FDllZippers := TList.Create;
end;

destructor TZippers.Destroy;
begin
  Clear;
  FUnZips.Free;
  FDllZippers.Free;
  inherited;
end;

procedure TZippers.Clear;
var
  i: Integer;
begin
  for i := 0 to FDllZippers.Count -1 do begin
    with PZipper(FDllZippers[i])^ do begin
      if DllHandle <> 0 then FreeLibrary(DllHandle);
    end;
    Dispose(PZipper(FDllZippers[i]));
  end;
  FDllZippers.Clear;
  inherited;
end;

procedure TZippers.DoSearchZippers;

  function DoFetch(var S: string; const sDeli: string): string;
  var
    idx: Integer;
  begin
    idx := Pos(sDeli, S);
    if idx > 0 then begin
      Result := Copy(S, 1, idx -1);
      System.Delete(S, 1, idx + Length(sDeli) -1);
    end else begin
      Result := S;
      S := '';
    end;
  end;

const
  cBufLen = 1024;
var
  hPlugIn: THandle;
  sr: TSearchRec;
  sPath: string;
  pGetZipDef: TGetZipDefProc;
  pUnZipFile: TUnZipFileProc;
  pEnumZipFiles: TEnumZipFilesProc;
  pGetAllZipFiles: TGetAllZipFilesProc;
  sBuf: array[0..cBufLen] of Char;
  iLen: Integer;
  pZip: PZipper;
  sDefStr, sZipName, sZipDef: string;
  bFindPlugIn: Boolean;
begin
  Clear;
  if Assigned(FOwner) and (csDesigning in FOwner.ComponentState) then begin
    Exit; //避免设计期搜索Delphi所在目录里的DLL后报某些DLL不存在的错误2012.9.20
  end;
  if FPath = '' then begin
    sPath := ExtractFilePath(ParamStr(0)); //'.\';  必须用绝对路径，否则可能出现Zipper初始化错误 2012.8.25
  end else begin
    sPath := IncludeTrailingBackslash(FPath);
  end;
  if FindFirst(sPath + '*.dll', faAnyFile, sr) = 0 then begin
    repeat
      Application.ProcessMessages;
      if FStop then Break;
      hPlugIn := LoadLibrary(PChar(sPath + sr.Name));
      if hPlugIn <> 0 then begin
        @pGetZipDef := GetProcAddress(hPlugIn, cpGetZipDef);
        if Assigned(pGetZipDef) then begin
          iLen := cBufLen;
          if pGetZipDef(sBuf, iLen) then begin
            @pUnZipFile := GetProcAddress(hPlugIn, cpUnZipFile);
            @pEnumZipFiles := GetProcAddress(hPlugIn, cpEnumZipFiles);
            @pGetAllZipFiles := GetProcAddress(hPlugIn, cpGetAllZipFiles);
            sDefStr := Copy(sBuf, 0, iLen);
            bFindPlugIn := False;
            while sDefStr <> '' do begin
              sZipDef := DoFetch(sDefStr, #13#10);
              sZipName := DoFetch(sZipDef, '=');
              if (sZipName <> '') and Assigned(pUnZipFile) and
                  Assigned(pEnumZipFiles) and Assigned(pGetAllZipFiles) then begin
                AddObject(Format('%s=%s', [sZipName, sZipDef]), TObject(FDllZippers.Count));
                bFindPlugIn := True;
              end;
            end;
            if bFindPlugIn then begin
              New(pZip);
              pZip.DllName   := sr.Name;
              pZip.DllHandle := hPlugIn;
              pZip.UnZipFile := pUnZipFile;
              pZip.EnumZipFiles := pEnumZipFiles;
              pZip.GetAllZipFiles := pGetAllZipFiles;
              FDllZippers.Add(pZip);
              hPlugIn := 0;
            end;
          end;
        end;
        if hPlugIn <> 0 then FreeLibrary(hPlugIn);
      end;
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;
end;

procedure TZippers.SetPath(const Value: string);
begin
  if (FPath <> Value) or (Value = '') or (Count = 0) then begin
    FPath := Value;
    DoSearchZippers;
  end;
end;

procedure TZippers.SetUnZipDir(const Value: string);
begin
  if FUnZipDir <> Value then begin
    FUnZipDir := Value;
    FUnZips.UnZipDir := Value;
  end;
end;

procedure TZippers.SetStop(Value: Boolean);
begin
  if FStop <> Value then begin
    FStop := Value;
  end;
end;

function TZippers.GetZipperDef(Index: Integer): string;
begin
  if (Index >= 0) and (Index < Count) then begin
    Result := Copy(Strings[Index], Pos('=', Strings[Index]) +1, MaxInt);
  end else begin
    Result := '';
  end;
end;

function TZippers.GetZipperName(Index: Integer): string;
begin
  if (Index >= 0) and (Index < Count) then begin
    Result := Copy(Strings[Index], 1, Pos('=', Strings[Index]) -1);
  end else begin
    Result := '';
  end;
end;

function TZippers.GetZipper(Index: Integer): PZipper;
begin
  if (Index >= 0) and (Index < Count) then begin
    Result := PZipper(FDllZippers[Integer(Objects[Index])]);
  end else begin
    Result := nil;
  end;
end;

function TZippers.GetRealPath(const sFullPath: string): string;

  function ZipFileExists(const sFullPath, sRealPath: string): Boolean;
  var               //更保险的方法是算文件的CRC,不过CreateTime够用了2009.9.26
    idx: Integer;
  begin
    idx := FUnZips.IndexOfZipPath(sFullPath);
    Result := (idx >= 0) and SameText(sRealPath, FUnZips.Items[idx]^.RealPath)
      and SameFileTime(GetFileCreateTime(sRealPath), FUnZips.Items[idx]^.CreateTime);
  end;

  {--------------------------------------------------------------------------
    函数: GetUsableOutFile
    目的: 处理多级压缩包,如以下形式的压缩包:
          SkyDirFileTree.rar
              |----------SkyDirFileTree.pas
              |----------SkyDirFileTree.rar
                             |----------SkyDirFileTree.pas
                             |----------SkyDirFileTree.rar
                                            |----------SkyDirFileTree.pas
          解压缩到临时目录,应得到如下结果:
          TempDir\
               |--SkyDirFileTree.pas
               |--SkyDirFileTree.rar\
                      |--------------SkyDirFileTree.pas
                      |--------------SkyDirFileTree.rar
                      |--------------SkyDirFileTree.rar~0\
                                         |----------------SkyDirFileTree.pas
                                         |----------------SkyDirFileTree.rar
  ---------------------------------------------------------------------------}
  function GetUsableOutFile(const sOutFile: string): string;
  var
    idxDir: Integer;
    sDir, sName: string;
  begin
    Result := sOutFile;
    idxDir := 0;
    while FileExists(ExtractFileDir(Result)) do begin
      if sDir = '' then begin
        sDir  := ExtractFileDir(Result);
        sName := ExtractFileName(Result);
      end;
      Result := Format('%s~%d\%s', [sDir, idxDir, sName]);
      Inc(idxDir);
    end;
    if idxDir > 0 then begin
      ForceDirectories(ExtractFileDir(Result));
    end;
  end;

var
  idx, iTmp: Integer;
  sZipFile, sSub: string;
  pUnZipFile: TUnZipFileProc;
  sUnZipDir, sRealSub, sOutFile: string;
begin
  Result := sFullPath;
  idx := Pos(cZipDelimiter, sFullPath);
  if idx > 0 then begin
    sZipFile  := Copy(sFullPath, 1, idx -1);
    sSub      := Copy(sFullPath , idx +1, MaxInt);
    sUnZipDir := IncludeTrailingBackslash(FUnZipDir);
    while (sSub <> '') and IsMatchZipFile(sZipFile, idx) do begin
      sRealSub  := MyFetch(sSub, cZipDelimiter);
      if (sSub <> '') or IsMatchZipFile(sRealSub, iTmp) then begin
        sUnZipDir := sUnZipDir + sRealSub + '\';
      end;
      sOutFile := sUnZipDir + ExtractFileNameAny(sRealSub);
      sOutFile := GetUsableOutFile(sOutFile);
      sUnZipDir := ExtractFilePath(sOutFile);
      ForceDirectories(sUnZipDir);
      SetFileAttributes(PChar(sOutFile), FILE_ATTRIBUTE_NORMAL);
      pUnZipFile := Zipper[idx].UnZipFile;
      if ZipFileExists(sZipFile + cZipDelimiter + sRealSub, sOutFile) or
          Assigned(pUnZipFile) and pUnZipFile(PChar(sZipFile), PChar(sRealSub), PChar(sUnZipDir)) then begin
        FUnZips.AddFirst(sZipFile + cZipDelimiter + sRealSub, sOutFile);
        sZipFile  := sOutFile;
      end else begin
        Exit;
      end;
    end;
    Result := sZipFile;
  end;
end;

function TZippers.IsMatchZipFile(const sFileName: string;
  var idx: Integer): Boolean;
var
  i: Integer;
begin
  idx := -1;
  Result := False;
  for i := 0 to Count -1 do begin
    if MatchFileName(sFileName, ZipperDef[i], True) then begin
      Result := True;
      idx := i;
      Break;
    end;
  end;
end;

procedure TZippers.SortByDllName(const sNameOrders: string);

  procedure InsertionSort(slSerial: TStrings; iLo, iHi: Integer);

    function GetSerialNo(idx: Integer): Integer;
    var
      m: Integer;
    begin
      m := slSerial.IndexOf(IntToStr(Zipper[idx]^.DllHandle));
      if m >= 0 then begin
        Result := Integer(slSerial.Objects[m]);
      end else begin
        Result := -1;
      end;
    end;

  var
    i, j, T: Integer;
    pDef: string;
    pObj: TObject;
  begin
    for i := iLo + 1 to iHi do begin
      T := GetSerialNo(i);
      pDef := Strings[i];
      pObj := Objects[i];
      j := i -1;
      while (j >= iLo) and (GetSerialNo(j) > T) do begin
        Strings[j + 1] := Strings[j];
        Objects[j + 1] := Objects[j];
        Dec(j);
      end;
      Strings[j + 1] := pDef;
      Objects[j + 1] := pObj;
    end;
  end;

var
  i, idx: Integer;
  slOrders, slSerial: TStrings;
begin
  slOrders := TStringList.Create;
  slSerial := TStringList.Create;
  try
    slOrders.Text := sNameOrders;
    //抽取Dll句柄及其索引
    for i := 0 to FDllZippers.Count -1 do begin
      with PZipper(FDllZippers[i])^ do begin
        idx := slOrders.IndexOf(DllName);
        if idx >= 0 then begin
          slSerial.AddObject(IntToStr(DllHandle), TObject(idx));
        end;
      end;
    end;
    //按索引对Zippers进行排序
    if slSerial.Count > 0 then begin
      InsertionSort(slSerial, 0, Count -1);
    end;
  finally
    slOrders.Free;
    slSerial.Free;
  end;
end;

end.
