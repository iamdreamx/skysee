{*****************************************************************************
*               TSkyDirFileTree  ----  一个文件目录树控件                    *
*                                                                            *
*  功能：显示驱动器、目录及文件                                              *
*  版本：0.98 alpha                                                          *
*  作者：顾中军                                                              *
*  实现：                                                                    *
*      编这个控件的初衷是为我写的一个文本搜索器提供目录树选择功能，后来则加  *
*  入对文件的支持，组合了显示目录及文件的功能，可以用来写文本编辑器了。      *
*      2005.10.27~2005.10.28 完成第一个目录树版本，支持D5~D2005!             *
*      2005.11.05~2005.11.06 加入对文件的支持（从我另一组件参考实现而来）；  *
*      2005.11.25~2005.11.26 改进了文件的过滤功能；                          *
*      2008.01.24~2008.01.26 采用TList及TStringList改进了排序方法，极大地提  *
*  高了有大量目录文件时结点的加载速度；                                      *
*      2008.02.04~2008.02.09 采用插件方式加入了对压缩文件的支持；            *
*      2009.09.18~2009.09.26 改进为支持读取压缩文件中的压缩文件；            *
*      2019.05.02~2019.05.04 修正高版本Delphi下运行时地址错误；              *
*      20xx....              增加自定义根结点的功能，可用于工程文件的管理；  *
*                                                                            *
*  说明：                                                                    *
*      这个东东是我整理以前自己写的代码重写而来，似乎用处不大噢^O^           *
*      也许它最大的用途是用来编写文本编辑器？！                              *  
*                                                                            *
*  版权声明：                                                                *
*      源码公开，你可以将它用于任何场合；                                    *
*      如果你有更好的改进，别忘了给我发一份啊；                              *
*      此外，如果你是在它的基础上改进，请保留我写的文本及说明，毕竟我花了不  *
*  少精力和时间，请尊重我的劳动成果；另外我希望你能将改进的代码公布出来，多  *
*  交流才能更快地进步嘛；                                                    *
*      最后，愿与所有喜爱软件开发的朋友们共勉：让世界因软件而变得更美好！    *
*                                                                            *
*  网站：不好意思，还没建立自己的网站呢                                      *
*  妹儿：iamdream@yeah.net                                                   *
*****************************************************************************}

unit SkyDirFileTree;

interface

// ========================== Determine compiler ========================== //
{$IFDEF VER80} Sorry, Delphi 1 is not supported !  {$ENDIF} // D1
{$IFDEF VER90} Sorry, Delphi 2 is not supported !  {$ENDIF} // D2
{$IFDEF VER93} Sorry, BCB 1 is not supported !     {$ENDIF} // CPPB 1
{$IFDEF VER100} Sorry, Delphi 3 is not supported ! {$ENDIF} // D3
{$IFDEF VER110} Sorry, BCB 3 is not supported !    {$ENDIF} // CPPB 3
{$IFDEF VER120} Sorry, Delphi 4 is not supported ! {$ENDIF} // D4
{$IFDEF VER125} Sorry, BCB 4 is not supported !    {$ENDIF} // CPPB 4
{$IFDEF VER130}        //D5
  {$DEFINE DELPHI5UP}
{$ENDIF}
{$IFDEF VER135}        //BCB5
  {$DEFINE DELPHI5UP}
{$ENDIF}
{$IFDEF VER140}        //D6
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
{$ENDIF}
{$IFDEF VER145}        //BCB6
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
{$ENDIF}
{$IFDEF VER150}        //D7
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
{$ENDIF}
{$IFDEF VER170}        //D9(D2005)
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
{$ENDIF}
{$IFDEF VER180}        //D10(D2006)|BCB10
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
{$ENDIF}
{$IFDEF VER185}        //D11(D2007)|BCB11
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
{$ENDIF}
{$IFDEF VER200}        //D12(D2009)|BCB12
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
{$ENDIF}
{$IFDEF VER210}        //D14(D2010)|BCB14
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
{$ENDIF}
{$IFDEF VER220}        //DelphiXE
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
{$ENDIF}
{$IFDEF VER230}        //DelphiXE2
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
  {$DEFINE DELPHIXE2UP}
{$ENDIF}
{$IFDEF VER240}        //DelphiXE3
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
  {$DEFINE DELPHIXE2UP}
  {$DEFINE DELPHIXE3UP}
{$ENDIF}
{$IFDEF VER250}        //DelphiXE4
  {$DEFINE DELPHI5UP}
  {$DEFINE DELPHI6UP}
  {$DEFINE DELPHI7UP}
  {$DEFINE D2005UP}
  {$DEFINE D2006UP}
  {$DEFINE D2007UP}
  {$DEFINE D2009UP}
  {$DEFINE D2010UP}
  {$DEFINE DELPHIXEUP}
  {$DEFINE DELPHIXE2UP}
  {$DEFINE DELPHIXE3UP}
  {$DEFINE DELPHIXE4UP}
{$ENDIF}
{$IFDEF UNICODE}
  {$IF CompilerVersion > 25}        //DelphiXE4 later
    {$DEFINE DELPHI5UP}
    {$DEFINE DELPHI6UP}
    {$DEFINE DELPHI7UP}
    {$DEFINE D2005UP}
    {$DEFINE D2006UP}
    {$DEFINE D2007UP}
    {$DEFINE D2009UP}
    {$DEFINE D2010UP}
    {$DEFINE DELPHIXEUP}
    {$DEFINE DELPHIXE2UP}
    {$DEFINE DELPHIXE3UP}
    {$DEFINE DELPHIXE4UP}
  {$IFEND}
{$ENDIF}

{$IFDEF DELPHI6UP}
  {$WARN SYMBOL_PLATFORM OFF}
{$ENDIF}
{$IFDEF DELPHI7UP}            //取消D7警告
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
  {$WARN SYMBOL_PLATFORM OFF}
{$ENDIF}
// ========================== Determine compiler ========================== //

uses
  Windows, Messages, SysUtils, Classes, Controls, ComCtrls, Forms, ImgList,
  {$IFDEF CONDITIONALEXPRESSIONS}
  {$IF CompilerVersion >= 23.0} //Delphi XE2+
  System.Types, System.UITypes,
  {$IFEND}
  {$ENDIF}
  Graphics, Commctrl, ShellApi, SkyZipPlugIn;

const
  SKY_ROOT_IMG_IDX = 0;
  SKY_FLOPPYDRV_IMG_IDX = 1;       //软驱结点之ImageIndex
  SKY_REMOVEDRV_IMG_IDX = 2;       //可移动盘结点之ImageIndex
  SKY_HARDDRV_IMG_IDX   = 3;       //硬驱结点之ImageIndex
  SKY_REMOTEDRV_IMG_IDX = 4;       //网络驱动器结点之ImageIndex
  SKY_CDROMDRV_IMG_IDX  = 5;       //光驱结点之ImageIndex
  SKY_RAMDRV_IMG_IDX    = 6;       //内存盘结点之ImageIndex
  SKY_DIR_IMG_IDX  = 7;            //目录结点之ImageIndex
  SKY_DIR_SEL_IDX  = 8;            //目录结点之SelectedIndex
  SKY_FILE_IMG_IDX = 9;            //文件结点之ImageIndex
  SKY_ZIP_IMG_IDX_START = 10;      //压缩文件结点之起始ImageIndex

type

  TSkyDirFileErrEvent = procedure (Sender: TObject; var sErr: String;
                                   var bHandled: Boolean) of object;

  TSkyDirFileSearch   = (sdfFindFile, sdfFindHidden, sdfHideReadOnly, sdfFindZip);
  TSkyDirFileSearchs  = set of TSkyDirFileSearch;

  TSkyDirFileTree = class(TCustomTreeView)
  private
    { Private declarations }
    FbUpdateTreeNode: Boolean;     //展开树结点时应更新它???
    FbQuickSearch:    Boolean;     //快速搜索子目录???即不预搜索下级子目录
    FbVkDownUp:       Boolean;
    FBoldFontOfDir:   Boolean;     //目录结点 之字体设置为 粗体 ???
    {$IFDEF VER130}
    FColorChanging:   Boolean;
    {$ENDIF}
    FDoubleClicked:   Boolean;     //刚进行鼠标双击?  2009.10.12

    FRootNode:        TTreeNode;   //目录树根结点
    FNowNode:         TTreeNode;   //当前操作结点
    FLastNode:        TTreeNode;   //上一次操作结点

    FRootName:        String;

    FDefTVImages:     TImageList;  //默认的ImageList

    FOnError:         TSkyDirFileErrEvent;   //发生错误时触发的事件
    FOnUpdateNode:    TNotifyEvent;          //更新结点时触发的事件

    {$IFNDEF DELPHI6UP}
    FFileTpDelimiter: Char;        //Delphi5 才用
    {$ENDIF}
    FFileType:        String;      //文件类型（扩展名）
    FslFileTypes:     TStringList; //文件类型（扩展名）- 已排序的
    FOptions:         TSkyDirFileSearchs;    //搜索选项
    FZipperPath:      string;      //压缩插件所在路径
    FZippers:         TZippers;    //压缩插件列表
    FUnZipDir:        string;      //解压缩临时目录
    FDirList,                      //加速用  2008.1.26
    FFileList:        TStringList; //加速用
    FImgIdxList:      TList;       //加速用

    function GetDriveNodeName(cDrive: Char; dwDriveType: DWORD;
        var dwSerialNum: DWORD; var iImgIdx: Integer;
        bFirstLoading: Boolean) : String;
    procedure InitDirTree(bLoading: Boolean = False);
    procedure DoEnumZipFiles(Node, ndParent: TTreeNode; sPath: string; bFindFirst: Boolean);
    procedure GetNodeChild(Node: TTreeNode);
    procedure GetNodeFirstChild(Node: TTreeNode);
    procedure UpdateDirNode(Node: TTreeNode);
    procedure UpdateDriveNode(Node: TTreeNode);
    procedure InitZippers;

    function GetSelectNodeText: String;
    procedure SetSelectNodeText(const Value: String);
    //function GetRootName: String;
    procedure SetRootName(const Value: String);
    function GetImages: TCustomImageList;
    procedure SetImages(Value: TCustomImageList);
    {$IFDEF DELPHI6UP}
    function GetFileTpDelimiter: Char;
    procedure SetFileTpDelimiter(Value: Char);
    {$ENDIF}
    function GetFileTypes: String;
    procedure SetFileTypes(const Value: String);
    procedure SetBoldFontOfDir(Value: Boolean);
    procedure SetZipperPath(const Value: string);
    procedure SetUnZipDir(const Value: string);
    {$IFDEF VER130}
    procedure CMRecreateWnd(var Message: TMessage); message CM_RECREATEWND;
    procedure CMColorChanged(var Message: TMessage); message CM_COLORCHANGED;
    {$ENDIF}

  protected
    { Protected declarations }
    function CanExpand(Node: TTreeNode): Boolean; override;
    procedure Change(Node: TTreeNode); override;
    procedure DblClick; override;
    procedure Click; override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
        X, Y: Integer); override;
    function CustomDrawItem(Node: TTreeNode; State: TCustomDrawState;
        Stage: TCustomDrawStage; var PaintImages: Boolean): Boolean; override;
    {$IFDEF DELPHI6UP}
    function IsCustomDrawn(Target: TCustomDrawTarget; //真见鬼! D5无法重载!
        Stage: TCustomDrawStage): Boolean; override;
    {$ENDIF}

    procedure CreateWnd; override;
    procedure Loaded; override;

    procedure InitDriveNodes(bFirstLoading: Boolean = False);
    function ShouldShow(Attr: Integer): Boolean;
    function IsMatchFileType(const sFileName: string): Boolean;  //2005.11.26
    function IsMatchZipFile(const sFileName: string; var idx: Integer): Boolean;

    procedure GetZipFile(Node: TTreeNode; sZipDir, sSub: string;
      bFindFirst, bIsDir: Boolean; var bHandled: Boolean);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure DeleteSelectedNode;                    //删除选中结点
    procedure UpdateSelectedNode;                    //更新选中结点（当前目录）
    procedure UpdateTreeNode(Node: TTreeNode);       //更新指定结点

    function GetFullPath(Node: TTreeNode): String;   //放在此处，以处理多选
    function GetRealPath(const sFullPath: string): string; overload;
    function GetRealPath(Node: TTreeNode): string; overload;
    function GetSelectNodeFullPath: String;

    function GetAllNodeCount: Integer;               //所有结点数
    function GetNodeChildCount(Node: TTreeNode): Integer;
    function GetNodeAllChildrenCount(Node: TTreeNode): Integer;
    function GetSelectNodeChildCount: Integer;       //当前结点第一级子结点数
    function GetSelectNodeAllChildrenCount: Integer; //当前结点所有子结点数
    function NodeIsDir(Node: TTreeNode): Boolean;
    function NodeIsFile(Node: TTreeNode): Boolean;
    function NodeIsPureFile(Node: TTreeNode): Boolean;
    function NodeIsZip(Node: TTreeNode): Boolean;
    function NodeIsZipChild(Node: TTreeNode): Boolean;
    function SelectedNodeIsDir: Boolean;             //选中结点是目录???
    function SelectedNodeIsFile: Boolean;            //选中结点是文件???
    function SelectedNodeIsPureFile: Boolean;
    function SelectedNodeIsZip: Boolean;             //选中结点是压缩文件???
    function SelectedNodeIsZipChild: Boolean;

    procedure UpdateOfInitPath(const sPath: String);
    function NodeIsNotRoot(Node: TTreeNode): Boolean;
    function SelectedIsNotRoot: Boolean;             //当前所选结点不是根结点?
    procedure SortZipperByDllName(const sNameOrders: string);
    
    property SelectNodeText: String read GetSelectNodeText
        write SetSelectNodeText;
  published
    { Published declarations }
    property Align;
    property Anchors;
    property AutoExpand;
    property BiDiMode;
    property BorderStyle;
    property BorderWidth;
    property ChangeDelay;
    property Color;
    property Ctl3D;
    property Constraints;
    property DragKind;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection default False;
    property HotTrack;
    //property Images;       TSkyDirFileTree 在下面重载了 2008.2.9
    property Indent;
    {$IFDEF DELPHI6UP}
    property MultiSelect default False;
    property MultiSelectStyle default [msControlSelect];
    {$ENDIF}
    property ParentBiDiMode;
    property ParentColor default False;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly default True;
    property RightClickSelect default True;
    property RowSelect;
    property ShowButtons;
    property ShowHint;
    property ShowLines;
    property ShowRoot;
    //property SortType;     TSkyDirFileTree 不需要 2008.2.9
    property StateImages;
    property TabOrder;
    property TabStop default True;
    property ToolTips;
    property Visible;

    property OnAdvancedCustomDraw;
    property OnAdvancedCustomDrawItem;
    property OnChange;
    property OnChanging;
    property OnClick;
    property OnCollapsed;
    property OnCollapsing;
    property OnCompare;
    property OnContextPopup;
    property OnCustomDraw;
    property OnCustomDrawItem;
    property OnDblClick;
    property OnDeletion;
    property OnDragDrop;
    property OnDragOver;
    property OnEdited;
    property OnEditing;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnExpanding;
    property OnExpanded;
    property OnGetImageIndex;
    property OnGetSelectedIndex;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
    { Items must be published after OnGetImageIndex and OnGetSelectedIndex }
    property Items stored False; //设计期数据不存到DFM  2012.9.20

    // 以下是TSkyDirFileTree的特殊属性  2008.2.9
    property QuickSearch: Boolean     read FbQuickSearch
                                      write FbQuickSearch    default False;
    property BoldFontOfDir: Boolean read FBoldFontOfDir
        write SetBoldFontOfDir default False;
    property RootName:    String      read {Get}FRootName write SetRootName;
    property Images: TCustomImageList read GetImages   write SetImages;
    {$IFDEF DELPHI6UP}
    property FileTpDelimiter: Char read GetFileTpDelimiter
        write SetFileTpDelimiter default '|';
    {$ELSE}
    property FileTpDelimiter: Char read FFileTpDelimiter
        write FFileTpDelimiter default '|';
    {$ENDIF}
    property FileTypes:   String      read GetFileTypes write SetFileTypes;
    property Options:     TSkyDirFileSearchs read FOptions
                          write FOptions default [sdfFindFile, sdfFindHidden];
    property UnZipDir:   string read FUnZipDir   write SetUnZipDir;
    property ZipperPath: string read FZipperPath write SetZipperPath;

    property OnError: TSkyDirFileErrEvent   read FOnError write FOnError;
    property OnUpdateNode: TNotifyEvent read FOnUpdateNode write FOnUpdateNode;
  end;

procedure Register;

{$IFDEF MULTILANGUAGE}
var
  rsDirFileHint: string;  // '目录树区';
  rsNoFloppyTip: string;  // '软驱内空空！请在软驱中插入磁盘。';
  rsNoRemoveTip: string;  // '请确定移动存储器是否插好！';
  rsNoRemoteTip: string;  // '请确定网络硬盘映射是否正确！';
  rsNoCdromTip: string;   // '光驱内空空！请将光盘放入光驱。';
  rsFloppyName: string;   // '3.5 吋软盘';
  rsRemoveName: string;   // '移动存储器';
  rsFixedName: string;    // '本地硬盘';
  rsRemoteName: string;   // '网络硬盘';
  rsCdromName: string;    // '光驱';
  rsRamDiskName: string;  // '虚拟盘';
  rsDriveNotFind: string; // '找不到驱动器（%s），不能更新！';
  rsRootName: string;     // '我的电脑';
{$ELSE}
resourcestring
  rsDirFileHint  = '目录树区';
  rsNoFloppyTip  = '软驱内空空！请在软驱中插入磁盘。';
  rsNoRemoveTip  = '请确定移动存储器是否插好！';
  rsNoRemoteTip  = '请确定网络硬盘映射是否正确！';
  rsNoCdromTip   = '光驱内空空！请将光盘放入光驱。';
  rsFloppyName   = '3.5 吋软盘';
  rsRemoveName   = '移动存储器';
  rsFixedName    = '本地硬盘';
  rsRemoteName   = '网络硬盘';
  rsCdromName    = '光驱';
  rsRamDiskName  = '虚拟盘';
  rsDriveNotFind = '找不到驱动器（%s），不能更新！';
  rsRootName     = '我的电脑';
{$ENDIF}

implementation

{$R *.Res}

procedure Register;
begin
  RegisterComponents('Samples', [TSkyDirFileTree]);
end;

{$IFDEF MULTILANGUAGE}
procedure InitResourceStrs;
begin
  rsDirFileHint  := 'Folder Area';     // '目录树区';
  rsNoFloppyTip  := 'Floppy Drive is Empty! Please Insert a Floppy Disk.'; // '软驱内空空！请在软驱中插入磁盘。';
  rsNoRemoveTip  := 'Please Ensure the Removable is ready.';               // '请确定移动存储器是否插好！';
  rsNoRemoteTip  := 'Remote Drive Map Error.';                             // '请确定网络硬盘映射是否正确！';
  rsNoCdromTip   := 'CD-ROM is Empty! Please Insert an Optical Disk.';     // '光驱内空空！请将光盘放入光驱。';
  rsFloppyName   := '3.5" Floppy';     // '3.5 吋软盘';
  rsRemoveName   := 'Removable Drive'; // '移动存储器';
  rsFixedName    := 'Fixed Drive';     // '本地硬盘';
  rsRemoteName   := 'Remote Drive';    // '网络硬盘';
  rsCdromName    := 'CD-ROM';          // '光驱';
  rsRamDiskName  := 'Ramdisk Drive';   // '虚拟盘';
  rsDriveNotFind := 'Drive(%s) not Found, Can''t Update!'; // '找不到驱动器（%s），不能更新！';
  rsRootName     := 'My Computer';     // '我的电脑';
end;
{$ENDIF}

//----------------------------------------------------------------------------
{
function CustomSortProc(Node_1, Node_2, ParamSort: LongInt): LongInt; stdcall;
var                     //对 TreeView 结点进行排序（按目录，文件名）
  Node1, Node2: TTreeNode;
begin
  Node1 := TTreeNode(Node_1);
  Node2 := TTreeNode(Node_2);
  if (Node1.ImageIndex = Node2.ImageIndex) or
     (Node1.ImageIndex >= SKY_FILE_IMG_IDX) and (Node2.ImageIndex >= SKY_FILE_IMG_IDX) then
  begin
    Result := AnsiCompareText(Node1.Text, Node2.Text);
  end
  else if Node1.ImageIndex > Node2.ImageIndex then
    Result := 1         //ImageIndex: 文件(2) > 目录(1)
  else Result := -1;
end;
}
function GetFileSmallIcon(const vFileName: string): HICON;
var
  shfi: SHFILEINFO;
begin
  FillChar(shfi, Sizeof(shfi), 0);
  SHGetFileInfo(PChar(vFileName),
            FILE_ATTRIBUTE_NORMAL,
            shfi,
            Sizeof(shfi),
            SHGFI_ICON or SHGFI_SMALLICON or SHGFI_USEFILEATTRIBUTES);
  Result := shfi.hIcon;
end;

procedure ZipFileCallBack(Node: Pointer; sZipDir, sSub: PChar;
  bFindFirst, bIsDir: Boolean; var bHandled: Boolean); stdcall;
begin
  TSkyDirFileTree(TTreeNode(Node).TreeView).GetZipFile(TTreeNode(Node),
    sZipDir, sSub, bFindFirst, bIsDir, bHandled);
end;

function ParentIsZip(Node: TTreeNode; var ndParent: TTreeNode): Boolean;
begin
  while Node <> nil do begin
    Node := Node.Parent;
    if (Node <> nil) and (Node.ImageIndex >= SKY_ZIP_IMG_IDX_START) then begin
      Result := True;
      ndParent := Node;
      Exit;
    end;
  end;
  Result := False;
  ndParent := nil;
end;

//----------------------------------------------------------------------------

constructor TSkyDirFileTree.Create(AOwner: TComponent);
var
  bmp: TBitmap;
begin
  Inherited;

  Width  := 160;
  Height := 200;

  HideSelection    := False;
  ReadOnly         := True;
  RightClickSelect := True;

  FRootName        := rsRootName;
  FRootNode        := nil;

  FDefTVImages   := TImageList.Create(Self);
  with FDefTVImages do  //默认Height=16, Width=16
  begin                 //直接载入时只支持 16 色??? 2005.3.18
    //GetResource(rtBitmap, 'DIRFILETREEICON', 16, [lrDefaultColor], clFuchsia);
    //ResourceLoad(rtBitmap, 'DIRFILETREEICON', clFuchsia);
    //FileLoad(rtBitmap, 'E:\aaa256.bmp', clFuchsia);
    bmp := TBitmap.Create;
    try
      bmp.LoadFromResourceName(HInstance, 'SKYDIRFILETREEICON');
      AddMasked(bmp, bmp.Canvas.Pixels[0, 0]);
    finally
      bmp.Free;
    end;
  end;

  Inherited Images := FDefTVImages;

  FslFileTypes     := TStringList.Create;
  {$IFDEF DELPHI6UP}
  FslFileTypes.Delimiter     := '|';
  FslFileTypes.CaseSensitive := False;
  {$ELSE}                        //Delphi5
  FFileTpDelimiter           := '|';
  {$ENDIF}
  FslFileTypes.Sorted        := True;
  FslFileTypes.Add('*.*');
  FFileType        := '*.*';
                   
  FOptions         := [sdfFindFile, sdfFindHidden];

  FDirList  := TStringList.Create;  //加速用
  FFileList := TStringList.Create;  //加速用
  FImgIdxList := TList.Create;      //加速用

end;

destructor TSkyDirFileTree.Destroy;
begin
  FslFileTypes.Free;

  Inherited Images := nil;   //应该先断开连接
  FDefTVImages.Free;         //然后再释放      2005.10.28

  FreeAndNil(FZippers);
  FreeAndNil(FDirList);
  FreeAndNil(FFileList);
  FreeAndNil(FImgIdxList);

  Inherited;
end;

function TSkyDirFileTree.CanExpand(Node: TTreeNode): Boolean;
begin                 //结点展开前
  Result := Inherited CanExpand(Node);

  if Result and  not (csLoading in ComponentState) then
  begin
    FLastNode := FNowNode;
    FNowNode  := Node;
    if (Node.Count = 1) and (Node.Item[0].ImageIndex < 0)then //只一空白子结点
    begin                                                     //需更新
      FbUpdateTreeNode := true;
      Node.DeleteChildren();
    end;
  end;
end;

procedure TSkyDirFileTree.Change(Node: TTreeNode);
begin
  inherited;

  if FbVkDownUp then
  begin
    FbVkDownUp := False;
    FLastNode  := FNowNode;
    FNowNode   := Node;
    if {(Node <> nil) and} (Node.Count = 0) and (Node.Level = 1) and (Node.Parent = FRootNode) then
    begin
      //Selected := Node;
      Application.ProcessMessages; //本行及下行 Sleep 是为了显示动画效果
      Sleep(10);
      UpdateTreeNode(Node);
    end;
  end;
end;

procedure TSkyDirFileTree.Click;
var
  Node: TTreeNode;
begin
  Inherited;

  if FbUpdateTreeNode then
    Node := FNowNode
  else
  begin
    Node := Selected;
    FLastNode := FNowNode;
    FNowNode  := Selected;
  end;

  if FbUpdateTreeNode or Assigned(Node) and (Node.Count = 0) and
     (Node.Level = 1) and (Node.Parent = FRootNode) then
  begin
    if FbUpdateTreeNode then  FbUpdateTreeNode := False;
    UpdateTreeNode(Node);
  end;
end;

procedure TSkyDirFileTree.DblClick;
begin
  Inherited;

  if (Selected <> nil)             //下行 Selected.Count = 0 实已将 Root 排除
     and (Selected.Count = 0) then //只有一个空白子结点或无子结点，需更新
  begin
    UpdateTreeNode(Selected);
  end;

  FDoubleClicked := True;
end;

procedure TSkyDirFileTree.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  Inherited;

  FLastNode := FNowNode;
  FNowNode  := Selected;
end;

procedure TSkyDirFileTree.KeyDown(var Key: Word; Shift: TShiftState);
begin
  Inherited;

  case Key of
    VK_RIGHT:
      begin
        if (Selected <> nil) and (
             (Selected.Count = 0) or
             (Selected.Count = 1) and (
               (Selected.Item[0].ImageIndex < 0) or
               (Selected.Level = 1) and (Selected.Parent = FRootNode))) then
        begin
          Selected.DeleteChildren();
          UpdateTreeNode(Selected);
        end;
      end;
    VK_DOWN,
    VK_UP:
      begin
        FbVkDownUp := True;
      end;
  end;
end;

procedure TSkyDirFileTree.MouseDown(Button: TMouseButton; Shift: TShiftState;
        X, Y: Integer);
{$IFDEF DELPHI6UP}
var
  selCount: Cardinal;
  ndSel: TTreeNode;
{$ENDIF}
begin
  {$IFDEF DELPHI6UP}
  selCount := SelectionCount;
  if FDoubleClicked and MultiSelect and (selCount > 1) then begin
    FDoubleClicked := False;
    ndSel := Selected;
  end else begin
    ndSel := nil;
  end;
  {$ENDIF}

  Inherited;

  if (Button = mbRight) then //与DoContextPopup()一起实现右键选择
    Selected := FNowNode;

  {$IFDEF DELPHI6UP}
  //当多选时,双击结点完了后会造成当前结点不选中,用以下方法令其重新选中
  if (ndSel <> nil) and (SelectionCount < selCount) then begin
    Self.Select(ndSel, [ssCtrl]);
  end;
  {$ENDIF}
end;

function TSkyDirFileTree.CustomDrawItem(Node: TTreeNode; State:TCustomDrawState;
    Stage: TCustomDrawStage; var PaintImages: Boolean): Boolean;
begin
  if FBoldFontOfDir and (Node.ImageIndex = SKY_DIR_IMG_IDX) then
    Canvas.Font.Style := Canvas.Font.Style + [fsBold];

  Result := inherited CustomDrawItem(Node, State, Stage, PaintImages);
end;

{$IFDEF DELPHI6UP}
function TSkyDirFileTree.IsCustomDrawn(Target: TCustomDrawTarget;
    Stage: TCustomDrawStage): Boolean;
begin
  Result := inherited IsCustomDrawn(Target, Stage) or FBoldFontOfDir;
end;
{$ENDIF}

procedure TSkyDirFileTree.CreateWnd;
begin
  inherited;

  if csDesigning in ComponentState then
  begin
    InitDirTree(True);             //初始化 “目录树”
    UpdateOfInitPath(ExtractFileDrive(Application.ExeName));
  end else begin
    if Self.Items.Count > 0 then begin
      FRootNode := Items[0];
    end;
  end;
end;

procedure TSkyDirFileTree.Loaded;
begin
  inherited;

  InitDirTree();                   //初始化 “目录树”

  if csDesigning in ComponentState then
  begin                            //为了设计时好看些  2005.10.28
    UpdateOfInitPath(ExtractFileDrive{Path}(Application.ExeName));
  end;
end;

function TSkyDirFileTree.GetDriveNodeName(cDrive: Char; dwDriveType: DWORD;
        var dwSerialNum: DWORD; var iImgIdx: Integer;
        bFirstLoading: Boolean) : String;
var
  cVolumeName:      Array [0..MAX_PATH] of Char;
  MaxLen, FileFlag: DWORD;
begin
  case dwDriveType of
    DRIVE_REMOVABLE:
      if cDrive < 'C' then        //简单区分一下
      begin
        Result  := rsFloppyName;
        iImgIdx := SKY_FLOPPYDRV_IMG_IDX;
      end
      else
      begin
        Result  := rsRemoveName;
        iImgIdx := SKY_REMOVEDRV_IMG_IDX;
      end;
    DRIVE_FIXED:
      begin
        Result  := rsFixedName;
        iImgIdx := SKY_HARDDRV_IMG_IDX;
      end;
    DRIVE_REMOTE:
      begin
        Result  := rsRemoteName;
        iImgIdx := SKY_REMOTEDRV_IMG_IDX;
      end;
    DRIVE_CDROM:
      begin
        Result  := rsCdromName;
        iImgIdx := SKY_CDROMDRV_IMG_IDX;
      end;
    DRIVE_RAMDISK:
      begin
        Result  := rsRamDiskName;
        iImgIdx := SKY_RAMDRV_IMG_IDX;
      end;
    else
      begin
        Result  := '';
        iImgIdx := -1;
      end;
  end;

  dwSerialNum   := 0;
  if Result <> '' then
  begin
    MaxLen := SizeOf(cVolumeName);
    if ((dwDriveType <> DRIVE_REMOVABLE) or (cDrive > 'C') or not bFirstLoading)
        and GetVolumeInformation(PChar(cDrive + ':\'),//否则取磁盘序列号
                                 cVolumeName,
                                 SizeOf(cVolumeName),
                                 @dwSerialNum,
                                 MaxLen,
                                 FileFlag,
                                 nil,
                                 0)
      then
    begin
      if cVolumeName[0] > #0 then //有卷标
        Result := StrPas(cVolumeName);
      if (dwDriveType = DRIVE_REMOTE)                 //网络硬盘
          and (WNetGetConnection(PChar(String(cDrive + ':')),    //取网络路径
                                 cVolumeName,
                                 MaxLen) = WN_SUCCESS)
          and (cVolumeName[0] > #0) then              //如果取到网络硬盘路径
        Result := StrPas(cVolumeName);                //则返回网络硬盘路径
    end
    else
      dwSerialNum := 0;
  end;
end;

procedure TSkyDirFileTree.InitDirTree(bLoading: Boolean);
var
  sRoot: String;
begin
  if bLoading {csLoading in ComponentState} then
    sRoot := rsRootName           //D5 下用csLoading设计时仍会出现错误?!
  else                            //2005.11.6
    sRoot := FRootName;
  Items.Clear();
  Items.Add(nil, sRoot{Name});
  FRootNode := Items.Item[0];
  FRootNode.ImageIndex    := 0;
  FRootNode.SelectedIndex := 0;
  InitDriveNodes(True);           //初始化各磁盘结点
end;

function TSkyDirFileTree.GetFullPath(Node: TTreeNode): String;
var                   //获取结点全路径（这里认为Node不空）
  sKeyPath: String;   //目录路径名
  sPath :   String;
begin
  while Assigned(Node) and (Node <> FRootNode) do
  begin
    sPath := Node.Text;
    if Node.Parent = FRootNode then
    begin                         //是磁盘根结点
      sKeyPath := Copy(sPath, Length(sPath) - 2, 2) + '\' + sKeyPath;
    end
    else
    begin
      if Assigned(Node.Parent) and (Node.Parent.ImageIndex >= SKY_ZIP_IMG_IDX_START) then begin //父结点为压缩文件
        if Node.ImageIndex = SKY_DIR_IMG_IDX then sPath := sPath + '\';
        sKeyPath := cZipDelimiter + sPath + sKeyPath;
      end else if Node.ImageIndex >= SKY_FILE_IMG_IDX then begin //文件结点
        sKeyPath := sPath + sKeyPath;
      end else begin                                             //目录结点
        sKeyPath := sPath + '\' + sKeyPath;
      end;
    end;
    Node := Node.Parent;
  end;
  Result := sKeyPath;
end;

function TSkyDirFileTree.GetRealPath(const sFullPath: string): string;
begin
  InitZippers;
  Result := FZippers.GetRealPath(sFullPath);
end;

function TSkyDirFileTree.GetRealPath(Node: TTreeNode): string;
begin
  Result := GetRealPath(GetFullPath(Node));
end;

function TSkyDirFileTree.ShouldShow(Attr: Integer): Boolean;
begin
  Result := ((Attr and SysUtils.faHidden) = 0)
              and ((Attr and SysUtils.faReadOnly) = 0)
            or ((Attr and SysUtils.faHidden) <> 0)
              and (sdfFindHidden in FOptions)
              and (not(sdfHideReadOnly in FOptions)
                   or ((Attr and SysUtils.faReadOnly) = 0)
                  )
            or ((Attr and SysUtils.faReadOnly) <> 0)
              and not(sdfHideReadOnly in FOptions)
              and ((sdfFindHidden in FOptions)
                   or ((Attr and SysUtils.faHidden) = 0)
                  );
end;

function TSkyDirFileTree.IsMatchFileType(const sFileName: string): Boolean;
var                      //对无扩展名[*.]的也进行了处理  2005.11.26
  i: Integer;
begin
  Result := False;
  for i := 0 to FslFileTypes.Count -1 do
    if MatchFileName(sFileName, FslFileTypes[i], True) then
    begin
      Result := True;
      Break;
    end;
end;

function TSkyDirFileTree.IsMatchZipFile(const sFileName: string; var idx: Integer): Boolean;
begin
  InitZippers;
  Result := FZippers.IsMatchZipFile(sFileName, idx);
end;

procedure TSkyDirFileTree.DoEnumZipFiles(Node, ndParent: TTreeNode; sPath: string; bFindFirst: Boolean);
var                       //注意,如果sPath是目录,则必须以\/结束!! 2009.9.23
  idx, idxZip: Integer;
  pEnumZipFiles: TEnumZipFilesProc;
  sSub: string;
begin
  if Node.ImageIndex = SKY_DIR_IMG_IDX then begin
    idxZip := ndParent.ImageIndex - SKY_ZIP_IMG_IDX_START;
  end else if Node.ImageIndex >= SKY_ZIP_IMG_IDX_START then begin //2008.2.8 ??
    idxZip := Node.ImageIndex - SKY_ZIP_IMG_IDX_START;
  end else begin
    Exit;
  end;
  pEnumZipFiles := FZippers[idxZip].EnumZipFiles;
  if Assigned(pEnumZipFiles) then begin
    idx := LastDelimiter(cZipDelimiter, sPath);
    if idx > 0 then begin
      if {$IFDEF UNICODE}CharInSet(sPath[Length(sPath)], ['\', '/']){$ELSE}sPath[Length(sPath)] in ['\', '/']{$ENDIF} then begin
        sSub := Copy(sPath, idx +1, MaxInt);
        System.Delete(sPath, idx, MaxInt);
      end else begin
        sSub := '';
      end;
      sPath := GetRealPath(sPath);
      if sPath = '' then Exit;
    end else begin
      sSub  := '';
    end;
    if (sSub <> '') and {$IFDEF UNICODE}CharInSet(sSub[Length(sSub)],{$ELSE}(sSub[Length(sSub)] in{$ENDIF} ['/', '\']) then begin
      System.Delete(sSub, Length(sSub), 1);
    end;
    pEnumZipFiles(Node, PChar(sPath), PChar(sSub), ZipFileCallBack, bFindFirst);
  end;
end;

procedure TSkyDirFileTree.GetNodeChild(Node: TTreeNode);
                         //取得“目录树”中某一结点下所有第一级子结点
  function GetFileExt(S: string): string;
  var
    idx: Integer;
  begin
    idx := LastDelimiter('.', S);
    if idx > 0 then begin
      Result := Copy(S, idx, MaxInt);
    end else begin
      Result := '';
    end;
  end;

  procedure AddZipIcons(AImages: TCustomImageList; const sPath: string; idxZip: Integer);
  var
    idx: Integer;
    icoZip: TIcon;
  begin
    icoZip := TIcon.Create;
    try
      idx := 0;
      while idx <= idxZip do begin
        if idx + SKY_ZIP_IMG_IDX_START >= AImages.Count then begin
          icoZip.Handle := GetFileSmallIcon(sPath + 'a' + GetFileExt(FZippers.ZipperDef[idx]));
          AImages.AddIcon(icoZip);
        end;
        Inc(idx);
      end;
    finally
      icoZip.Free;
    end;
  end;

var
  sPath: String;
  srTemp: TSearchRec;
  i, idx, iTemp, idxZip: Integer;
  pImages: TCustomImageList;
  ndParent: TTreeNode;
begin
  if (Node.ImageIndex < 1)               //不是 目录(/驱动器) 结点则退出
      or (Node.ImageIndex >= SKY_FILE_IMG_IDX)
         and (Node.ImageIndex < SKY_ZIP_IMG_IDX_START) then   Exit;

  if Assigned(FOnUpdateNode) then
    FOnUpdateNode(Self);
  sPath  := GetFullPath(Node);
  Items.BeginUpdate;                     //如果不加BeginUpdate,则D5下极慢!
  try
    if (Node.ImageIndex >= SKY_ZIP_IMG_IDX_START) or ParentIsZip(Node, ndParent) then begin
      //处理压缩文件
      DoEnumZipFiles(Node, ndParent, sPath, False);
      //Node.CustomSort(CustomSortProc, 0);  //XP下搜索到超过1000个记录时速度极慢 ??!!
    end else if FindFirst(sPath + '*.*', faAnyFile, srTemp) = 0 then begin
      repeat
        idx := 0;
        if ShouldShow(srTemp.Attr) then begin
          if (srTemp.Attr and faDirectory) <> 0 then begin  //目录
            if (srTemp.Name <> '.') and (srTemp.Name <> '..') then
              idx := SKY_DIR_IMG_IDX;
          end else if (sdfFindZip in FOptions) and IsMatchZipFile(srTemp.Name, idxZip) then begin
            idx := SKY_ZIP_IMG_IDX_START + idxZip;
            pImages := Inherited Images;
            if Assigned(pImages) and (idx >= pImages.Count) then begin
              AddZipIcons(pImages, sPath, idxZip);
            end;
          end else if sdfFindFile in FOptions then begin    //文件
            if FslFileTypes.Find('*.*', iTemp) or IsMatchFileType(srTemp.Name) then
              idx := SKY_FILE_IMG_IDX;
          end;
          if idx <> 0 then begin
            //用AddChildFirst而不用AddChild是因搜索到的目录一般逆序出现
            {with Items.AddChildFirst(Node, srTemp.Name) do begin
              ImageIndex := idx;
              if idx = SKY_DIR_IMG_IDX then Inc(idx);
              SelectedIndex := idx;
            end;}
            if idx = SKY_DIR_IMG_IDX then begin
              FDirList.Add(srTemp.Name);
            end else begin
              FFileList.AddObject(srTemp.Name, TObject(idx));
            end;
          end;
        end;
      until FindNext(srTemp) <> 0;
      FindClose(srTemp);
    end;
    // 2008.1.26 改用TStringList排序,以加快速度(搜索结果超过1000时加速几十倍)
    if FDirList.Count > 0 then begin
      FDirList.Sort;
      for i := 0 to FDirList.Count -1 do begin
        with Items.AddChild(Node, FDirList[i]) do begin
          ImageIndex := SKY_DIR_IMG_IDX;
          SelectedIndex := SKY_DIR_SEL_IDX;
        end;
        FImgIdxList.Add(Pointer(SKY_DIR_IMG_IDX))
      end;
      FDirList.Clear;
    end;
    if FFileList.Count > 0 then begin
      FFileList.Sort;
      for i := 0 to FFileList.Count -1 do begin
        idx := Integer(FFileList.Objects[i]);
        with Items.AddChild(Node, FFileList[i]) do begin
          ImageIndex := idx;
          SelectedIndex := idx;
        end;
        FImgIdxList.Add(Pointer(idx));
      end;
      FFileList.Clear;
    end;
    //Node.CustomSort(CustomSortProc, 0); -> XP下搜索到超过1000个记录时速度极慢
  finally
    Items.EndUpdate;
  end;
  //Node.Collapse(True); //折叠
end;

procedure TSkyDirFileTree.GetNodeFirstChild(Node: TTreeNode);
var                    //获得结点之下的第一个子目录或文件
  sPath: String;
  srTemp: TSearchRec;
  idx, iTemp, idxZip: Integer;
  ndParent: TTreeNode;
begin                  //本函数应在GetNodeChild()后执行
  sPath := GetFullPath(Node);
  if (Node.ImageIndex >= SKY_ZIP_IMG_IDX_START) or ParentIsZip(Node, ndParent) then begin
    //处理压缩文件
    DoEnumZipFiles(Node, ndParent, sPath, True);
  end else if FindFirst(sPath + '*.*', faAnyFile, srTemp) = 0 then begin
    repeat
      idx := 0;
      if ShouldShow(srTemp.Attr) then begin
        if (srTemp.Attr and faDirectory) <> 0 then begin   //目录
          if (srTemp.Name <> '.') and (srTemp.Name <> '..') then
            idx := -1;
        end else if (sdfFindZip in FOptions) and IsMatchZipFile(srTemp.Name, idxZip) then begin
          idx := -1;
        end else if sdfFindFile in FOptions then begin     //文件
          if FslFileTypes.Find('*.*', iTemp) or IsMatchFileType(srTemp.Name) then
            idx := -1;
        end;
        if idx <> 0 then begin //下面用AddChildFirst而不用AddChild是因为搜索到的目录一般逆序出现
          with Items.AddChildFirst(Node, '') do
            ImageIndex  := idx;
        end;
      end;
    until (idx <> 0) or (FindNext(srTemp) <> 0);
    FindClose(srTemp);
  end;
  Node.Collapse(True);   //折叠起来
end;

procedure TSkyDirFileTree.InitDriveNodes(bFirstLoading: Boolean);
var                      //初始化各磁盘结点
  dwDrives:         DWORD;
  cDrive:           Char;
  dwDriveType:      DWORD;
  dwSerialNum:      DWORD;
  iImgIdx:          Integer;
  Node:             TTreeNode;
  sNodeName:        string;
begin
  FRootNode.DeleteChildren;
  dwDrives := Windows.GetLogicalDrives();
  cDrive   := 'A';
  while dwDrives > 0 do
  begin
    if (dwDrives and 1) > 0 then
    begin
      dwDriveType := GetDriveType(PChar(cDrive + ':\'));
      sNodeName   := GetDriveNodeName(cDrive,
                                      dwDriveType,
                                      dwSerialNum,
                                      iImgIdx,
                                      bFirstLoading
                                     );
      if sNodeName <> '' then
      begin
        Node := Items.AddChildObject(FRootNode,
                                     Format('%s (%s:)', [sNodeName, cDrive]),
                                     Pointer(dwSerialNum));
        Node.ImageIndex    := iImgIdx;
        Node.SelectedIndex := iImgIdx;
        if dwSerialNum <> 0 then  //有子目录(除可移动驱动器与光驱可能无盘片外)
          if FbQuickSearch then
          begin
            with Items.AddChildFirst(Node, '') do
              ImageIndex := -1;
          end
          else
            GetNodeFirstChild(Node);
      end;
    end;
    Inc(cDrive);
    dwDrives := dwDrives shr 1;   //右移一位，取下一个盘符标志
  end;
  FRootNode.Collapse(True);
  FRootNode.Expand(False);
end;

procedure TSkyDirFileTree.UpdateDirNode(Node: TTreeNode);
var
  i, imgIdx: Integer;
  //ANode: TTreeNode;
begin
  if (Node.Count = 0) or (Node.Item[0].ImageIndex < 0) then
  begin
    FImgIdxList.Clear;
    GetNodeChild(Node);
    Node.Expand(False);
    if Node.Count <> 0 then
      Application.ProcessMessages();       //可改善闪动状况
    Items.BeginUpdate; //不加BeginUpdate,则D5下极慢!  2005.11.6
    try
      for i := 0 to Node.Count - 1 do
      begin
        //ANode := Node.Item[i];       2008.1.26 这个取Item在结点多时速度也很慢
        imgIdx := Integer(FImgIdxList[i]);
        if (imgIdx = SKY_DIR_IMG_IDX) or (imgIdx >= SKY_ZIP_IMG_IDX_START) then
        begin
          if FbQuickSearch then        //快速搜索
          begin
            with Items.AddChildFirst(Node.Item[i], '') do
              ImageIndex := -1;
          end
          else
            GetNodeFirstChild(Node.Item[i]);  //取得子目录及文件
        end
        else if not(sdfFindZip in Options) then   //是文件结点则退出，因此后全是文件结点
          Break;                       //2005.3.24   因为所有结点已排好序
      end;
    finally
      Items.EndUpdate;
      FImgIdxList.Clear;
    end;
  end;
end;

procedure TSkyDirFileTree.UpdateDriveNode(Node: TTreeNode);
var
  sRootDir:    String{[4]};  //用String[4]在BCB下可能会出现问题 2007.11.14
  dwDriveType: DWORD;
  dwSerialNum: DWORD;
  sNodeName,
  sTip:        String;
  bError:      Boolean;
  bHandled:    Boolean;
  iImgIdx:     Integer;
begin
  sRootDir    := 'A:\';
  sRootDir[1] := Node.Text[Length(Node.Text) - 2];
  dwDriveType := Windows.GetDriveType(PChar(@sRootDir[1]));
  dwSerialNum := 0;
  sNodeName   := GetDriveNodeName(sRootDir[1], dwDriveType, dwSerialNum,
                                  iImgIdx, false);

  bError := (sNodeName = '');
  if bError then
    sTip := Format(rsDriveNotFind, [sRootDir])
  else
  begin
    if dwSerialNum = 0 then   //未取到序列号,可认为GetVolumeInformation: False
    begin
      case(dwDriveType)of     //测试磁盘类型
        DRIVE_REMOVABLE:      //2 可移动驱动器（常为软驱）
          if sRootDir[1] < 'C' then
            sTip := rsNoFloppyTip
          else
            sTip := rsNoRemoveTip;
        DRIVE_REMOTE:         //4 网络驱动器
          sTip := rsNoRemoteTip;
        DRIVE_CDROM:          //5 光驱
          sTip := rsNoCdromTip;
      end;
      bError := True;
    end;
    if dwSerialNum <> DWORD(Node.Data)then
    begin
      Node.Data := Pointer(dwSerialNum);
      Node.Text := Format('%s (%s:)', [sNodeName, sRootDir[1]]);
      Node.DeleteChildren();
    end;
    if (dwSerialNum <> 0) then
      UpdateDirNode(Node);
  end;
  if bError then
  begin
    Selected := FLastNode;
    if sTip <> '' then
    begin
      FNowNode := FLastNode;
      bHandled := False;
      if Assigned(FOnError) then         //可以由调用者确定是否提示出错
        FOnError(Self, sTip, bHandled);  //也可改变提示信息
      if not bHandled then               //如果未处理
        raise Exception.Create(sTip);    //那就抛出异常
    end;
  end;
end;

procedure TSkyDirFileTree.UpdateTreeNode(Node: TTreeNode);
begin                     //2009.10.29 在某些情况下(可能是点的太快)会出错?!
  if (Node <> nil) and ((Node.ImageIndex < SKY_FILE_IMG_IDX) or (Node.ImageIndex >= SKY_ZIP_IMG_IDX_START)) then
  begin                                  //文件结点就不搜了
    if Node = FRootNode then
      InitDriveNodes()           //根结点
    else if (Node.Level = 1) and (Node.Parent = FRootNode) then
      UpdateDriveNode(Node)      //驱动器结点
    else
      UpdateDirNode(Node);       //Level > 1 or Remote Node  目录/网络路径结点
  end;
end;

procedure TSkyDirFileTree.DeleteSelectedNode;  //删除选中结点
begin
  if Selected <> nil then
    Items.Delete(Selected);
end;

procedure TSkyDirFileTree.UpdateSelectedNode;  //更新选中结点（当前目录）
begin
  if Selected <> nil then
  begin
    Selected.DeleteChildren;
    UpdateTreeNode(Selected);
  end;
end;

function TSkyDirFileTree.GetSelectNodeText: String;
begin                 //取当前选择结点文本
  if Selected <> nil then
    Result := Selected.Text
  else
    Result := '';
end;

procedure TSkyDirFileTree.SetSelectNodeText(const Value: String);
begin                 //设置当前选择结点文本
  if Selected <> nil then
    Selected.Text := Value;
end;

function TSkyDirFileTree.GetSelectNodeFullPath: String;
begin                 //取当前选择结点全路径名（目录全路径名）
  if Selected <> nil then
    Result := GetFullPath(Selected)
  else
    Result := '';
end;

function TSkyDirFileTree.GetAllNodeCount: Integer;   //所有结点数
begin
  Result := Items.Count;
end;

function TSkyDirFileTree.GetNodeChildCount(Node: TTreeNode): Integer;
begin                  //指定结点第一级子结点数
  if Node <> nil then
    Result := Node.Count
  else
    Result := -1;
end;

function TSkyDirFileTree.GetNodeAllChildrenCount(Node: TTreeNode): Integer;
                       //指定结点所有子结点数
  function GetAllChildsCount(ANode: TTreeNode): Integer;
  var                  //用递归统计当前结点的所有子结点个数
    i: Integer;
  begin
    Result := 0;
    for i:=0 to ANode.Count-1 do
    begin
      if (ANode.Item[i].ImageIndex = SKY_FILE_IMG_IDX) and not(sdfFindZip in Options) then
        Break;         //遇到文件结点就不用再统计下去了（因已排好序）
      Inc(Result, GetAllChildsCount(ANode.Item[i]));
    end;
    Inc(Result, ANode.Count);
  end;

begin
  if Node <> nil then
  begin
    if Node = FRootNode then
      Result := GetAllNodeCount() - 1
    else
      Result := GetAllChildsCount(Node);
  end
  else
    Result := -1;
end;

function TSkyDirFileTree.GetSelectNodeChildCount: Integer;
begin                  //当前结点第一级子结点数
  Result := GetNodeChildCount(Selected);
end;

function TSkyDirFileTree.GetSelectNodeAllChildrenCount: Integer;
begin                  //当前结点所有子结点数
  Result := GetNodeAllChildrenCount(Selected);
end;

function TSkyDirFileTree.NodeIsDir(Node: TTreeNode): Boolean;
begin                  //指定结点是目录???
  Result := (Node <> nil) and (Node.ImageIndex > 0)
            and (Node.ImageIndex <= SKY_DIR_IMG_IDX);
end;

function TSkyDirFileTree.NodeIsFile(Node: TTreeNode): Boolean;
begin                  //指定结点是文件(包含压缩文件!!!)???
  Result := (Node <> nil) and (Node.ImageIndex >= SKY_FILE_IMG_IDX);
end;

function TSkyDirFileTree.NodeIsPureFile(Node: TTreeNode): Boolean;
begin
  Result := (Node <> nil) and (Node.ImageIndex = SKY_FILE_IMG_IDX);
end;

function TSkyDirFileTree.NodeIsZip(Node: TTreeNode): Boolean;
begin                  //指定结点是压缩文件???
  Result := (Node <> nil) and (Node.ImageIndex >= SKY_ZIP_IMG_IDX_START);
end;

function TSkyDirFileTree.NodeIsZipChild(Node: TTreeNode): Boolean;
var
  ndParent: TTreeNode;
begin
  Result := ParentIsZip(Node, ndParent);
end;

function TSkyDirFileTree.SelectedNodeIsDir: Boolean;  //选中结点是目录???
begin
  Result := NodeIsDir(Selected);
end;

function TSkyDirFileTree.SelectedNodeIsFile: Boolean; //选中结点是文件???
begin
  Result := NodeIsFile(Selected);
end;

function TSkyDirFileTree.SelectedNodeIsPureFile: Boolean;
begin
  Result := NodeIsPureFile(Selected);
end;

function TSkyDirFileTree.SelectedNodeIsZip: Boolean;  //选中结点是压缩文件???
begin
  Result := NodeIsZip(Selected);
end;

function TSkyDirFileTree.SelectedNodeIsZipChild: Boolean;
var
  ndParent: TTreeNode;
begin
  Result := ParentIsZip(Selected, ndParent);
end;
{
function TSkyDirFileTree.GetRootName: String;
begin
  Result := FRootName;
end;}

procedure TSkyDirFileTree.SetRootName(const Value: String);
begin
  if FRootName <> Value then
  begin
    FRootName := Value;
    if FRootNode <> nil then
      FRootNode.Text := Value;
  end;
end;

procedure TSkyDirFileTree.UpdateOfInitPath(const sPath: String);
                       //直接初始化到某一路径/文件（只支持绝对路径）

  function FindNodeOfChildren(Node: TTreeNode; const sText: String): TTreeNode;
  var
    i: Integer;
  begin
    Result := nil;
    for i:=0 to Node.Count-1 do
      if AnsiSameText(Node.Item[i].Text, sText) then
      begin
        Result := Node.Item[i];
        Break;
      end;
  end;

  function UpdateOfInitDrive(cDrive: Char): Char;
  var
    i: Integer;
  begin
    Result := #0;
    with FRootNode do
      for i:=0 to Count-1 do
        if Item[i].Text[Length(Item[i].Text) - 2] = cDrive then
        begin
          Self.Selected := Item[i];
          FNowNode      := Item[i];
          Item[i].DeleteChildren();
          UpdateDriveNode(Item[i]);
          Result := cDrive;
          Break;
        end;
  end;

  procedure UpdateOfInitRemotePath(const sRmtRoot: string);
  var
    i: Integer;
  begin
    for i := 1 to Items.Count -1 do begin  //Items[0] = FRootNode
      if AnsiSameText(sRmtRoot, Items[i].Text) then begin
        Self.Selected := Items[i];
        FNowNode      := Items[i];
        Items[i].DeleteChildren;
        UpdateDirNode(Items[i]);
        Exit;
      end;
    end;
    FNowNode := Self.Items.Add(nil, sRmtRoot);
    FNowNode.ImageIndex := SKY_REMOTEDRV_IMG_IDX;
    FNowNode.SelectedIndex := SKY_REMOTEDRV_IMG_IDX;
    Selected := FNowNode;
    UpdateDirNode(FNowNode);
  end;

  function FirstDelimiter(const Delimiter, S: string; vStartIdx: Integer = 1): Integer;
  var
    i: Integer;
  begin
    for i := vStartIdx to Length(S) do begin
      if Pos(S[i], Delimiter) > 0 then begin
        Result := i;
        Exit;
      end;
    end;
    Result := 0;
  end;

var
  sDrive,
  sTemp,
  sDirText:  String;
  CurrNode:  TTreeNode;
  idx:       Integer;
  cNowDrive: Char;
  sRmtRoot:  string;
begin
  if not(csDesigning in ComponentState) then begin
    InitZippers;
  end;
  sDrive := ExtractFileDrive(sPath);
  if (sDrive <> '') then begin
    if {$IFDEF UNICODE}CharInSet(sDrive[1],{$ELSE}(sDrive[1] in{$ENDIF} ['a'..'z', 'A'..'Z']) then begin
      cNowDrive := UpdateOfInitDrive(sDrive[1]);
      if cNowDrive <> sDrive[1] then Exit; //若初始路径不存在，则不再执行下面的
      CurrNode := FNowNode;
      sTemp    := sPath;
      System.Delete(sTemp, 1, 3); //对于如 C:\ 开始的有效串,先删掉起始驱动器部分
    end else begin     // (sPath[1] = PathDelim) and (sPath[2] = PathDelim) !!
      sRmtRoot := sDrive;         //因不能对网络计算机使用FindFirst..
      sTemp    := sPath;          //故将第一级共享目录当根目录处理 testing..
      System.Delete(sTemp, 1, Length(sDrive) +1);
      UpdateOfInitRemotePath(sRmtRoot);
      CurrNode := FNowNode;
    end;
    while sTemp <> '' do
    begin
      idx := FirstDelimiter('\/|', sTemp);
      if idx > 0 then
      begin
        sDirText := Copy(sTemp, 1, idx - 1);
        System.Delete(sTemp, 1, idx);
      end
      else
      begin
        sDirText := sTemp;
        sTemp    := '';
      end;
      if CurrNode <> FRootNode then
      begin                   //如果是目录结点，则继续
        CurrNode  := FindNodeOfChildren(CurrNode, sDirText);
        if CurrNode <> nil then
        begin
          if (CurrNode.ImageIndex < SKY_FILE_IMG_IDX) or (CurrNode.ImageIndex >= SKY_ZIP_IMG_IDX_START) then
          begin                           //是目录结点而不是文件结点 或是压缩文件结点
            CurrNode.DeleteChildren;      //删掉那个可能存在的空子结点
            UpdateTreeNode(CurrNode);     //继续更新下一级子目录
          end;
          Selected := CurrNode;
        end
        else if sTemp <> '' then
          sTemp := '';
      end
      else if sTemp <> '' then            //对于正常路径（文件名在最后）实不需要
        sTemp := '';
    end;
  end;
end;

function TSkyDirFileTree.GetImages: TCustomImageList;
begin
  if Inherited Images <> FDefTVImages then
    Result := Inherited Images
  else
    Result := nil;
end;

procedure TSkyDirFileTree.SetImages(Value: TCustomImageList);
begin
  if Value <> nil then
    Inherited Images := Value
  else
    Inherited Images := FDefTVImages;
end;

function TSkyDirFileTree.NodeIsNotRoot(Node: TTreeNode): Boolean;
begin
  Result := Node <> FRootNode;
end;

function TSkyDirFileTree.SelectedIsNotRoot: Boolean; //当前所选结点不是根结点?
begin
  Result := Selected <> FRootNode;
end;

{$IFDEF DELPHI6UP}
function TSkyDirFileTree.GetFileTpDelimiter: Char;
begin
  Result := FslFileTypes.Delimiter;
end;

procedure TSkyDirFileTree.SetFileTpDelimiter(Value: Char);
begin
  if Value <> FslFileTypes.Delimiter then
    FslFileTypes.Delimiter := Value;
end;
{$ENDIF}

function TSkyDirFileTree.GetFileTypes: String;
{$IFNDEF DELPHI6UP}
const
  sLineBreak = #13#10;  //D5 还不支持Linux呢
{$ENDIF}
begin
  Result := StringReplace(FFileType, sLineBreak, FileTpDelimiter,
                          [rfReplaceAll]);
  if (Result <> '') and (Result[Length(Result)] = FileTpDelimiter) then
    System.Delete(Result, Length(Result), 1);
end;

procedure TSkyDirFileTree.SetFileTypes(const Value: String);
begin
  if (Value <> '') and  not SameText(Value, FFileType{FslFileTypes.Text}) then
  begin
    {$IFDEF DELPHI6UP}
    FslFileTypes.DelimitedText := Value;
    {$ELSE}                        //Delphi5
    FslFileTypes.Text := AnsiLowerCase(StringReplace(Value,
                                                     FFileTpDelimiter,
                                                     #13#10,
                                                     [rfReplaceAll]
                                      ));
    {$ENDIF}
    FFileType := Value;
  end;
end;

procedure TSkyDirFileTree.SetBoldFontOfDir(Value: Boolean);
begin                     //对D5,由于IsCustomDrawn不可重载,故需有自画Item事件
  if FBoldFontOfDir <> Value then      //BoldFontOfDir 才能起作用 2005.11.5
  begin
    FBoldFontOfDir := Value;
    Repaint;
  end;
end;

procedure TSkyDirFileTree.SetZipperPath(const Value: string);
begin
  if (FZipperPath <> Value) or (Value = '') then begin
    FZipperPath := Value;
    if Assigned(FZippers) then FZippers.ZipperPath := Value;
  end;
end;

procedure TSkyDirFileTree.SetUnZipDir(const Value: string);
begin
  if FUnZipDir <> Value then begin
    FUnZipDir := IncludeTrailingBackslash(Value);
    if Assigned(FZippers) then FZippers.UnZipDir := Value;
  end;
end;

{$IFDEF VER130}
procedure TSkyDirFileTree.CMRecreateWnd(var Message: TMessage);
begin
  if FColorChanging then begin
    FColorChanging := False;
  end else begin
    inherited;
  end;
end;

procedure TSkyDirFileTree.CMColorChanged(var Message: TMessage);
begin
  FColorChanging := True;
  inherited;   //错误:D5里color变化时竟调用了RecreateWnd??!! 2007.10.19
  TreeView_SetBkColor(Handle, ColorToRGB(Color));
end;
{$ENDIF}

procedure TSkyDirFileTree.InitZippers;
begin
  if FZippers = nil then begin
    FZippers := TZippers.Create(Self);
    FZippers.ZipperPath := FZipperPath;
    FZippers.UnZipDir   := FUnZipDir;
  end else if FZippers.Count = 0 then begin
    FZippers.ZipperPath := FZipperPath;
  end;
end;

procedure TSkyDirFileTree.GetZipFile(Node: TTreeNode; sZipDir, sSub: string;
  bFindFirst, bIsDir: Boolean; var bHandled: Boolean);
var
  idx, iImgIdx{, iSelIdx}: Integer;
begin
  if bIsDir then begin
    iImgIdx := SKY_DIR_IMG_IDX;
    //iSelIdx := SKY_DIR_IMG_IDX +1;
  end else if IsMatchZipFile(sSub, idx) then begin
    iImgIdx := SKY_ZIP_IMG_IDX_START + idx;
    //iSelIdx := iImgIdx;
  end else if (sdfFindFile in Options) and IsMatchFileType(sSub) then begin
    iImgIdx := SKY_FILE_IMG_IDX;
    //iSelIdx := SKY_FILE_IMG_IDX;
  end else begin
    Exit;
  end;
  if bFindFirst then begin
    with TSkyDirFileTree(Node.TreeView).Items.AddChildFirst(Node, '') do begin;
      ImageIndex := -1;
    end;
  end else begin
    sSub := ExtractFileNameAny(sSub);
    {with TSkyDirFileTree(Node.TreeView).Items.AddChildFirst(Node, sSub) do begin
      ImageIndex := iImgIdx;
      SelectedIndex := iSelIdx;
    end;
    FImgIdxList.Add(Pointer(iImgIdx)); }
    if bIsDir then begin
      FDirList.Add(sSub);
    end else begin
      FFileList.AddObject(sSub, TObject(iImgIdx));
    end;
  end;
  bHandled := True;
end;

procedure TSkyDirFileTree.SortZipperByDllName(const sNameOrders: string);
begin
  if sNameOrders <> '' then begin
    InitZippers;
    FZippers.SortByDllName(sNameOrders);
  end;
end;

initialization
  {$IFDEF MULTILANGUAGE}
  InitResourceStrs;
  {$ENDIF}

end.
