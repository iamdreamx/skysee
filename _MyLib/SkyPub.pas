unit SkyPub;

interface

uses
  Windows, SysUtils, Classes;

  function MatchStrings(const Source,Pattern: string; bNoCase: Boolean):Boolean;
  function MatchFileName(const Source,Pattern: string; bNoCase:Boolean):Boolean;
  function ExtractFileNameAny(const FileName: string): string;
  function GetSysTempDir: string;

implementation

function MatchStrings(const Source, Pattern: string; bNoCase: Boolean): Boolean;
         //MatchStrings - 字符串的模糊匹配，支持 ? 和 *   2005.11.13 搜集
  function MatchPattern(element, pattern: PChar): Boolean;
  const
    ALPHA = ['a'..'z', 'A'..'Z'];
  begin
    if 0 = StrComp(pattern, '*') then
      Result := True
    else if element^ = #0 then
      Result := (pattern^ = #0)
    else
    begin
      case pattern^ of
        '*':
          if MatchPattern(element, @pattern[1]) then
            Result := True
          else
            Result := MatchPattern(@element[1], pattern);
        '?': Result := MatchPattern(@element[1], @pattern[1]);
      else
        if (element^ = pattern^)
            or bNoCase and {$IFDEF UNICODE}CharInSet(element^, ALPHA) and CharInSet(pattern^, ALPHA){$ELSE}(element^ in ALPHA) and (pattern^ in ALPHA){$ENDIF}
                and (Byte(element^)and $df = Byte(pattern^) and $df) then
          Result := MatchPattern(@element[1], @pattern[1])
        else
          Result := False;
      end;
    end;
  end;
begin
  Result := MatchPattern(PChar(Source), PChar(Pattern));
end;

function MatchFileName(const Source,Pattern: string; bNoCase: Boolean):Boolean;
begin                     //模糊匹配文件名 2005.11.26
  if (Pattern <> '') and (Pattern[Length(Pattern)] = '.') then
  begin
    if Pattern = '*.' then            //表示无扩展名
      Result := Pos('.', Source) = 0
    else if (Pos('.', Pattern) = Length(Pattern)) and (Pos('.', Source) > 0)then
      Result := False                 //处理 Pattern = f*. 的情况
    else
      Result := MatchStrings(PChar(Source + '.'), PChar(Pattern), bNoCase);
  end
  else
    Result := MatchStrings(Source, Pattern, bNoCase);
end;

function ExtractFileNameAny(const FileName: string): string;
var                       //可处理Windows/Linux目录分隔符的 ExtractFileName
  I: Integer;
begin
  I := LastDelimiter('\/|' + DriveDelim, FileName);
  Result := Copy(FileName, I + 1, MaxInt);
end;

function GetSysTempDir: string;
var
  dwSize: DWord;
begin
  dwSize := GetTempPath(0, nil);
  SetLength(Result, dwSize);
  dwSize := GetTempPath(dwSize, PChar(Result));
  //Result := TrimRight(Result);  //真奇怪! 在Debug下与单独运行执行结果不一样?!
  //System.Delete(Result, dwSize +1, MaxInt);
  SetLength(Result, dwSize);
end;

end.
