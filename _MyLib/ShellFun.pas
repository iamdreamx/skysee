unit ShellFun;

interface

uses Windows, Classes, ShellApi;

    function ShowDirFileProperties(sName: String; FWnd: HWND; bShowFile: boolean = true):Boolean;

implementation

function ShowDirFileProperties(sName: String; FWnd: HWND; bShowFile: boolean):Boolean;
var                 //调用 目录/文件 属性对话框
  sfi: TSHELLEXECUTEINFO;
begin
  with sfi do
  begin
    cbSize := SizeOf(sfi);
    if bShowFile then
      lpFile := PChar(sName)
    else
      lpFile := nil;
    Wnd := FWnd;
    fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_INVOKEIDLIST or SEE_MASK_FLAG_NO_UI;
    lpVerb := PChar('properties');
    lpIDList := nil;
    if bShowFile then
      lpDirectory := nil
    else
      lpDirectory := PChar(sName);
    nShow := 0;
    hInstApp := 0;
    lpParameters := nil;
    dwHotKey := 0;
    hIcon := 0;
    hkeyClass := 0;
    hProcess := 0;
    lpClass := nil;
  end;
  Result := ShellExecuteEX(@sfi);
end;

end.
 