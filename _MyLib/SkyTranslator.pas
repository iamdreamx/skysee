unit SkyTranslator;

interface

uses
  Windows, SysUtils, Classes, Forms, StdCtrls, Menus, ActnList, ValEdit,
  Graphics, IniFiles,
  HashTable;

type
  TSkyTranslator = class
  private
    FHash: THashTable;
    FLanguageName: string;
    FLanguageID:   LANGID;
    FFontCharset: TFontCharset;
    FFontName:    string;
    FFontSize:    Integer;
  public
    constructor Create(Size: Integer = 2048);
    destructor Destroy; override;

    procedure InitFromFile(const FileName: string; ReadMsgs: Boolean = True);
    procedure Translate(Form: TForm); overload;
    function Translate(const Key, Default: string): string; overload;
    
    property LanguageName: string read FLanguageName;
    property LanguageID: LANGID read FLanguageID;
    property FontCharset: TFontCharset read FFontCharset;
    property FontName: string read FFontName;
    property FontSize: Integer read FFontSize;
  end;

  TSkyTranslateManager = class
  private
  public
  end;

  TSkyGenerator = class
  private
    FMaps: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function Scan(Form: TForm): TForm;
    procedure SaveToFile(FileName: string);
  end;

procedure InitTranslator(const TranslateDir: string);

procedure Translate(Form: TForm); overload;
function Translate(const Key: string): string; overload;
function Translate(const Key, Default: string): string; overload;

implementation

var
  Translator: TSkyTranslator;

{ Public Functions }

procedure InitTranslator(const TranslateDir: string);

  procedure CreateCurrentTranslator(const FileName: string);
  begin
    if Translator = nil then begin
      Translator := TSkyTranslator.Create();
    end;
    Translator.InitFromFile(FileName);
  end;

var
  dir: string;
  sr: TSearchRec;
  tmpTranslator: TSkyTranslator;
  curLangID: LANGID;
  defFile: string;
begin
  dir := TranslateDir;
  if (dir <> '') and (dir[Length(dir)] <> SysUtils.PathDelim) then begin
    dir := dir + SysUtils.PathDelim;
  end;
  if FindFirst(dir + '*.skl', faAnyFile, sr) = 0 then try
    curLangID := GetSystemDefaultLangID();
    tmpTranslator := TSkyTranslator.Create(0);
    try
      repeat
        tmpTranslator.InitFromFile(dir + sr.Name, False);
        if curLangID = tmpTranslator.LanguageID then begin
          CreateCurrentTranslator(dir + sr.Name);  // find local language file
          Exit;
        end else if tmpTranslator.LanguageID = 0 then begin
          defFile := dir + sr.Name;
        end;
      until FindNext(sr) <> 0;
    finally
      tmpTranslator.Free;
    end;
    if defFile <> '' then begin
      CreateCurrentTranslator(defFile);  // find default language file
    end;
  finally
    FindClose(sr);
  end;
end;

procedure Translate(Form: TForm);
begin
  if Assigned(Translator) then begin
    Translator.Translate(Form);
  end;
end;

function Translate(const Key: string): string;
begin
  Result := Translate(Key, Key);
end;

function Translate(const Key, Default: string): string;
begin
  if Key = '' then begin
    Result := '';
  end else if Assigned(Translator) then begin
    Result := Translator.Translate(Key, Default);
  end else begin
    Result := Default;
  end;
end;

{ constants }

const
  cHintTail = '.Hint';

{ Internal Functions }

// 因为IniFile处理值时会将前后空格去掉，所以用\s表示空格 2012.12.24
// convert #13 => '\r', #10 => '\n', '\' => '\\', ' ' => '\s'
function MyFmtValStr(const S: string): string;

  function GetTransferChar(C: Char): Char;
  begin
    case C of
      #10: Result := 'n';
      #13: Result := 'r';
      ' ': Result := 's';
    else   Result := C;
    end;
  end;

var
  i: Integer;
  m: Integer;
  k: Integer;
begin
  for i := 1 to Length(S) do begin
    if {$IFDEF UNICODE}CharInSet(S[i], [#10, #13, '\', ' ']){$ELSE}S[i] in [#10, #13, '\', ' ']{$ENDIF} then begin
      SetLength(Result, i -1 + (Length(S) - i +1)* 2);
      CopyMemory(@Result[1], @S[1], (i -1)* SizeOf(Char));
      Result[i] := '\';
      m := i +1;
      Result[m] := GetTransferChar(S[i]);
      k := m +1;
      while m <= Length(S) do begin
        if {$IFDEF UNICODE}CharInSet(S[m], [#10, #13, '\', ' ']){$ELSE}S[m] in [#10, #13, '\', ' ']{$ENDIF} then begin
          Result[k] := '\';
          Inc(k);
          Result[k] := GetTransferChar(S[m]);
          Inc(m);
          Inc(k);
        end else begin
          Result[k] := S[m];
          Inc(m);
          Inc(k);
        end;
      end;
      SetLength(Result, k -1);
      Exit;
    end;
  end;
  Result := S;
end;

// convert '\r' => #13, '\n' => #10, '\\' => '\', '\s' => ' '
function MyRealValStr(const S: string): string;

  function GetRealChar(C: Char): Char;
  begin
    case C of
      'n': Result := #10;
      'r': Result := #13;
      's': Result := ' ';
    else   Result := C;
    end;
  end;

var
  i: Integer;
  m: Integer;
  k: Integer;
begin
  Result := S;
  for i := 1 to Length(S) -1 do begin
    if (S[i] = '\') and {$IFDEF UNICODE}CharInSet(S[i +1],{$ELSE}(S[i +1] in{$ENDIF} ['n', 'r', '\', 's']) then begin
      Result[i] := GetRealChar(S[i +1]);
      m := i + 2;
      k := i + 1;
      while m < Length(S) do begin
        if (S[m] = '\') and {$IFDEF UNICODE}CharInSet(S[m +1],{$ELSE}(S[m +1] in{$ENDIF} ['n', 'r', '\', 's']) then begin
          Result[k] := GetRealChar(S[m +1]);
          Inc(m);
        end else begin
          Result[k] := S[m];
        end;
        Inc(m);
        Inc(k);
      end;
      if m = Length(S) then begin
        Result[k] := S[m];
      end else begin
        Dec(k);
      end;
      SetLength(Result, k);
      Exit;
    end;
  end;
end;

// convert  ###ddddd#ddddd#123Abc# => #XX123Abc#
{$IFDEF UNICODE}
function ConvertUnicodeToStr(const S: string): string;
var
  i: Integer;
  m: Integer;
  len: Integer;
  t: Integer;
  ch: SmallInt;
begin
  i := 1;
  m := 1;
  len := Length(S);
  SetLength(Result, len);
  while (i <= len) do begin
    if (i < len) and (S[i] = '#') then begin
      t := i +1;
      ch := 0;
      while (t <= len) and {$IFDEF UNICODE}CharInSet(S[t],{$ELSE}(S[t] in{$ENDIF} ['0'..'9']) do begin
        ch := ch * 10 + Ord(S[t]) - Ord('0');
        Inc(t);
      end;
      Result[m] := Char(ch); //?? Unicode Char
      i := t -1;
    end else begin
      Result[m] := S[i];
    end;
    Inc(i);
    Inc(m);
  end;
  Result := S;
end;
{$ENDIF}

{ TSkyTranslator }

constructor TSkyTranslator.Create(Size: Integer);
begin
  FHash := THashTable.Create(Size);
  FFontCharset := DEFAULT_CHARSET;
end;

destructor TSkyTranslator.Destroy;
begin
  FHash.Free;
  inherited;
end;

procedure TSkyTranslator.InitFromFile(const FileName: string;
  ReadMsgs: Boolean);
const
  cLangSection = 'LangOptions';
  cMsgSection  = 'Messages';
var
  s: string;
  charset: Integer;
  sections: TStrings;
  i: Integer;
begin
  with TMemIniFile.Create(FileName) do try
    // read language options
    {$IFDEF UNICODE}
    Self.FLanguageName := ConvertUnicodeToStr(ReadString(cLangSection, 'LanguageUniName', 'Unknow'));
    {$ELSE}
    Self.FLanguageName := ReadString(cLangSection, 'LanguageName', 'Unknow');
    {$ENDIF}
    Self.FLanguageID := StrToIntDef(ReadString(cLangSection, 'LanguageID', ''), 0);
    s := ReadString(cLangSection, 'FontCharset', '');
    if (s <> '') and Graphics.IdentToCharset(s, charset) then begin
      Self.FFontCharset := charset;
    end else begin
      Self.FFontCharset := DEFAULT_CHARSET;
    end;
    Self.FFontName := ReadString(cLangSection, 'FontName', '');
    Self.FFontSize := ReadInteger(cLangSection, 'FontSize', 0);

    // read translate messages
    FHash.Clear;
    if ReadMsgs then begin
      sections := TStringList.Create;
      try
        ReadSection(cMsgSection, sections);
        for i := 0 to sections.Count -1 do begin
          Self.FHash.ValueOf[sections[i]] := ReadString(cMsgSection, sections[i], '');
        end;
      finally
        sections.Free;
      end;
    end;
  finally
    Free;
  end;
end;

procedure TSkyTranslator.Translate(Form: TForm);

  procedure SetFont(Font: TFont);
  begin
    if Self.FontName <> '' then begin
      Font.Charset := Self.FontCharset;
      Font.Name := Self.FontName;
      Font.Size := Self.FontSize;
    end;
  end;

  procedure TranslateComponent(Component: TComponent);
  var
    k: Integer;
    ac: TAction;
    lb: TLabel;
    mi: TMenuItem;
    bt: TButton;
    gb: TGroupBox;
    cb: TCheckBox;
    ve: TValueListEditor;
  begin
    if Component is TAction then begin
      ac := TAction(Component);
      if (ac.Tag >= 0) and (ac.Caption <> '') then begin
        ac.Caption := Translate(ac.Name, ac.Caption);
        if ac.Hint <> '' then begin
          ac.Hint := Translate(ac.Name + cHintTail, ac.Hint);
        end;
      end;
    end else if Component is TLabel then begin
      lb := TLabel(Component);
      if (lb.Tag >= 0) and (lb.Caption <> '') then begin
        lb.Caption := MyRealValStr(Translate(lb.Name, lb.Caption));
        if lb.Hint <> '' then begin
          lb.Hint := Translate(lb.Name + cHintTail, lb.Hint);
        end;
        if not lb.ParentFont then SetFont(lb.Font);
      end;
    end else if Component is TMenuItem then begin
      mi := TMenuItem(Component);
      if (mi.Action = nil) and (mi.Caption <> '') and (mi.Caption <> '-') then begin
        mi.Caption := Translate(mi.Name, mi.Caption);
        if mi.Hint <> '' then begin
          mi.Hint := Translate(mi.Name + cHintTail, mi.Hint);
        end;
      end;
    end else if Component is TButton then begin
      bt := TButton(Component);
      if (bt.Action = nil) and (bt.Caption <> '') then begin
        bt.Caption := Translate(bt.Caption, bt.Caption); //!! not use bt.Name
        if bt.Hint <> '' then begin
          bt.Hint := Translate(bt.Hint, bt.Hint);        //!! not use bt.Name+'.Hint'
        end;
        if not bt.ParentFont then SetFont(bt.Font);
      end;
    end else if Component is TGroupBox then begin
      gb := TGroupBox(Component);
      if (gb.Caption <> '') then begin
        gb.Caption := MyRealValStr(Translate(gb.Name, gb.Caption));
        if gb.Hint <> '' then begin
          gb.Hint := Translate(gb.Name + cHintTail, gb.Hint);
        end;
        if not gb.ParentFont then SetFont(gb.Font);
      end;
    end else if Component is TCheckBox then begin
      cb := TCheckBox(Component);
      if (cb.Tag >= 0) and (cb.Caption <> '') then begin
        cb.Caption := Translate(cb.Name, cb.Caption);
        if cb.Hint <> '' then begin
          cb.Hint := Translate(cb.Name + cHintTail, cb.Hint);
        end;
      end;
    end else if Component is TValueListEditor then begin
      ve := TValueListEditor(Component);
      for k := 0 to ve.TitleCaptions.Count -1 do begin
        if ve.TitleCaptions[k] <> '' then begin
          ve.TitleCaptions[k] := Translate(ve.TitleCaptions[k], ve.TitleCaptions[k]); //!!
        end;
      end;
      if not ve.ParentFont then SetFont(ve.Font);
    end;
  end;

var
  i: Integer;
  k: Integer;
  frmName: string;
  frame: TFrame;
begin
  SetFont(Form.Font);
  frmName := Copy(Form.ClassName, 2, MaxInt);
  Form.Caption := Translate(frmName, Form.Caption);
  for i := 0 to Form.ComponentCount -1 do begin
    if Form.Components[i] is TFrame then begin
      frame := TFrame(Form.Components[i]);
      for k := 0 to frame.ComponentCount -1 do begin
        TranslateComponent(frame.Components[k]);
      end;
    end else begin
      TranslateComponent(Form.Components[i]);
    end;
  end;
end;

function TSkyTranslator.Translate(const Key, Default: string): string;
begin
  Result := Self.FHash.ValueOf[Key];
  if Result = '' then begin
    Result := Default;
  end;
end;

{ TSkyGenerator }

constructor TSkyGenerator.Create;
begin
  FMaps := TStringList.Create;
end;

destructor TSkyGenerator.Destroy;
begin
  FMaps.Free;
  inherited;
end;

procedure TSkyGenerator.Clear;
begin
  FMaps.Clear;
end;

function TSkyGenerator.Scan(Form: TForm): TForm;

  procedure ScanComponent(Component: TComponent);
  var
    k: Integer;
    ac: TAction;
    lb: TLabel;
    mi: TMenuItem;
    bt: TButton;
    gb: TGroupBox;
    cb: TCheckBox;
    ve: TValueListEditor;
  begin
    if Component is TAction then begin
      ac := TAction(Component);
      if (ac.Tag >= 0) and (ac.Caption <> '') then begin
        FMaps.Values[ac.Name] := ac.Caption;
        if ac.Hint <> '' then begin
          FMaps.Values[ac.Name + cHintTail] := ac.Hint;
        end;
      end;
    end else if Component is TLabel then begin
      lb := TLabel(Component);
      if (lb.Tag >= 0) and (lb.Caption <> '') then begin
        FMaps.Values[lb.Name] := MyFmtValStr(lb.Caption);
        if lb.Hint <> '' then begin
          FMaps.Values[lb.Name + cHintTail] := lb.Hint;
        end;
      end;
    end else if Component is TMenuItem then begin
      mi := TMenuItem(Component);
      if (mi.Action = nil) and (mi.Caption <> '') and (mi.Caption <> '-') then begin
        FMaps.Values[mi.Name] := mi.Caption;
        if mi.Hint <> '' then begin
          FMaps.Values[mi.Name + cHintTail] := mi.Hint;
        end;
      end;
    end else if Component is TButton then begin
      bt := TButton(Component);
      if (bt.Action = nil) and (bt.Caption <> '') then begin
        FMaps.Values[bt.Caption] := bt.Caption; //!! not use bt.Name
        if bt.Hint <> '' then begin
          FMaps.Values[bt.Hint] := bt.Hint;     //!! not use bt.Name+'.Hint'
        end;
      end;
    end else if Component is TGroupBox then begin
      gb := TGroupBox(Component);
      if (gb.Caption <> '') then begin
        FMaps.Values[gb.Name] := MyFmtValStr(gb.Caption);
        if gb.Hint <> '' then begin
          FMaps.Values[gb.Name + cHintTail] := gb.Hint;
        end;
      end;
    end else if Component is TCheckBox then begin
      cb := TCheckBox(Component);
      if (cb.Tag >= 0) and (cb.Caption <> '') then begin
        FMaps.Values[cb.Name] := cb.Caption;
        if cb.Hint <> '' then begin
          FMaps.Values[cb.Name + cHintTail] := cb.Hint;
        end;
      end;
    end else if Component is TValueListEditor then begin
      ve := TValueListEditor(Component);
      for k := 0 to ve.TitleCaptions.Count -1 do begin
        if ve.TitleCaptions[k] <> '' then begin
          FMaps.Values[ve.TitleCaptions[k]] := ve.TitleCaptions[k]; //!!
        end;
      end;
    end;
  end;

var
  i: Integer;
  k: Integer;
  frmName: string;
  frame: TFrame;
begin
  Result := Form;
  frmName := Copy(Form.ClassName, 2, MaxInt);
  FMaps.Values[frmName] := Form.Caption;
  for i := 0 to Form.ComponentCount -1 do begin
    if Form.Components[i] is TFrame then begin
      frame := TFrame(Form.Components[i]);
      for k := 0 to frame.ComponentCount -1 do begin
        ScanComponent(frame.Components[k]);
      end;
    end else begin
      ScanComponent(Form.Components[i]);
    end;
  end;
end;

procedure TSkyGenerator.SaveToFile(FileName: string);
var
  i: Integer;
begin
  //使用TIniFile时，如果值尾部有空格，则每写一次会多加一个空格! 2012.12.22
  with TMemIniFile.Create(FileName) do try
    FMaps.Sort;
    for i := 0 to FMaps.Count -1 do begin
      WriteString('Messages', FMaps.Names[i], FMaps.ValueFromIndex[i]);
    end;
    UpdateFile;
  finally
    Free;
  end;
end;

{-----------------------------------------------------------------------------}

initialization

finalization
  Translator.Free;

end.
