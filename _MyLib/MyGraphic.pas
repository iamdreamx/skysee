unit MyGraphic;

interface

uses
  Windows, Graphics, SysUtils, Math, RGBHSV;

type
  TPercent = 1..100;

{ 注意：以下函数均为pf24bit编写! }  

//灰度化
procedure GrayScale(const Bitmap: TBitmap);
//模糊/柔化    todo. 注意：四条边未处理到 2011.5.20
procedure Blur(const SrcBmp: TBitmap);
//锐化
procedure Sharpen(const SrcBmp: TBitmap);
//反色，即：底片效果
procedure NotColor(const SrcBmp: TBitmap);
//曝光
procedure Exposure(const SrcBmp: TBitmap);
//浮雕
procedure Emboss(const SrcBmp: TBitmap);
//雕刻
procedure Engrave(const SrcBmp: TBitmap);
//颜色覆盖
procedure MergeColor(const Bmp: TBitmap; Color: TColor);
//图片遮罩
procedure CoverWithColor(const Bmp: TBitmap; Color: TColor; Percent: TPercent = 50);

//木刻
procedure Woodcut(const Bmp: TBitmap);
//水彩画, 与油画CanvasOut(RandomStep=1)时类似
procedure Watercolor(const Bmp: TBitmap);
//油画 (RandomStep = 2..5 为宜)
procedure CanvasOut(const Bmp: TBitmap; RandomStep: Integer = 2);
//油画, 使用缓存, 效果更好一些 (RandomStep = 2..5 为宜)
procedure CanvasOut2(const Bmp: TBitmap; RandomStep: Integer = 2);
//灯光, 即在指定位置显示一个光源的效果
procedure Light(const Bmp: TBitmap; x, y: Integer; m: Integer = 200; n: Integer = 1);
//明暗效果
procedure BrightDark(const Bmp: TBitmap; Ratio: Integer = 4);

//调整RGB
procedure AdjRGB(SrcBmp, DstBmp: TBitmap; RedVar, GreenVar, BlueVar: Integer);
//调整色调
procedure AdjHue(SrcBmp, DstBmp: TBitmap; AdjVar: Integer; ARange: Integer = 360);  //ARange = 240  --> in Windows
//调整亮度
procedure AdjBrightness(SrcBmp, DstBmp: TBitmap; AdjVar: Integer);
procedure AdjBrightness2(SrcBmp, DstBmp: TBitmap; AdjVar: Integer);
//调整饱和度
procedure AdjSaturation(SrcBmp, DstBmp: TBitmap; AdjVar: Integer);
//增强对比度
procedure EnhanceContrast(SrcBmp: TBitmap; AdjVal: Integer);
//减弱对比度
procedure WeakenContrast(SrcBmp: TBitmap; AdjVal: Integer);

//水平镜象
procedure HorzMirror(const Bmp: TBitmap);
//垂直镜象
procedure VertMirror(const Bmp: TBitmap);

//左旋90度(逆时针) 速度较快
procedure QuickLevorotation90(const Bmp: TBitmap);
//右旋90度(顺时针) 速度较快
procedure QuickDextrorotation90(const Bmp: TBitmap);
//右旋90度(顺时针)
procedure Dextrorotation90(const ObjBmp: TBitmap);
//旋转180度
procedure Rotate180(const Bmp: TBitmap);


implementation

{----------------------------------------------------------------------------}

//灰度化
procedure GrayScale(const Bitmap: TBitmap);
var
  X: Integer;
  Y: Integer;
  PRGB: pRGBTriple;
  Gray: Byte;
begin
  for Y := 0 to (Bitmap.Height - 1) do begin
    PRGB := Bitmap.ScanLine[Y];
    for X := 0 to (Bitmap.Width - 1) do begin
      Gray := Trunc(0.3 * PRGB^.rgbtRed + 0.59 * PRGB^.rgbtGreen + 0.11 * PRGB^.rgbtBlue);
      PRGB^.rgbtRed   := Gray;
      PRGB^.rgbtGreen := Gray;
      PRGB^.rgbtBlue  := Gray;
      Inc(PRGB);
    end;
  end;
end;

//模糊/柔化    todo. 注意：四条边未处理到 2011.5.20
procedure Blur(const SrcBmp: TBitmap);
var
  i, j: integer;
  Value: integer;
  SrcRow: pRGBTriple;
  SrcNextRow: pRGBTriple;
  SrcPreRow: pRGBTriple;
  lastPreRow: pRGBTriple;
  lastCurRow: pRGBTriple;
  lastNextRow: pRGBTriple;
  nextPreRow: pRGBTriple;
  nextCurRow: pRGBTriple;
  nextNextRow: pRGBTriple;
begin
  for i := 1 to SrcBmp.Height - 2 do begin
    SrcRow := SrcBmp.ScanLine[i];
    SrcPreRow := SrcBmp.ScanLine[i-1];
    SrcNextRow := SrcBmp.ScanLine[i+1];
    lastPreRow := SrcPreRow;
    lastCurRow := SrcRow;
    lastNextRow := SrcNextRow;
    Inc(SrcRow);
    Inc(SrcPreRow);
    Inc(SrcNextRow);
    for j := 1 to SrcBmp.Width - 2 do  begin
      // add brightness value to pixel's RGB values
      // RGB values must be less than 256
      nextPreRow := SrcPreRow;
      nextCurRow := SrcRow;
      nextNextRow := SrcNextRow;
      Inc(nextPreRow);
      Inc(nextCurRow);
      Inc(nextNextRow);

      Value:=(lastPreRow^.rgbtRed + lastCurRow^.rgbtRed + lastNextRow^.rgbtRed +
             SrcPreRow^.rgbtRed + SrcRow^.rgbtRed + SrcNextRow^.rgbtRed +
             nextPreRow^.rgbtRed + nextCurRow^.rgbtRed + nextNextRow^.rgbtRed) div 9;
      SrcRow^.rgbtRed := value;

      Value:=(lastPreRow^.rgbtGreen + lastCurRow^.rgbtGreen + lastNextRow^.rgbtGreen +
             SrcPreRow^.rgbtGreen + SrcRow^.rgbtGreen + SrcNextRow^.rgbtGreen +
             nextPreRow^.rgbtGreen + nextCurRow^.rgbtGreen + nextNextRow^.rgbtGreen) div 9;
      SrcRow^.rgbtGreen:=value;

      Value:=(lastPreRow^.rgbtBlue + lastCurRow^.rgbtBlue + lastNextRow^.rgbtBlue +
             SrcPreRow^.rgbtBlue + SrcRow^.rgbtBlue + SrcNextRow^.rgbtBlue +
             nextPreRow^.rgbtBlue + nextCurRow^.rgbtBlue + nextNextRow^.rgbtBlue) div 9;
      SrcRow^.rgbtBlue:=value;

      lastPreRow := SrcPreRow;
      lastCurRow := SrcRow;
      lastNextRow := SrcNextRow;
      Inc(SrcRow);
      Inc(SrcPreRow);
      Inc(SrcNextRow);
    end;
  end;
end;

//锐化
procedure Sharpen(const SrcBmp: TBitmap);
var
  i, j: integer;
  Value: integer;
  SrcRow: pRGBTriple;
  SrcPreRow: pRGBTriple;
begin
  for i := 1 to SrcBmp.Height - 1 do begin
    SrcRow := SrcBmp.ScanLine[i];
    SrcPreRow := SrcBmp.ScanLine[i-1];
    Inc(SrcRow);
    for j := 1 to SrcBmp.Width - 1 do begin
      // add brightness value to pixel's RGB values
      // RGB values must be less than 256
      Value := SrcRow^.rgbtRed + (SrcRow^.rgbtRed - SrcPreRow^.rgbtRed) div 2;
      if Value < 0 then Value := 0;
      if Value > 255 then Value := 255;
      SrcRow^.rgbtRed := value;

      Value:=SrcRow^.rgbtGreen + (SrcRow^.rgbtGreen - SrcPreRow^.rgbtGreen)div 2;
      if Value < 0 then Value := 0;
      if Value > 255 then Value := 255;
      SrcRow^.rgbtGreen := value;

      Value:=SrcRow^.rgbtBlue + (SrcRow^.rgbtBlue - SrcPreRow^.rgbtBlue)div 2;
      if Value < 0 then Value := 0;
      if Value > 255 then Value := 255;
      SrcRow^.rgbtBlue := value;

      Inc(SrcRow);
      Inc(SrcPreRow);
    end;
  end;
end;

//反色，即：底片效果
procedure NotColor(const SrcBmp: TBitmap);
var
  i, j: integer;
  SrcRow: pRGBTriple;
begin
  for i := 0 to SrcBmp.Height - 1 do begin
    SrcRow := SrcBmp.ScanLine[i];
    for j := 0 to SrcBmp.Width - 1 do  begin
      SrcRow^.rgbtRed   := not SrcRow^.rgbtRed;
      SrcRow^.rgbtGreen := not SrcRow^.rgbtGreen;
      SrcRow^.rgbtBlue  := not SrcRow^.rgbtBlue;
      Inc(SrcRow);
    end;
  end;
end;

//曝光
procedure Exposure(const SrcBmp: TBitmap);
var
  i, j: integer;
  SrcRow: pRGBTriple;
begin
  for i := 0 to SrcBmp.Height - 1 do begin
    SrcRow := SrcBmp.ScanLine[i];
    for j := 0 to SrcBmp.Width - 1 do  begin
      // add brightness value to pixel's RGB values
      // RGB values must be less than 256
      if SrcRow^.rgbtRed < 128 then
        SrcRow^.rgbtRed := not SrcRow^.rgbtRed ;

      if SrcRow^.rgbtGreen < 128 then
        SrcRow^.rgbtGreen := not SrcRow^.rgbtGreen;

      if SrcRow^.rgbtBlue < 128 then
        SrcRow^.rgbtBlue := not SrcRow^.rgbtBlue;

      Inc(SrcRow);
    end;
  end;
end;

//浮雕  (右下边作了特殊处理)
procedure Emboss(const SrcBmp: TBitmap);

  function CalcColorValue(OrgVal, RefVal: Byte): Integer;
  begin
    Result := OrgVal - RefVal + 128;
    if Result < 0 then Result := 0;
    if Result > 255 then Result := 255;
  end;

var
  curRow: pRGBTriple;
  nxtRow: pRGBTriple;

  procedure CalcColor;
  begin
    curRow^.rgbtRed := CalcColorValue(curRow^.rgbtRed, nxtRow^.rgbtRed);
    curRow^.rgbtGreen := CalcColorValue(curRow^.rgbtGreen, nxtRow^.rgbtGreen);
    curRow^.rgbtBlue := CalcColorValue(curRow^.rgbtBlue, nxtRow^.rgbtBlue);
  end;

var
  i, j: integer;
begin
  if (SrcBmp.Height < 2) or (SrcBmp.Width < 2) then Exit;

  for i := 0 to SrcBmp.Height - 2 do begin
    curRow := SrcBmp.ScanLine[i];
    nxtRow := SrcBmp.ScanLine[i+1];
    Inc(nxtRow);
    for j := 0 to SrcBmp.Width - 2 do begin
      // add brightness value to pixel's RGB values
      // RGB values must be less than 256
      CalcColor;
      Inc(curRow);
      Inc(nxtRow);
    end;
    Dec(nxtRow);
    CalcColor;    //计算最右边的点
  end;

  //计算最下边的行(用左上角的值回填，相当于对斜线取均值)
  curRow := SrcBmp.ScanLine[SrcBmp.Height -1];
  nxtRow := SrcBmp.ScanLine[SrcBmp.Height -2];
  curRow^ := nxtRow^;
  Inc(curRow);
  for j := 0 to SrcBmp.Width -1 do begin
    curRow^ := nxtRow^;
    Inc(curRow);
    Inc(nxtRow);
  end;
end;

//雕刻  (右下边作了特殊处理)
procedure Engrave(const SrcBmp: TBitmap);

  function CalcColorValue(OrgVal, RefVal: Byte): Integer;
  begin
    Result := OrgVal - RefVal + 128;
    if Result < 0 then Result := 0;
    if Result > 255 then Result := 255;
  end;

var
  curRow: pRGBTriple;
  nxtRow: pRGBTriple;

  procedure CalcColor;
  begin
    curRow^.rgbtRed := CalcColorValue(nxtRow^.rgbtRed, curRow^.rgbtRed);
    curRow^.rgbtGreen := CalcColorValue(nxtRow^.rgbtGreen, curRow^.rgbtGreen);
    curRow^.rgbtBlue := CalcColorValue(nxtRow^.rgbtBlue, curRow^.rgbtBlue);
  end;

var
  i, j: integer;
begin
  if (SrcBmp.Height < 2) or (SrcBmp.Width < 2) then Exit;

  for i := 0 to SrcBmp.Height - 2 do begin
    curRow := SrcBmp.ScanLine[i];
    nxtRow := SrcBmp.ScanLine[i+1];
    Inc(nxtRow);
    for j := 0 to SrcBmp.Width - 2 do begin
      // add brightness value to pixel's RGB values
      // RGB values must be less than 256
      CalcColor;
      Inc(curRow);
      Inc(nxtRow);
    end;
    Dec(nxtRow);
    CalcColor;    //计算最右边的点
  end;

  //计算最下边的行(用左上角的值回填，相当于对斜线取均值)
  curRow := SrcBmp.ScanLine[SrcBmp.Height -1];
  nxtRow := SrcBmp.ScanLine[SrcBmp.Height -2];
  curRow^ := nxtRow^;
  Inc(curRow);
  for j := 0 to SrcBmp.Width -1 do begin
    curRow^ := nxtRow^;
    Inc(curRow);
    Inc(nxtRow);
  end;
end;

//颜色覆盖
procedure MergeColor(const Bmp: TBitmap; Color: TColor);
var
  i, j: Integer;
  p: PRGBTriple;
  L: Cardinal;
  r, g, b: Byte;
begin
  //Bmp.PixelFormat := pf24bit;
  L := ColorToRGB(Color);
  r := GetRValue(L);
  g := GetGValue(L);
  b := GetBValue(L);
  for i := 0 to Bmp.Height - 1 do begin
    p := Bmp.ScanLine[i];
    for j := 0 to Bmp.Width - 1 do begin
      { 计算公式：目标色R/G/B值 = 255 - (255 - 灰色R/G/B值) * (255 - 指定色R/G/B值) / (255 - 最深色R/G/B值) }
      p^.rgbtRed   := 255 - (255 - p^.rgbtRed) * (255 - r) div 255;
      p^.rgbtGreen := 255 - (255 - p^.rgbtGreen) * (255 - g) div 255;
      p^.rgbtBlue  := 255 - (255 - p^.rgbtBlue) * (255 - b) div 255;
      Inc(p);
    end;
  end;
end;

//图片遮罩
procedure CoverWithColor(const Bmp: TBitmap; Color: TColor; Percent: TPercent = 50);
var
  i, j: Integer;
  p: PRGBTriple;
  L: Cardinal;
  r, g, b: Integer;
begin
  Percent := 100 - Percent;
  //Bmp.PixelFormat := pf24bit;
  L := ColorToRGB(Color);
  r := GetRValue(L);
  g := GetGValue(L);
  b := GetBValue(L);
  for i := 0 to Bmp.Height - 1 do begin
    p := Bmp.ScanLine[i];
    for j := 0 to Bmp.Width - 1 do begin
      // 计算公式：目标色R/G/B值 = 指定色R/G/B值 + (100 - 遮罩百分比) * (原色R/G/B值 - 指定色R/G/B值) div 100
      p^.rgbtBlue  := b + Percent * (p^.rgbtBlue - b) div 100;
      p^.rgbtGreen := g + Percent * (p^.rgbtGreen - g) div 100;
      p^.rgbtRed   := r + Percent * (p^.rgbtRed - r) div 100;
      Inc(p);
    end;
  end;
end;

//木刻
procedure Woodcut(const Bmp: TBitmap);
const
  cPixels: array[Boolean] of Byte = (255, 0);
var
  i, j: Integer;
  p: PRGBTriple;
  val: Integer;
begin
  for i := 0 to Bmp.Height -1 do begin
    p := Bmp.ScanLine[i];
    for j := 1 to Bmp.Width do begin
      val := Round((p^.rgbtBlue + p^.rgbtGreen + p^.rgbtRed) / 3);
      FillChar(p^, SizeOf(TRGBTriple), cPixels[val > 128]);
      Inc(p);
    end;
  end;
end;

//水彩画, 与油画CanvasOut(RandomStep=1)时类似
{.............................................................................
图像扩散(类似水彩画Watercolor的效果)算法
　　算法很简单，就是将当前点用周围的随即的点来代替。

A B C D
E F G H
I J K L
M N O P

　　F点可以从它周围的A,B,C,E,G,I,J,K中任意选一点代替。
　　G点可以从它周围的B,C,D,F,H,J,K,L中任意选一点代替。
　　J点可以从它周围的E,F,G,I,K,M,N,O中任意选一点代替。
　　K点可以从它周围的F,G,H,J,L,N,O,P中任意选一点代替。

　　至于选哪一点，可以用一个随即数来选定。
.............................................................................}
procedure Watercolor(const Bmp: TBitmap);
var
  i, j: Integer;
  curRow: PRGBTriple;
  preRow: PRGBTriple;
  floRow: PRGBTriple;
  lastCur: PRGBTriple;
  lastPre: PRGBTriple;
  lastFlo: PRGBTriple;
  nextCur: PRGBTriple;
  nextPre: PRGBTriple;
  nextFlo: PRGBTriple;
  pixelArray: array[0..7] of TRGBTriple;
  oldRow: PRGBTriple;
  newRow: PRGBTriple;
  rowLen: Integer;
begin
  Randomize;
  rowLen := SizeOf(TRGBTriple)* Bmp.Width;
  GetMem(oldRow, rowLen);
  GetMem(newRow, rowLen);
  try
    CopyMemory(oldRow, Bmp.ScanLine[0], rowLen);
    for i := 1 to Bmp.Height -2 do begin
      curRow := Bmp.ScanLine[i];
      CopyMemory(newRow, curRow, rowLen);
      preRow := oldRow; //Bmp.ScanLine[i -1];
      floRow := Bmp.ScanLine[i +1];
      lastCur := curRow;
      lastPre := preRow;
      lastFlo := floRow;
      Inc(curRow);
      Inc(preRow);
      Inc(floRow);
      nextCur := curRow;
      nextPre := preRow;
      nextFlo := floRow;
      Inc(nextCur);
      Inc(nextPre);
      Inc(nextFlo);
      for j := 1 to Bmp.Width - 2 do begin
        pixelArray[0] := lastPre^;
        pixelArray[1] := preRow^;
        pixelArray[2] := nextPre^;
        pixelArray[3] := lastCur^;
        pixelArray[4] := nextCur^;
        pixelArray[5] := lastFlo^;
        pixelArray[6] := floRow^;
        pixelArray[7] := nextFlo^;
        curRow^ := pixelArray[Random(1000000) mod 8];
        lastCur := curRow;
        lastPre := preRow;
        lastFlo := floRow;
        Inc(curRow);
        Inc(preRow);
        Inc(floRow);
        Inc(nextCur);
        Inc(nextPre);
        Inc(nextFlo);
      end;
      CopyMemory(oldRow, newRow, rowLen);
    end;
  finally
    FreeMem(newRow);
    FreeMem(oldRow);
  end;
end;

//油画 (RandomStep = 2..5 为宜)
procedure CanvasOut(const Bmp: TBitmap; RandomStep: Integer = 2);
var
  range: Integer;

  function GetTargetOffset(Offset, MaxVal: Integer): Integer;
  begin
    Result := (Random(1000000) mod range) - RandomStep;
    if Result >= 0 then begin
      Inc(Result, Offset + 1);
      if Result > MaxVal then Result := MaxVal;
    end else begin
      Inc(Result, Offset);
      if Result < 0 then Result := 0;
    end;
  end;

var
  i, j: Integer;
  c, r: Integer;
  curRow, nxtRow: PRGBTriple;
begin
  if (Bmp.Height < 5) or (Bmp.Width < 5) then Exit;

  range := RandomStep * 2;
  Randomize;
  for i := 0 to Bmp.Height - 1 do begin
    curRow := Bmp.ScanLine[i];
    for j := 0 to Bmp.Width - 1 do begin
      c := GetTargetOffset(j, Bmp.Width - 1);
      r := GetTargetOffset(i, Bmp.Height - 1);
      nxtRow := Bmp.ScanLine[r];
      Inc(nxtRow, c);
      curRow^ := nxtRow^;
      Inc(curRow);
    end;
  end;
end;

//油画, 使用缓存, 效果更好一些 (RandomStep = 2..5 为宜)
procedure CanvasOut2(const Bmp: TBitmap; RandomStep: Integer = 2);
var
  range: Integer;

  function GetTargetOffset(Offset, MaxVal: Integer): Integer;
  begin
    Result := (Random(1000000) mod range) - RandomStep;
    if Result >= 0 then begin
      Inc(Result, Offset + 1);
      if Result > MaxVal then Result := MaxVal;
    end else begin
      Inc(Result, Offset);
      if Result < 0 then Result := 0;
    end;
  end;

var
  i, j: Integer;
  c, r: Integer;
  curRow, nxtRow: PRGBTriple;
  tmpMap: TBitmap;
begin
  if (Bmp.Height < 5) or (Bmp.Width < 5) then Exit;

  range := RandomStep * 2;
  Randomize;
  tmpMap := TBitmap.Create;
  try
    tmpMap.Assign(Bmp);
    for i := 0 to Bmp.Height - 1 do begin
      curRow := Bmp.ScanLine[i];
      for j := 0 to Bmp.Width - 1 do begin
        c := GetTargetOffset(j, Bmp.Width - 1);
        r := GetTargetOffset(i, Bmp.Height - 1);
        nxtRow := tmpMap.ScanLine[r];
        Inc(nxtRow, c);
        curRow^ := nxtRow^;
        Inc(curRow);
      end;
    end;
  finally
    tmpMap.Free;
  end;
end;

//灯光, 即在指定位置显示一个光源的效果
procedure Light(const Bmp: TBitmap; x, y: Integer; m: Integer = 200; n: Integer = 1);

  function TrimOfByte(Value: Integer): Integer;
  begin
    if Value < 0 then begin
      Result := 0;
    end else if Value > 255 then begin
      Result := 255;
    end else begin
      Result := Value;
    end;
  end;

var               //x,y为灯光光源坐标, m,n为调节参数
  i, j: Integer;
  curRow: PRGBTriple;
  adj: Extended;
begin
  for i := 0 to Bmp.Height -1 do begin
    curRow := Bmp.ScanLine[i];
    for j := 0 to Bmp.Width -1 do begin
      adj := Sqrt((x - j)*(x - j) + (y - i)*(y - i));
      if adj - y < 0 then begin
        adj := m * (1 - (adj + n)/y);
        curRow.rgbtRed   := TrimOfByte(Round(curRow.rgbtRed + adj));
        curRow.rgbtGreen := TrimOfByte(Round(curRow.rgbtGreen + adj));
        curRow.rgbtBlue  := TrimOfByte(Round(curRow.rgbtBlue + adj));
      end;
      Inc(curRow);
    end;
  end;
end;

//明暗效果
procedure BrightDark(const Bmp: TBitmap; Ratio: Integer);
var
  i, j: Integer;
  p: PRGBTriple;
begin
  for i := 0 to Bmp.Height -1 do begin
    p := Bmp.ScanLine[i];
    for j := 0 to Bmp.Width -1 do begin
      p^.rgbtRed  := Round(p^.rgbtRed / Ratio);
      p^.rgbtGreen:= Round(p^.rgbtGreen / Ratio);
      p^.rgbtBlue := Round(p^.rgbtBlue / Ratio);
      Inc(p);
    end;
  end;
end;

{----------------------------------------------------------------------------}

//调整RGB
procedure AdjRGB(SrcBmp, DstBmp: TBitmap; RedVar, GreenVar, BlueVar: Integer);
var
  i, j: Integer;
  srcRow: PRGBTriple;
  dstRow: PRGBTriple;
begin
  for i := 0 to SrcBmp.Height -1 do begin
    srcRow := SrcBmp.ScanLine[i];
    dstRow := DstBmp.ScanLine[i];
    for j := 0 to SrcBmp.Width -1 do begin
      if RedVar >= 0 then begin
        dstRow^.rgbtRed   := Min(255, srcRow^.rgbtRed + RedVar);
      end else begin
        dstRow^.rgbtRed   := Max(0, srcRow^.rgbtRed + RedVar);
      end;
      if GreenVar >= 0 then begin
        dstRow^.rgbtGreen := Min(255, srcRow^.rgbtGreen + GreenVar);
      end else begin
        dstRow^.rgbtGreen := Max(0, srcRow^.rgbtGreen + GreenVar);
      end;
      if blueVar >= 0 then begin
        dstRow^.rgbtBlue  := Min(255, srcRow^.rgbtBlue + BlueVar);
      end else begin
        dstRow^.rgbtBlue  := Max(0, srcRow^.rgbtBlue + BlueVar);
      end;
      Inc(srcRow);
      Inc(dstRow);
    end;
  end;
end;

//调整色调
procedure AdjHue(SrcBmp, DstBmp: TBitmap; AdjVar, ARange: Integer);
var
  i, j: Integer;
  srcRow: PRGBTriple;
  dstRow: PRGBTriple;
  hue: Single;
  r, g, b, h, s, v: Single;
begin
  if AdjVar < 0 then begin
    hue := 0;
  end else if AdjVar > ARange then begin
    hue := 1;
  end else begin
    hue := AdjVar / ARange;
  end;
  for i := 0 to SrcBmp.Height -1 do begin
    srcRow := SrcBmp.ScanLine[i];
    dstRow := DstBmp.ScanLine[i];
    for j := 0 to SrcBmp.Width -1 do begin
      RGBToHSV(srcRow^.rgbtRed, srcRow^.rgbtGreen, srcRow^.rgbtBlue, h, s, v);
      HSVToRGB({h + }hue, s, v, r, g, b);
      dstRow^.rgbtRed   := Round(r);
      dstRow^.rgbtGreen := Round(g);
      dstRow^.rgbtBlue  := Round(b);
      Inc(srcRow);
      Inc(dstRow);
    end;
  end;
end;

//调整亮度
procedure AdjBrightness(SrcBmp, DstBmp: TBitmap; AdjVar: Integer);
var
  i, j: Integer;
  srcRow: PRGBTriple;
  dstRow: PRGBTriple;
begin
  for i := 0 to SrcBmp.Height -1 do begin
    srcRow := SrcBmp.ScanLine[i];
    dstRow := DstBmp.ScanLine[i];
    for j := 0 to SrcBmp.Width -1 do begin
      if AdjVar >= 0 then begin
        dstRow^.rgbtRed   := Min(255, srcRow^.rgbtRed + AdjVar);
        dstRow^.rgbtGreen := Min(255, srcRow^.rgbtGreen + AdjVar);
        dstRow^.rgbtBlue  := Min(255, srcRow^.rgbtBlue + AdjVar);
      end else begin
        dstRow^.rgbtRed   := Max(0, srcRow^.rgbtRed + AdjVar);
        dstRow^.rgbtGreen := Max(0, srcRow^.rgbtGreen + AdjVar);
        dstRow^.rgbtBlue  := Max(0, srcRow^.rgbtBlue + AdjVar);
      end;
      Inc(srcRow);
      Inc(dstRow);
    end;
  end;
end;

{..............................................................................
  以下公式据说来自于PhotoShop：   (2012.9.19)
if (value >= 0)
  RGB = RGB + (255 - RGB) * value / 255;
else
  RGB = RGB + RGB * value / 255;
..............................................................................}
procedure AdjBrightness2(SrcBmp, DstBmp: TBitmap; AdjVar: Integer);
var
  i, j: Integer;
  srcRow: PRGBTriple;
  dstRow: PRGBTriple;
begin
  for i := 0 to SrcBmp.Height -1 do begin
    srcRow := SrcBmp.ScanLine[i];
    dstRow := DstBmp.ScanLine[i];
    for j := 0 to SrcBmp.Width -1 do begin
      if AdjVar >= 0 then begin
        dstRow^.rgbtRed   := srcRow^.rgbtRed + Round((255 - srcRow^.rgbtRed) * AdjVar / 255);
        dstRow^.rgbtGreen := srcRow^.rgbtGreen + Round((255 - srcRow^.rgbtGreen) * AdjVar / 255);
        dstRow^.rgbtBlue  := srcRow^.rgbtBlue + Round((255 - srcRow^.rgbtBlue) * AdjVar / 255);
      end else begin
        dstRow^.rgbtRed   := srcRow^.rgbtRed + Round(srcRow^.rgbtRed * AdjVar / 255);
        dstRow^.rgbtGreen := srcRow^.rgbtGreen + Round(srcRow^.rgbtGreen * AdjVar / 255);
        dstRow^.rgbtBlue  := srcRow^.rgbtBlue + Round(srcRow^.rgbtBlue * AdjVar / 255);
      end;
      Inc(srcRow);
      Inc(dstRow);
    end;
  end;
end;

//调整饱和度
procedure AdjSaturation(SrcBmp, DstBmp: TBitmap; AdjVar: Integer);
var
  Grays: array[0..767] of Integer;
  Alpha: array[0..255] of Word;
  Gray, X, Y: Integer;
  srcRow: PRGBTriple;
  dstRow: PRGBTriple;
  I: Byte;
begin
  for I := 0 to 255 do begin
    Alpha[I] := (I * AdjVar) shr 8;
  end;
  X := 0;
  for I := 0 to 255 do begin
    Gray := I - Alpha[I];
    Grays[X] := Gray;
    Inc(X);
    Grays[X] := Gray;
    Inc(X);
    Grays[X] := Gray;
    Inc(X);
  end;

  for Y := 0 to SrcBmp.Height - 1 do begin
    srcRow := SrcBmp.ScanLine[Y];
    dstRow := DstBmp.ScanLine[Y];
    for X := 0 to SrcBmp.Width - 1 do begin
      Gray := Grays[srcRow^.rgbtRed + srcRow^.rgbtGreen + srcRow^.rgbtBlue];
      dstRow^.rgbtRed   := Gray + Alpha[srcRow^.rgbtRed];
      dstRow^.rgbtGreen := Gray + Alpha[srcRow^.rgbtGreen];
      dstRow^.rgbtBlue  := Gray + Alpha[srcRow^.rgbtBlue];
      Inc(srcRow);
      Inc(dstRow);
    end;
  end;
end;

//增强对比度
procedure EnhanceContrast(SrcBmp: TBitmap; AdjVal: Integer);
var
  X, Y: Integer;
  SrcRow: PRGBTriple;
begin
  for Y := 0 to SrcBmp.Height - 1 do begin
    SrcRow := SrcBmp.ScanLine[Y];
    for X := 0 to SrcBmp.Width - 1 do begin
      if SrcRow^.rgbtRed >= 128 then begin
        SrcRow^.rgbtRed := Min(255, SrcRow^.rgbtRed + AdjVal);
      end else begin
        SrcRow^.rgbtRed := Max(0, SrcRow^.rgbtRed - AdjVal);
      end;
      if SrcRow^.rgbtGreen >= 128 then begin
        SrcRow^.rgbtGreen := Min(255, SrcRow^.rgbtGreen + AdjVal);
      end else begin
        SrcRow^.rgbtGreen := Max(0, SrcRow^.rgbtGreen - AdjVal);
      end;
      if SrcRow^.rgbtBlue >= 128 then begin
        SrcRow^.rgbtBlue := Min(255, SrcRow^.rgbtBlue + AdjVal);
      end else begin
        SrcRow^.rgbtBlue := Max(0, SrcRow^.rgbtBlue - AdjVal);
      end;
      Inc(SrcRow);
    end;
  end;
end;

//减弱对比度
procedure WeakenContrast(SrcBmp: TBitmap; AdjVal: Integer);
var
  X, Y: Integer;
  SrcRow: PRGBTriple;
begin
  for Y := 0 to SrcBmp.Height - 1 do begin
    SrcRow := SrcBmp.ScanLine[Y];
    for X := 0 to SrcBmp.Width - 1 do begin
      if SrcRow^.rgbtRed >= 128 then begin
        SrcRow^.rgbtRed := Max(128, SrcRow^.rgbtRed - AdjVal);
      end else begin
        SrcRow^.rgbtRed := Min(128, SrcRow^.rgbtRed + AdjVal);
      end;
      if SrcRow^.rgbtGreen >= 128 then begin
        SrcRow^.rgbtGreen := Max(128, SrcRow^.rgbtGreen - AdjVal);
      end else begin
        SrcRow^.rgbtGreen := Min(128, SrcRow^.rgbtGreen + AdjVal);
      end;
      if SrcRow^.rgbtBlue >= 128 then begin
        SrcRow^.rgbtBlue := Max(128, SrcRow^.rgbtBlue - AdjVal);
      end else begin
        SrcRow^.rgbtBlue := Min(128, SrcRow^.rgbtBlue + AdjVal);
      end;
      Inc(SrcRow);
    end;
  end;
end;

//水平镜象
procedure HorzMirror(const Bmp: TBitmap);
var
  i, j: Integer;
  rowLeft: PRGBTriple;
  rowRight: PRGBTriple;
  tmp: TRGBTriple;
begin
  for i := 0 to Bmp.Height -1 do begin
    rowLeft := Bmp.ScanLine[i];
    rowRight := rowLeft;
    Inc(rowRight, Bmp.Width -1);
    for j := 0 to (Bmp.Width div 2) -1 do begin
      tmp := rowLeft^;
      rowLeft^ := rowRight^;
      rowRight^ := tmp;
      Inc(rowLeft);
      Dec(rowRight);
    end;
  end;
end;

//垂直镜象
procedure VertMirror(const Bmp: TBitmap);
var
  i: Integer;
  len: Integer;
  row1: PRGBTriple;
  row2: PRGBTriple;
  temp: array of Byte;
  p: PByte;
begin
  len := Bmp.Width * SizeOf(TRGBTriple);
  SetLength(temp, len);
  p := PByte(temp);
  for i := 0 to (Bmp.Height div 2) -1 do begin
    row1 := Bmp.ScanLine[i];
    row2 := Bmp.ScanLine[Bmp.Height -1 - i];
    CopyMemory(p, row1, len);
    CopyMemory(row1, row2, len);
    CopyMemory(row2, p, len);
  end;
end;

//左旋90度(逆时针) 速度较快
procedure QuickLevorotation90(const Bmp: TBitmap);
var
  nIdx, nOfs,
  x, y, i, nMultiplier: integer;
  nMemWidth, nMemHeight, nMemSize, nScanLineSize: LongInt;
  aScnLnBuffer: PByteArray;
  aScanLine: PByteArray;
begin
  nMultiplier := 3; //只处理24bit位图
  nMemWidth := Bmp.Height;
  nMemHeight := Bmp.Width;
  nMemSize := nMemWidth * nMemHeight * nMultiplier;
  GetMem(aScnLnBuffer, nMemSize);
  try
    nScanLineSize := Bmp.Width * nMultiplier;
    GetMem(aScanLine, nScanLineSize);
    try
      for y := 0 to Bmp.Height - 1 do begin
        Move(Bmp.ScanLine[y]^, aScanLine^, nScanLineSize);
        for x := 0 to Bmp.Width - 1 do begin
          nIdx := ((Bmp.Width - 1) - x) * nMultiplier;
          nOfs := (x * nMemWidth * nMultiplier) + (y * nMultiplier);
          for i := 0 to nMultiplier - 1 do begin
            Byte(aScnLnBuffer[nOfs + i]) := Byte(aScanLine[nIdx + i]);
          end;
        end;
      end;
      Bmp.Height := nMemHeight;
      Bmp.Width := nMemWidth;
      for y := 0 to nMemHeight - 1 do begin
        nOfs := y * nMemWidth * nMultiplier;
        Move((@(aScnLnBuffer[nOfs]))^, Bmp.ScanLine[y]^, nMemWidth * nMultiplier);
      end;
    finally
      FreeMem(aScanLine, nScanLineSize);
    end;
  finally
    FreeMem(aScnLnBuffer, nMemSize);
  end;
end;

//右旋90度(顺时针) 速度较快
procedure QuickDextrorotation90(const Bmp: TBitmap);
var
  nIdx, nOfs, nMultiplier: integer;
  x, y, i: Integer;
  nMemWidth, nMemHeight, nMemSize,nScanLineSize: LongInt;
  aScnLnBuffer: PByteArray;
  aScanLine: PByteArray;
begin
  nMultiplier := 3; //only for 24bit //GetPixelSize(Bmp);
  nMemWidth := Bmp.Height;
  nMemHeight := Bmp.Width;
  nMemSize := nMemWidth * nMemHeight * nMultiplier;
  GetMem(aScnLnBuffer, nMemSize);
  try
    nScanLineSize := Bmp.Width * nMultiplier;
    GetMem(aScanLine, nScanLineSize);
    try
      for y := 0 to Bmp.Height-1 do begin
        Move(Bmp.ScanLine[y]^, aScanLine^, nScanLineSize);
        for x := 0 to Bmp.Width-1 do begin
          nIdx := x * nMultiplier;
          nOfs := (x * nMemWidth * nMultiplier) +(((Bmp.Height - 1) - y) * nMultiplier);
          for i := 0 to nMultiplier-1 do
            Byte(aScnLnBuffer[nOfs + i]) := aScanLine[nIdx+i];
        end;
      end;
      Bmp.Height := nMemHeight;
      Bmp.Width := nMemWidth;
      for y := 0 to nMemHeight-1 do begin
        nOfs := y * nMemWidth * nMultiplier;
        Move((@(aScnLnBuffer[nOfs]))^, Bmp.ScanLine[y]^, nMemWidth * nMultiplier);
      end;
    finally
      FreeMem(aScanLine, nScanLineSize);
    end;
  finally
    FreeMem(aScnLnBuffer, nMemSize);
  end;
end;

//右旋90度(顺时针)
procedure Dextrorotation90(const ObjBmp: TBitmap);
var
  i, j:Integer;
  rowIn, rowOut: pRGBTriple;
  Bmp: TBitmap;
  Width, Height: Integer;
begin
  Bmp:=TBitmap.Create;
  try
    Bmp.Assign(ObjBmp);
    ObjBmp.Width := Bmp.Height;
    ObjBmp.Height := Bmp.Width;
    Width := Bmp.Width -1;
    Height := Bmp.Height -1;
    for j := 0 to Height do begin
      rowIn  := Bmp.ScanLine[j];
      for i := 0 to Width do begin
        rowOut := ObjBmp.ScanLine[i];
        Inc(rowOut, Height-j);
        rowOut^ := rowIn^;
        Inc(rowIn);
      end;
    end;
  finally
    Bmp.Free;
  end;
end;

//旋转180度
procedure Rotate180(const Bmp: TBitmap);
var
  i, j: Integer;
  width: Integer;
  row1, row2: pRGBTriple;
  tmp: TRGBTriple;
begin
  for i := 0 to (Bmp.Height -1) div 2 do begin
    row1 := Bmp.ScanLine[i];
    row2 := Bmp.ScanLine[Bmp.Height - 1 - i];
    if row1 = row2 then begin
      width := (Bmp.Width div 2) -1;  //对奇数行来说，中间一行需要特别处理
    end else begin
      width := Bmp.Width -1;
    end;
    Inc(row2, Bmp.Width -1);
    for j := 0 to width do  begin
      tmp := row2^;
      row2^ := row1^;
      row1^ := tmp;
      Inc(row1);
      Dec(row2);
    end;
  end;
end;

end.
