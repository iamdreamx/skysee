unit MultiFileProps;

{ Copyright Kingron 2005 }
{ For Windows 2000 or Higher, Shell32.dll version 5.0 or later }

interface

uses
  Classes, Windows, SysUtils, ActiveX, ShlObj, ComObj;

function ShowFileProperties(Files: TStrings; aWnd: HWND): Boolean;

implementation

//function SHMultiFileProperties(pDataObj: IDataObject; Flag: DWORD): HRESULT; stdcall; external 'shell32.dll';
type
  TShMultiFilePropsFun = function(pDataObj: IDataObject; Flag: DWORD): HRESULT; stdcall;

function GetFileListDataObject(Files: TStrings; aWnd: HWND): IDataObject;
type
  PArrayOfPItemIDList = ^TArrayOfPItemIDList;
  TArrayOfPItemIDList = array[0..0] of PItemIDList;
var
  Malloc: IMalloc;
  Root: IShellFolder;
  p: PArrayOfPItemIDList;
  chEaten, dwAttributes: ULONG;
  i, FileCount: Integer;
begin
  Result := nil;
  FileCount := Files.Count;
  if FileCount = 0 then Exit;

  OleCheck(SHGetMalloc(Malloc));
  OleCheck(SHGetDesktopFolder(Root));
  p := AllocMem(SizeOf(PItemIDList) * FileCount);
  try
    for i := 0 to FileCount - 1 do
    try
      if not (DirectoryExists(Files[i]) or FileExists(Files[i])) then Continue;
      OleCheck(Root.ParseDisplayName(aWnd, //GetActiveWindow,
        nil,
        PWideChar(WideString(Files[i])),
        chEaten,
        p^[i],
        dwAttributes));
    except
    end;
    OleCheck(Root.GetUIObjectOf(aWnd, //GetActiveWindow,
      FileCount,
      p^[0],
      IDataObject,
      nil,
      Pointer(Result)));
  finally
    for i := 0 to FileCount - 1 do begin
      if p^[i] <> nil then Malloc.Free(p^[i]);
    end;
    FreeMem(p);
  end;
end;

function ShowFileProperties(Files: TStrings; aWnd: HWND): Boolean;
var
  Data: IDataObject;
  propFun: TShMultiFilePropsFun;
begin
  Result := False;
  if Files.Count = 0 then Exit;
  propFun := TShMultiFilePropsFun(GetProcAddress(GetModuleHandle('shell32.dll'), 'SHMultiFileProperties')); //改成动态调用，以便可以在Win98运行! 2012.9.10
  if Assigned(propFun) then begin
    Data := GetFileListDataObject(Files, aWnd);
    propFun(Data, 0);
    Result := True;
  end;
  //SHMultiFileProperties(Data, 0);
end;

end.