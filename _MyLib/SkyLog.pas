unit SkyLog;

{*****************************************************************************
    Sky Thread Logger (for D5~D7)

    2012.8.29
*****************************************************************************}

interface

{$IFDEF VER150}            //ȡ��D7����
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$ENDIF}

uses
  Windows, SysUtils, Classes, SyncObjs;

procedure InitLogger(const AFilePrefix, ALogDir: string; ALogTime: Boolean);

procedure Log(const S: string);
procedure LogFmt(const S: string; Args: array of const);

function GetLastLogError: string;

implementation

const
  cMaxLogBlock = 32 * 1024;  //32K
  cMaxLogTick  = 30 * 1000;  //30s

type
  TSkyLogThread = class(TThread)
  private
    FFilePrefix: string;
    FLogDir:  string;
    FLogTime: Boolean;
    FLocker:  TCriticalSection;
    FLogList: TStrings;
    FLogLen:  Integer;
    FLogTick: DWord;
  protected
    procedure Execute; override;
    procedure Reset(const AFilePrefix, ALogDir: string; ALogTime: Boolean);
    procedure SaveLogToFile;
  public
    constructor Create(const AFilePrefix, ALogDir: string; ALogTime: Boolean);
    destructor Destroy; override;
    procedure Log(const S: string);
  end;

var
  g_SkyLog: TSkyLogThread;
  g_LastErr: string;

{-----------------------------------------------------------------------------
  Public Interface
-----------------------------------------------------------------------------}

procedure InitLogger(const AFilePrefix, ALogDir: string; ALogTime: Boolean);
begin
  if g_SkyLog = nil then begin
    g_SkyLog := TSkyLogThread.Create(AFilePrefix, ALogDir, ALogTime);
  end else begin
    g_SkyLog.Reset(AFilePrefix, ALogDir, ALogTime);
  end;
end;

procedure Log(const S: string);
begin
  if g_SkyLog = nil then begin
    InitLogger('SkyLog_', ExtractFilePath(ParamStr(0)), True);
  end;
  g_SkyLog.Log(S);
end;

procedure LogFmt(const S: string; Args: array of const);
begin
  Log(Format(S, Args));
end;

function GetLastLogError: string;
begin
  Result := g_LastErr;
end;

{-----------------------------------------------------------------------------
  TSkyLogThread
-----------------------------------------------------------------------------}

constructor TSkyLogThread.Create(const AFilePrefix, ALogDir: string;
  ALogTime: Boolean);
begin
  FFilePrefix := AFilePrefix;
  FLogDir     := IncludeTrailingPathDelimiter(ALogDir);
  FLogTime    := ALogTime;
  FLocker     := TCriticalSection.Create;
  FLogList    := TStringList.Create;
  inherited Create(False);
end;

destructor TSkyLogThread.Destroy;
begin
  if FLogList.Count > 0 then begin
    SaveLogToFile;
  end;
  FLogList.Free;
  FLocker.Free;
  inherited;
end;

procedure TSkyLogThread.Log(const S: string);
var
  msg: string;
begin
  FLocker.Enter;
  try
    if FLogTime then begin
      msg := Format('%s'#9'%s', [FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now), S]);
    end else begin
      msg := S;
    end;
    Inc(FLogLen, Length(msg));
    FLogList.Add(msg);
    if FLogLen > cMaxLogBlock then begin
      SaveLogToFile;
    end;
  finally
    FLocker.Leave;
  end;
end;

procedure TSkyLogThread.Execute;
begin
  while not Terminated do begin
    FLocker.Enter;
    try
      if (FLogLen > cMaxLogBlock) or (GetTickCount - FLogTick > cMaxLogTick) then begin
        SaveLogToFile;
      end;
    finally
      FLocker.Leave;
    end;
    Sleep(20);
  end;
end;

procedure TSkyLogThread.Reset(const AFilePrefix, ALogDir: string;
  ALogTime: Boolean);
begin
  FLocker.Enter;
  try
    if (AFilePrefix <> FFilePrefix) or (IncludeTrailingPathDelimiter(ALogDir) <> FLogDir) then begin
      SaveLogToFile;
    end;
    FFilePrefix := AFilePrefix;
    FLogDir     := IncludeTrailingPathDelimiter(ALogDir);
    FLogTime    := ALogTime;
  finally
    FLocker.Leave;
  end;
end;

procedure TSkyLogThread.SaveLogToFile;
const
  cModes: array[Boolean] of Integer = (fmCreate, fmOpenReadWrite or fmShareDenyWrite);
  {$IFDEF VER130}sLineBreak = #13#10;{$ENDIF}
var
  i: Integer;
  logFile: string;
  fs: TFileStream;
begin
  try
    if FLogList.Count > 0 then begin
      logFile := Format('%s%s_%s.log', [FLogDir, FFilePrefix, FormatDateTime('yyyymmdd', Date)]);
      fs := TFileStream.Create(logFile, cModes[FileExists(logFile)]);
      try
        fs.Position := fs.Size;
        for i := 0 to FLogList.Count -1 do begin
          fs.Write(PChar(FLogList[i])^, Length(FLogList[i]) * SizeOf(Char));
          fs.Write(PChar(sLineBreak)^, Length(sLineBreak) * SizeOf(Char));
        end;
      finally
        fs.Free;
      end;
      FLogList.Clear;
    end;
  except
    g_LastErr := Format('%s - %s', [FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now), Exception(ExceptObject).Message]);
  end;
  FLogLen  := 0;
  FLogTick := GetTickCount;
end;

initialization

finalization
  g_SkyLog.Free;

end.
